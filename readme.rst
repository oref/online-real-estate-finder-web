##########################
OREF Git SSH Configuration
##########################

Refer to this link: `How to setup SSH? <https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html>`_.

#############################
Virtual Hosting Configuration
#############################

On Windows Operating System

1. Open *httpd.conf* located in your *xampp/apache/conf/* or *wampp/apache/conf* directory

2. Find this line "Include conf/extra/httpd-vhosts.conf", if this is commented out "**#**", removed it, otherwise, add that line.

3. Open *httpd-vhosts.conf* and this line

   <VirtualHost *:80>

        ServerAdmin your@email.com

        DocumentRoot "C:/xampp/htdocs/oref_web"

        ServerName oref.com

        ServerAlias www.oref.com

        ErrorLog "logs/oref.com-error.log"

        CustomLog "logs/oref.com-access.log" common

   </VirtualHost>

4. Open your hosts file located in *C:/Windows/System32/drivers/etc/*

   *Note: In order to edit this file, search "notepad" and "Run as administrator"*

5. Add this line

    **127.0.0.1      localhost**

    **127.0.0.1      oref.com**

6. Restart apache on your xampp/wampp.

7. Try to open the project using **http://oref.com** in your favorite browser and enjoy the power of **Virtual Hosting**.