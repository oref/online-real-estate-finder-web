$( function( $ ){
    var obj = {};

    init();

    obj.request_grid = $("#table-buyer").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_user_details_by_type/buyer',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row)
            {
                var $view = $( '<button />' ),
                    $message = $( '<button/>' );

                $view
                    .attr({
                        type : 'button',
                        class : 'btn bgm-blue command-view',
                        'data-row-id' : row.id,
                        'data-bind' : 'view_user_details'
                    })
                    .html( '<span class="zmdi zmdi-eye"></span>' );

                $message
                    .attr({
                        type : 'button',
                        class : 'btn bgm-green command-message',
                        'data-row-id' : row.id,
                        'data-bind' : 'send_message'
                    })
                    .html( '<span class="zmdi zmdi-comments"></span>' );

                return $view.outerHTML() + $message.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        /* Executes after data is loaded and rendered */
        obj.request_grid
        .find(".command-view")
        .on("click", function( e )
        {
            e.preventDefault();

            var id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method ='view';

            load_modal( url, id );
        })
        .end()
        .find(".command-message")
        .on("click", function( e )
        {
            e.preventDefault();

            var id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'message';

            load_modal( url, id );
        });
    });

    $( '[data-bind]').click( function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_add_message = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        return $.ajax( {
            url : obj.base_url + 'admin/add_message',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status )
                {
                    load_message();
                }
                else{
                    toastr.error( "Unable to send your message", "OREF" );
                }

                $submit.prop( 'disabled', false );
            },
            error : function( xhr, status ){

                $submit.prop( 'disabled', false );
                console.log( xhr.responseText );
            }
        } );
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.method = $( 'body' ).data( 'method' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    function get_admin_details( id, $modal ){
        return $.ajax({
            url : obj.base_url + 'admin/get_admin_details',
            type : 'get',
            dataType : 'json',
            data : { id : id },
            success: function( result ){
                $.each( result, function(index, value){
                    switch(obj.method){
                        case 'view':
                            var $field = $( '.' + index );

                            if( index === 'avatar' ){
                                $field.attr('src', obj.base_url + value);
                            }
                            else{
                                $field.text( value );
                            }
                            break;
                        case 'message':
                            var $form = $modal.find( 'form' );
                            var $field = $form.find( '[name=' + index + ']' );

                            $field.val( value );
                            $field.prop('disabled', true);
                            break;
                    }
                } );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function validate( $form, modal )
    {
        $form.validate({
            ignore: ":hidden:not(#to_id)",
            rules: {
                    to_id : {
                        required : true
                    },
                    msg : {
                        required : true
                    }
            },
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            errorPlacement: function( erorr, element ){
                if($(element).attr('name') === 'to_id')
                {
                    $('#to').focus();
                }
            },
            submitHandler: function( form ){
                var $form = $( form );

                obj.ajax_add_message( $form ).done( function(){
                    if(modal) {
                        $( '#ajaxModal' ).modal( 'hide' );
                    }
                    else {
                        $( '#send_to_msg' ).val( null );
                    }
                } );
            }
        });
    }

    function load_modal( url, id )
    {
        var $modal = $( '#ajaxModal' ),
            ajax_url = obj.base_url + 'admin/modal/' + url;

        $modal.modal( {
            show : true,
            backdrop: 'static',
        } );

        switch( obj.method ){
            case 'view':
                obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                    get_admin_details( id );
                } );
                break;
            case 'message':
                obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                    get_admin_details( id, $modal ).done( function(){
                        validate( $modal.find( 'form' ), true );
                    } );
                } );
                break;
        }
    }

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<div>").append(this.eq(0).clone()).html();
    };
});