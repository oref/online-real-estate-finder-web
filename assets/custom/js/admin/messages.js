$( function(){
    var obj = {};

    init();
    load_message();
    validate( $( '#msg-form' ) );

    $( '[data-bind]').click( function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    $( '.message-list' ).niceScroll( {
                                cursorcolor: 'rgba(0,0,0,0.5)',
                                cursorborder: 0,
                                cursorborderradius: 0,
                                cursorwidth: '5px',
                                bouncescroll: true,
                                mousescrollstep: 100,
                                //autohidemode: false
                        } );

    $( '.message-list' ).scroll( function(){
        if( $( this ).scrollTop() === 0 ) {
            $( '.loader', this ).remove();
            $( this ).prepend( obj.loader );
            obj.msg_limit += 10;
            obj.is_scrolled = true;

            if( obj.is_scrolled ){
                $( this ).scrollTop( 64 );
            }
        }
        else{
            obj.is_scrolled = false;
        }
    } );

    $( document ).on( 'click', '.conversation-list > .lv-item', function( e ){
        e.preventDefault();

        var id = $( this ).data( 'id' );

        $( '.conversation-list > .lv-item' ).removeClass('active');
        $( this ).addClass( 'active' );
        obj.conversation_index = $(this).index();

        load_message_header( $( this ) );

        get_all_messages( id );
    } );

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_add_message = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        return $.ajax( {
            url : obj.base_url + 'admin/add_message',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status )
                {
                    load_message();
                }
                else{
                    toastr.error( "Unable to send your message", "OREF" );
                }

                $submit.prop( 'disabled', false );
            },
            error : function( xhr, status ){

                $submit.prop( 'disabled', false );
                console.log( xhr.responseText );
            }
        } );
    }

    function load_message_header( $elem )
    {
        $( '#msg-title' ).text( $elem.find('.lv-title').text() );
        $( '#msg-avatar' ).empty().html( $elem.find( '.lv-avatar' ).html() );
    }

    function load_message()
    {
        obj.msg_load_counter++;
        get_all_conversations().done( function(){
            var $active = $( '.conversation-list' ).find( '.lv-item.active' ),
                id = $active.data( 'id' );

            load_message_header( $active );

            if( id ){
                get_all_messages( id );
            }
        } );
    }

    function conversation_format( id, name, avatar, avatar_bg_color, msg, is_active ) {
        var html = '<a href="#" data-id="' + id + '" class="lv-item media' + is_active + '"> \
                        <div class="lv-avatar pull-left">'
                    if(avatar.trim()) {
                        html += '<img src="' + obj.base_url + avatar + '" alt="avatar">';
                    }
                    else {
                        html += '<div class="lv-avatar bgm-' + avatar_bg_color + '">' + name[0].toUpperCase() + '</div>';
                    }
                    html += '</div> \
                        <div class="media-body"> \
                            <div class="lv-title">' + name + '</div> \
                            <div class="lv-small">' + msg + '</div> \
                        </div> \
                    </a>';

        return html;
    }

    function message_format( id, name, avatar, avatar_bg_color, msg, date, currentClass, currentPos ) {

        var html = '<div data-msg-id="' + id + '" class="lv-item media' + currentClass + '"> \
                        <div class="lv-avatar pull-' + currentPos + '">';
                    if(avatar.trim()) {
                        html += '<img src="' + obj.base_url + avatar + '" alt="avatar">';
                    }
                    else {
                        html += '<div class="lv-avatar bgm-' + avatar_bg_color + '">' + name[0].toUpperCase() + '</div>';
                    }
                    html += '</div> \
                        <div class="media-body"> \
                            <div class="ms-item"> \
                                ' + msg + '\
                            </div> \
                            <small class="ms-date"><i class="zmdi zmdi-access-time"></i> ' + date + '</small> \
                        </div> \
                    </div>';

        return html;
    }

    function get_all_conversations() {

        var $conversation_list = $( '.conversation-list' ),
            $message_list = $( '.message-list' );

        return $.ajax({
            url : obj.base_url + 'admin/get_all_conversations',
            type : 'get',
            dataType : 'json',
            success : function( result ){
                $conversation_list.empty();

                if( result.conversations instanceof Array )
                {
                    $.each( result.conversations, function( index, value ){
                        var is_active = (index === obj.conversation_index) ? ' active' : '';

                        if( result.conversations[index].from_id === result.id )
                        {
                            obj.last_msg_id = result.conversations[index].msg_id
                            $conversation_list.append( conversation_format( result.conversations[index].id, result.conversations[index].to_full_name, result.conversations[index].to_avatar, result.conversations[index].to_avatar_bg_color, result.conversations[index].msg, is_active ) );
                        }
                        else if( result.conversations[index].to_id === result.id )
                        {
                            obj.last_msg_id = result.conversations[index].msg_id
                            $conversation_list.append( conversation_format( result.conversations[index].id, result.conversations[index].from_full_name, result.conversations[index].from_avatar, result.conversations[index].from_avatar_bg_color, result.conversations[index].msg, is_active ) );
                        }
                    } );
                }
                else
                {
                    $message_list.html('<h2 class="text-center">No message to display</h2>');
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function get_all_messages( id ) {
        var $message_list = $( '.message-list' );

        $.ajax({
            url : obj.base_url + 'admin/get_all_messages',
            type : 'post',
            dataType : 'json',
            data : { id : id, start : obj.msg_limit },
            success : function( result ){
                $message_list.empty();
                if( obj.is_scrolled ){
                    $message_list.scrollTop( 10 );
                    obj.is_scrolled = false;
                }

                if( result.messages instanceof Array )
                {
                    $.each( result.messages, function( index, value ){
                        var currentPos = ( result.id === result.messages[index].sender_id ) ? 'right' : 'left',
                            currentClass = ( result.id === result.messages[index].sender_id ) ? ' right' : '',
                            receiver_id;

                        $message_list.append( message_format( result.messages[index].id, result.messages[index].sender_name, result.messages[index].sender_avatar, result.messages[index].sender_avatar_bg_color, result.messages[index].msg, result.messages[index].date_added, currentClass, currentPos ) );
                        if( result.id === result.messages[index].from_id ) {
                            receiver_id = result.messages[index].to_id;
                        }
                        else {
                            receiver_id = result.messages[index].from_id;
                        }

                        $('#send_to_id').val(receiver_id);
                    } );

                    if( obj.msg_load_counter <= 1 && !obj.is_scrolled ){
                        $message_list.animate({ scrollTop: $( '.message-list' ).prop('scrollHeight') }, 500);
                    }

                }
                else
                {
                    $message_list.html('<h2 class="text-center">No message to display</h2>');
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function typeahead() {
        var users = new Bloodhound({
                                datumTokenizer: Bloodhound.tokenizers.whitespace,
                                queryTokenizer: Bloodhound.tokenizers.whitespace,
                                remote:{
                                    url: obj.base_url + 'admin/get_all_users/%q',
                                    wildcard: '%q'
                                },
                          });

        users.initialize();

        $('#to').typeahead(null,{
            name: 'users',
            source : users,
            displayKey : 'full_name',
            templates : {
                suggestion : function( user ) {
                    var html = '',
                        avatar = user.avatar;

                    html += '<div class="media">';
                        html += '<div class="media-left">';
                            if(avatar.trim()) {
                                html += '<img class="media-object" src="' + obj.base_url + user.avatar + '" alt="' + user.username + '-avatar">';
                            }
                            else {
                                html += '<div class="lv-avatar bgm-' + user.avatar_bg_color + '">' + user.full_name[0].toUpperCase() + '</div>';
                            }
                        html += '</div>';
                        html += '<div class="media-body">';
                            html += '<ul class="list-unstyled">';
                            html += '<li><strong class="media-heading">' + user.full_name + '</strong></li>';
                            html += '<li>' + ((user.username) ? '@' + user.username : 'N/A') + '</li>';
                            html += '<li>' + user.email + '</li>';
                            html += '</ul>';
                         html += '</div>';
                    html += '</div>';

                    return html;
                }
            }
        });

        /*$('#to').bind('typeahead:change', function( e ) {
            var $to_id = $('#to_id');

            $to_id.val(null);
        });*/
        $('#to').bind('typeahead:select', function( e, suggestion ) {
            var $to_id = $('#to_id');

            $to_id.val(suggestion.id);
        })
        $('#to').bind('typeahead:autocomplete', function( e, suggestion ) {
            var $to_id = $('#to_id');

            $to_id.val(suggestion.id);
        });
        $('#to').bind('typeahead:cursorchange', function( e, suggestion ) {
            var $to_id = $('#to_id');

        });
    }

    function validate( $form, modal )
    {
        $form.validate({
            ignore: ":hidden:not(#to_id)",
            rules: {
                    to_id : {
                        required : true
                    },
                    msg : {
                        required : true
                    }
            },
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            errorPlacement: function( erorr, element ){
                if($(element).attr('name') === 'to_id')
                {
                    $('#to').focus();
                }
            },
            submitHandler: function( form ){
                var $form = $( form );

                obj.ajax_add_message( $form ).done( function(){
                    if(modal) {
                        $( '#ajaxModal' ).modal( 'hide' );
                    }
                    else {
                        $( '#send_to_msg' ).val( null );
                    }
                } );
            }
        });
    }

    function load_modal( url )
    {
        var $modal = $( '#ajaxModal' ),
            ajax_url = obj.base_url + 'admin/modal/' + url;

        $modal.modal( {
            show : true,
            backdrop: 'static',
        } );

        obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
            typeahead();
            placeholder_float();

            validate( $modal.find( 'form' ), true );
        } );
    }

    function placeholder_float()
    {
        //Add blue animated border and remove with condition when focus and blur
        if($('.fg-line')[0]) {
            $('body').on('focus', '.form-control', function(){
                $(this).closest('.fg-line').addClass('fg-toggled');
            })

            $('body').on('blur', '.form-control', function(){
                var p = $(this).closest('.form-group');
                var i = p.find('.form-control').val();

                if (p.hasClass('fg-float')) {
                    if (i.length == 0) {
                        $(this).closest('.fg-line').removeClass('fg-toggled');
                    }
                }
                else {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            });
        }

        //Add blue border for pre-valued fg-flot text feilds
        if($('.fg-float')[0]) {
            $('.fg-float .form-control').each(function(){
                var i = $(this).val();

                if (!i.length == 0) {
                    $(this).closest('.fg-line').addClass('fg-toggled');
                }

            });
        }
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';
        obj.conversation_index = parseInt($.cookie( 'conversation_index' )) || 0;
        obj.msg_limit = 10;
        obj.is_scrolled = false;
        obj.msg_load_counter = 0;
        $.removeCookie( 'conversation_index', { path: '/' });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        setInterval(load_message, 1000);
    }

} );