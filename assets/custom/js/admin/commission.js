$( function( $ ){
    var obj = {};

    init();
    get_all_sold_properties();


    function view_sold_property_format( id, name, price, type, agent_full_name, date_sold, info )
    {
        var commission = ((price * .05).toFixed(2)),
            html = '<div class="t-view" data-tv-type="image"> \
                        <div class="tv-header media"> \
                            <div class="media-body p-t-5"> \
                                <strong class="d-block">' + name + '</strong> \
                                <small class="c-gray">' + date_sold + '</small> \
                            </div> \
                        </div> \
                        <div class="tv-body"> \
                            <p>Price: ' + price + '</p> \
                            <p>Commission of Agent: <b><i class="fa fa-ruble"></i> ' + commission + '</b></p> \
                            <p>Agent: ' + agent_full_name + '</p> \
                            <p>Type: ' + type + '</p> \
                            <p>Info: ' + info + '</p> \
                        </div> \
                    </div> \
                    <div class="clearfix"></div>';

        return html;
    }

    function get_all_sold_properties()
    {
        var $sold_property = $( '.sold_property' );

        $.ajax({
            url : obj.base_url + 'admin/get_all_property_details_by_status/sold',
            type : 'get',
            dataType : 'json',
            success : function ( result ){
                $sold_property.empty();

                if( result instanceof Array)
                {
                    $.each( result, function(key, value){
                        $sold_property.append(view_sold_property_format(result[key].id, result[key].name, result[key].price, result[key].type, result[key].agent_full_name, result[key].date_sold, result[key].info));
                    } );
                }
            },
            error : function( xhr, status )
            {
                console.log( xhr.responseText );
            }
        });
    }

    function placeholder_float(){
        if($('.fg-line')[0]) {
            $('body').on('focus', '.form-control', function(){
                $(this).closest('.fg-line').addClass('fg-toggled');
            })

            $('body').on('blur', '.form-control', function(){
                var p = $(this).closest('.form-group');
                var i = p.find('.form-control').val();

                if (p.hasClass('fg-float')) {
                    if (i.length == 0) {
                        $(this).closest('.fg-line').removeClass('fg-toggled');
                    }
                }
                else {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            });
        }

        //Add blue border for pre-valued fg-flot text feilds
        if($('.fg-float')[0]) {
            $('.fg-float .form-control').each(function(){
                var i = $(this).val();

                if (!i.length == 0) {
                    $(this).closest('.fg-line').addClass('fg-toggled');
                }

            });
        }
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }
});