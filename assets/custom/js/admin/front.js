$( function( $ ){
    var obj = {};

    init();

    if( $.cookie( 'error' ) )
    {
        toastr.error( $.cookie( 'error' ), "OREF" );
        $.removeCookie( 'error', { path: '/' });
    }

    $('#admin-login').validate({
        rules: {
                login : {
                    required : true
                },
                password : {
                    required : true
                }
        },
        highlight: function( element, errorClass, validClass ){
            $(element).addClass( errorClass ).removeClass( validClass );
        },
        unhighlight: function( element, errorClass, validClass ){
            $(element).removeClass( errorClass ).addClass( validClass );
        },
        errorPlacement: function( erorr, element ){
            return false;
        },
        submitHandler: function( form ){
            var $form = $( form );

            obj.ajax_signin( $form );
        }
    });

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.controller = $( 'body' ).data( 'controller' );
        obj.method = $( 'body' ).data( 'method' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    obj.ajax_signin = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );
        $.ajax( {
            url : obj.base_url + 'admin/signin',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.html( '<i class="fa fa-refresh fa-spin"></i>' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status )
                {
                    switch( obj.method )
                    {
                        case 'lock':
                            window.location = obj.base_url + 'admin';
                            break;
                        default:
                            location.reload();
                            break;
                    }
                }
                else{
                    toastr.error( "Incorrect username/password", "OREF" );
                }

                $submit.html( '<i class="zmdi zmdi-arrow-forward"></i>' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

} );