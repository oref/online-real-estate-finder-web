$( function( $ ){
    var obj = {};

    init();

    obj.agent_approve_property_grid = $("#table-approve-property-request").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_all_assigned_properties',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "select" : function(column, row)
            {
                var $select = $( '<select />' );

                $select.attr({
                    name : 'agent_id',
                    class : 'form-control selectpicker agents',
                    'data-row-id' : row.id
                });

                return $select.outerHTML();
            },
            "commands": function(column, row)
            {
                var $view = $( '<button />' ),
                    $accept = $( '<button />' ),
                    $decline = $( '<button />' );

                $view
                    .attr({
                        type : 'button',
                        title : 'View',
                        class : 'btn bgm-blue command-view',
                        'data-row-id' : row.id,
                        'data-bind' : 'view_property_details'
                    })
                    .html( '<span class="zmdi zmdi-eye"></span>' );

                $accept
                    .attr({
                        type : 'button',
                        title : 'Accept',
                        class : 'btn bgm-green command-accept',
                        'data-row-id' : row.id
                    })
                    .html( '<span class="zmdi zmdi-check"></span>' );

                $decline
                    .attr({
                        type : 'button',
                        title : 'Decline',
                        class : 'btn bgm-red command-block',
                        'data-row-id' : row.id,
                        'data-bind' : 'confirmation'
                    })
                    .html( '<span class="zmdi zmdi-block"></span>' );

                return $view.outerHTML() + $accept.outerHTML() + $decline.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        obj.agent_approve_property_grid
        .find(".command-view")
        .on("click", function( e )
        {
            e.preventDefault();
            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            load_modal( url, property_id );

        })
        .end()
        .find(".command-accept")
        .on("click", function( e )
        {
            e.preventDefault();
            var property_id = $(this).data('row-id'),
                agent_id = $(this).parents('tr').find('.agents').val();

                if(agent_id === 'none') {
                    toastr.error('Please assign an agent for this property.');
                }
                else {
                    obj.ajax_update_property_approve(property_id);
                }
        })
        .end()
        .find(".command-block")
        .on("click", function( e )
        {
            e.preventDefault();

            obj.method = "decline";

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            load_modal( url, property_id );

        })
    });

    obj.broker_approve_property_grid = $("#table-update-property-status-request").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_all_approved_properties/pending',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row)
            {
                var $view = $( '<button />' ),
                    $accept = $( '<button />' ),
                    $decline = $( '<button />' );
                $view
                    .attr({
                        type : 'button',
                        class : 'btn bgm-blue command-view',
                        'data-row-id' : row.id,
                        title : 'View',
                        'data-bind' : 'view_property_details'
                    })
                    .html( '<span class="zmdi zmdi-eye"></span>' );

                $accept
                    .attr({
                        type : 'button',
                        class : 'btn bgm-green command-accept',
                        title : 'Accept',
                        'data-row-id' : row.id
                    })
                    .html( '<span class="zmdi zmdi-check"></span>' );

                $decline
                    .attr({
                        type : 'button',
                        class : 'btn bgm-red command-decline',
                        title : 'Block',
                        'data-row-id' : row.id,
                        'data-bind' : 'confirmation'
                    })
                    .html( '<span class="zmdi zmdi-block"></span>' );

                return $view.outerHTML() + $accept.outerHTML() + $decline.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        obj.broker_approve_property_grid
        .find(".command-view")
        .on("click", function( e )
        {
            e.preventDefault();

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            load_modal( url, property_id );

        })
        .end()
        .find(".command-accept")
        .on("click", function( e )
        {
            e.preventDefault();

            var property_id = $(this).data('row-id');

            obj.ajax_update_property_status(property_id);
        })
        .end()
        .find(".command-decline")
        .on("click", function( e )
        {
            e.preventDefault();

            obj.method = "block";

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            load_modal( url, property_id );

        });
    });

    obj.assign_agent_grid = $("#table-assign-agent-request").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_all_not_assigned_properties',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "select" : function(column, row)
            {
                var $select = $( '<select />' );

                $select.attr({
                    name : 'agent_id',
                    class : 'form-control selectpicker agents',
                    'data-row-id' : row.id
                });

                return $select.outerHTML();
            },
            "commands": function(column, row)
            {
                var $view = $( '<button />' ),
                    $accept = $( '<button />' );

                $view
                    .attr({
                        type : 'button',
                        title : 'View',
                        class : 'btn bgm-blue command-view',
                        'data-row-id' : row.id,
                        'data-bind' : 'view_property_details'
                    })
                    .html( '<span class="zmdi zmdi-eye"></span>' );

                $accept
                    .attr({
                        type : 'button',
                        title : 'Accept',
                        class : 'btn bgm-green command-accept',
                        'data-row-id' : row.id
                    })
                    .html( '<span class="zmdi zmdi-check"></span>' );

                return $view.outerHTML() + $accept.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        get_all_agents( $('.agents') );

        obj.assign_agent_grid
        .find(".command-view")
        .on("click", function( e )
        {
            e.preventDefault();
            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            load_modal( url, property_id );

        })
        .end()
        .find(".command-accept")
        .on("click", function( e )
        {
            e.preventDefault();
            var property_id = $(this).data('row-id');
                agent_id = $(this).parents('tr').find('.agents').val();

            if(agent_id === 'none') {
                toastr.error('Please assign an agent for this property.', "OREF");
            }
            else {
                obj.ajax_update_agent_assign(property_id, agent_id);
            }
        });
    });

    $( '[data-bind]').click( function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    obj.ajax_update_property_status = function( property_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_property_status/approved',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    toastr.success( 'Saved!' );
                    obj.broker_approve_property_grid.bootgrid( 'reload' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_update_property_declined = function( $form, $confirm, property_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_property_status/declined',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    obj.broker_approve_property_grid.bootgrid( 'reload' );
                    $confirm.modal( 'hide' );
                    toastr.success('Property declined!', "OREF");
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_update_property_agent_id = function( $form, $confirm, property_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_property_agent_id',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    obj.agent_approve_property_grid.bootgrid( 'reload' );
                    $confirm.modal( 'hide' );
                    toastr.success('Property blocked!', "OREF");
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_update_property_approve = function( property_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_property_approve',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    toastr.success( 'Saved!' );
                    obj.agent_approve_property_grid.bootgrid( 'reload' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_update_agent_assign = function( property_id, agent_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_property_agent',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id, agent_id : agent_id },
            success : function( result ){
                if( result.status )
                {
                    toastr.success( 'Wait for the agent response', "OREF" );
                    obj.assign_agent_grid.bootgrid( 'reload' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_get_confirmation_content = function( url, $confirm ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $confirm.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $confirm.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    function confirmation( $form, $confirm, property_id )
    {
        $form.validate({
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            errorPlacement: function( erorr, element ){
                return false;
            },
            submitHandler: function(){
                switch(obj.method){
                    case "block":
                        obj.ajax_update_property_declined( $form, $confirm, property_id );
                        break;
                    case "decline":
                        obj.ajax_update_property_agent_id( $form, $confirm, property_id );
                        break;
                }
            }
        });
    }

    function get_all_agents( $agents )
    {
        $.ajax({
            url : obj.base_url + 'admin/get_all_agents',
            type : 'get',
            dataType : 'json',
            beforeSend : function(){
                var $option = $('<option />');

                $option.attr('value', 'none').text('Fetching data...');

                $agents.append($option);
            },
            success : function( result ){
                var $label = $('<option />');
                $agents.empty();

                $label.attr({ value : 'none' }).prop('selected', true).text('-Select-');
                $agents.append($label);

                $.each(result, function(index, value){
                    var $option = $('<option />');

                    $option.attr('value', result[index].id).text(result[index].firstname + ' ' + result[index].lastname);
                    $agents.append($option);
                });
            }
        });
    }

    function ajax_get_property_details( property_id, $modal ){
        var $form = $modal.find( 'form' );
        return $.ajax({
            url : obj.base_url + 'admin/get_property_details_by_id',
            type : 'get',
            dataType : 'json',
            data : { property_id : property_id },
            success: function( result ){
                if( result instanceof Object ) {
                    $.each( result, function(index, value){
                        var $field = $form.find( '[name=' + index + ']' );
                        switch(index){
                            case 'region_id' :
                                $field.val( result.region );
                                break;
                            case 'province_id' :
                                $field.val( result.province );
                                break;
                            case 'city_id' :
                                $field.val( result.city );
                                break;
                            case 'barangay_id' :
                                $field.val( result.barangay );
                                break;
                            case 'type_id' :
                                $field.val( result.type );
                                break;
                            case 'images' :
                                var $property_images = $( '.property-images' ),
                                    html = '';

                                $property_images.empty();
                                $.each(result.images, function(index, val) {
                                    html += '<div class="col-xs-6 col-md-3">';
                                    html += '<a href="' + obj.base_url + result.images[index].path + '" class="thumbnail image-link">';
                                    html += '<img src="' + obj.base_url + result.images[index].path + '" />';
                                    html += '</a>';
                                    html += '</div>';
                                });

                                $property_images.append( html );
                                break;
                            default :
                                $field.val( value );
                        }
                        $field.prop('disabled', true);
                    } );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function load_modal( url, property_id )
    {
        var $modal = $( '#ajaxModal' ),
            $confirm = $( '#confirmation' ),
            property_id = property_id;
            ajax_url = obj.base_url + 'admin/modal/' + url;

        if( url === 'confirmation' ){
            $confirm.modal( {
                show : true,
                backdrop: 'static',
            } );

            obj.ajax_get_confirmation_content( ajax_url, $confirm ).done( function(){
                confirmation( $confirm.find( 'form' ), $confirm, property_id );
            } );
        }
        else{
            $modal.modal( {
                show : true,
                backdrop: 'static',
            } );

            obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                ajax_get_property_details( property_id, $modal ).done( function(){
                    $('.property-images').find( 'a' ).magnificPopup({
                      // delegate : 'a',
                      type: 'image',
                      gallery: {
                        enabled:true
                      }
                      // other options
                    });
                } );
            } );
        }
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<div>").append(this.eq(0).clone()).html();
    };
});