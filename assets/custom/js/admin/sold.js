$( function( $ ){
    var obj = {};

    init();

    obj.sold_property_grid = $("#table-sold-properties").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_all_sold_properties',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row)
            {
                var $view = $( '<button />' ),
                    $claimed = $( '<button />' );

                $view
                    .attr({
                        type : 'button',
                        title : 'View',
                        class : 'btn bgm-blue command-view',
                        'data-row-id' : row.id,
                        'data-bind' : 'view_property_details'
                    })
                    .html( '<span class="zmdi zmdi-eye"></span>' );

                $claimed
                    .attr({
                        type : 'button',
                        title : 'Claimed',
                        class : 'btn bgm-green command-details',
                        'data-row-id' : row.id,
                        'data-bind' : 'confirmation'
                    })
                    .html( '<span class="zmdi zmdi-thumb-up"></span>' );

                return $view.outerHTML() + $claimed.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        obj.sold_property_grid
        .find(".command-view")
        .on("click", function( e )
        {
            e.preventDefault();

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            load_modal( url, property_id );

        })
        .end()
        .find(".command-details")
        .on("click", function( e )
        {
            e.preventDefault();

            obj.method = "claimed";

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            load_modal( url, property_id );

        });
    });

    obj.ajax_update_claimed_commission = function( $form, $confirm, property_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_claimed_commission',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    toastr.success( 'Saved!' );
                    obj.sold_property_grid.bootgrid( 'reload' );

                    $confirm.modal( 'hide' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
                placeholder_float();
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_get_confirmation_content = function( url, $confirm ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $confirm.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $confirm.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    function ajax_get_property_details( property_id, $modal ){
        var $form = $modal.find( 'form' );
        return $.ajax({
            url : obj.base_url + 'admin/get_property_details_by_id',
            type : 'get',
            dataType : 'json',
            data : { property_id : property_id },
            success: function( result ){
                if( result instanceof Object ) {
                    $.each( result, function(index, value){
                        var $field = $form.find( '[name=' + index + ']' );
                        switch(index){
                            case 'region_id' :
                                $field.val( result.region );
                                break;
                            case 'province_id' :
                                $field.val( result.province );
                                break;
                            case 'city_id' :
                                $field.val( result.city );
                                break;
                            case 'barangay_id' :
                                $field.val( result.barangay );
                                break;
                            case 'type_id' :
                                $field.val( result.type );
                                break;
                            case 'images' :
                                var $property_images = $( '.property-images' ),
                                    html = '';

                                $property_images.empty();
                                $.each(result.images, function(index, val) {
                                    html += '<div class="col-xs-6 col-md-3">';
                                    html += '<a href="' + obj.base_url + result.images[index].path + '" class="thumbnail image-link">';
                                    html += '<img src="' + obj.base_url + result.images[index].path + '" />';
                                    html += '</a>';
                                    html += '</div>';
                                });

                                $property_images.append( html );
                                break;
                            default :
                                $field.val( value );
                        }
                        $field.prop('disabled', true);
                    } );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function confirmation( $form, $confirm, property_id )
    {
        $form.validate({
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            errorPlacement: function( erorr, element ){
                return false;
            },
            submitHandler: function(){
                switch(obj.method){
                    case 'claimed' :
                        obj.ajax_update_claimed_commission( $form, $confirm, property_id );
                        break;
                }
            }
        });
    }

    function load_modal( url, property_id )
    {
        var $modal = $( '#ajaxModal' ),
            $confirm = $( '#confirmation' ),
            ajax_url = obj.base_url + 'admin/modal/' + url;

            if( url === 'confirmation' ){
                $confirm.modal( {
                    show : true,
                    backdrop: 'static',
                } );

                obj.ajax_get_confirmation_content( ajax_url, $confirm ).done( function(){
                    confirmation( $confirm.find( 'form' ), $confirm, property_id );
                } );
            }
            else{
                $modal.modal( {
                    show : true,
                    backdrop: 'static',
                } );

                obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                    ajax_get_property_details( property_id, $modal ).done( function(){
                        $('.property-images').find( 'a' ).magnificPopup({
                          // delegate : 'a',
                          type: 'image',
                          gallery: {
                            enabled:true
                          }
                          // other options
                        });
                    } );
                } );
            }
    }

    function placeholder_float(){
        if($('.fg-line')[0]) {
            $('body').on('focus', '.form-control', function(){
                $(this).closest('.fg-line').addClass('fg-toggled');
            })

            $('body').on('blur', '.form-control', function(){
                var p = $(this).closest('.form-group');
                var i = p.find('.form-control').val();

                if (p.hasClass('fg-float')) {
                    if (i.length == 0) {
                        $(this).closest('.fg-line').removeClass('fg-toggled');
                    }
                }
                else {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            });
        }

        //Add blue border for pre-valued fg-flot text feilds
        if($('.fg-float')[0]) {
            $('.fg-float .form-control').each(function(){
                var i = $(this).val();

                if (!i.length == 0) {
                    $(this).closest('.fg-line').addClass('fg-toggled');
                }

            });
        }
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<div>").append(this.eq(0).clone()).html();
    };
});