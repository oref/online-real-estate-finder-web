$( function( $ ){
    var obj = {};

    init();

    $( '[data-bind]').click( function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    $( document ).on( 'change', '#regions', function(){
        var value = $(this).val(),
            $provinces = $('#provinces');
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $provinces.prop('disabled', true).empty();
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
        }
        else{
            $provinces.prop('disabled', false);
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            ajax_get_provinces(value);
        }
    });

    $( document ).on( 'change', '#provinces', function(){
        var value = $(this).val(),
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
        }
        else{
            $cities.prop('disabled', false);
            $barangays.prop('disabled', true).empty();
            ajax_get_cities(value);
        }
    });

    obj.declined_property_grid = $("#table-developer-properties").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_all_declined_properties',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row)
            {
                var $view = $( '<button />' ),
                    $edit = $( '<button />' ),
                    $unblock = $( '<button />' );
                $view
                    .attr({
                        type : 'button',
                        title : 'View',
                        class : 'btn bgm-blue command-view',
                        'data-row-id' : row.id,
                        'data-bind' : 'view_property_details'
                    })
                    .html( '<span class="zmdi zmdi-eye"></span>' );

                $edit
                    .attr({
                        type : 'button',
                        title : 'Edit',
                        class : 'btn bgm-green command-edit',
                        'data-row-id' : row.id,
                        'data-bind' : 'edit_property'
                    })
                    .html( '<span class="zmdi zmdi-edit"></span>' );

                $unblock
                    .attr({
                        type : 'button',
                        title : 'Unblock',
                        class : 'btn bgm-red command-decline',
                        'data-row-id' : row.id,
                        'data-bind' : 'confirmation'
                    })
                    .html( '<span class="zmdi zmdi-undo"></span>' );

                return $view.outerHTML() + $edit.outerHTML() + $unblock.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        obj.declined_property_grid
        .find(".command-view")
        .on("click", function( e )
        {
            e.preventDefault();

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'view';

            load_modal( url, property_id );

        })
        .end()
        .find(".command-edit")
        .on("click", function( e )
        {
            e.preventDefault();

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'edit';

            load_modal( url, property_id );
        })
        .end()
        .find(".command-decline")
        .on("click", function( e )
        {
            e.preventDefault();

            var property_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'declined';

            load_modal( url, property_id );
        });
    });

    obj.ajax_update_property_status = function( $form, $confirm, property_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_property_status/approved',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    obj.declined_property_grid.bootgrid( 'reload' );
                    $confirm.modal( 'hide' );
                    toastr.success('Property unblocked!', "OREF");
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
                placeholder_float();
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_get_confirmation_content = function( url, $confirm ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $confirm.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $confirm.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_update_property_status = function( $form, $confirm, property_id ){
        $.ajax({
            url : obj.base_url + 'admin/update_property_status/approved',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    obj.declined_property_grid.bootgrid( 'reload' );
                    toastr.success('Property unblocked!', "OREF");

                    $confirm.modal( 'hide' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_update_property = function ( $form, $modal ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'admin/update_property',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    obj.declined_property_grid.bootgrid( 'reload' );
                    toastr.success('Sucessfully updated property!', "OREF");

                    $modal.modal( 'hide' );
                }
                else{
                    toastr.error( "Failed to update", "OREF" );
                }

                $submit.text( 'Update' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function ajax_get_regions(){
        var $regions = $('#regions');

        return $.ajax({
            url : obj.base_url + 'admin/get_region_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', 'none').text('Fetching');
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', 'none').text('-Select-');

                    $regions.empty();
                    $regions.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].region_id ).text( result[index].short_name );
                        $regions.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_provinces( region_id ){
        var $provinces = $('#provinces');

        return $.ajax({
            url : obj.base_url + 'user/get_province_details',
            type : 'get',
            dataType : 'json',
            data : { region_id : region_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $provinces.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $provinces.empty();
                    $provinces.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].province_id ).text( result[index].long_name );
                        $provinces.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_cities( province_id ){
        var $cities = $('#cities');

        return $.ajax({
            url : obj.base_url + 'user/get_city_details',
            type : 'get',
            dataType : 'json',
            data : { province_id : province_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $cities.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $cities.empty();
                    $cities.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].city_id ).text( result[index].long_name );
                        $cities.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_barangays( city_id ){
        var $barangays = $('#barangays');

        return $.ajax({
            url : obj.base_url + 'user/get_barangay_details',
            type : 'get',
            dataType : 'json',
            data : { city_id : city_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $barangays.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $barangays.empty();
                    $barangays.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].barangay_id ).text( result[index].long_name );
                        $barangays.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_property_types(){
        var $types = $('#types');

        return $.ajax({
            url : obj.base_url + 'admin/get_property_types_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $types.empty();
                    $types.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].id ).text( result[index].type );
                        $types.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function ajax_get_property_details( property_id, $modal ){
        var $form = $modal.find( 'form' );
        return $.ajax({
            url : obj.base_url + 'admin/get_property_details_by_id',
            type : 'get',
            dataType : 'json',
            data : { property_id : property_id },
            success: function( result ){
                if( result instanceof Object ) {
                    $.each( result, function(index, value){
                        var $field = $form.find( '[name=' + index + ']' );

                        switch(obj.method)
                        {
                            case 'edit':
                                switch( index ){
                                    case 'region_id' :
                                        $( '[name=' + index + '] option' ).filter( function(){
                                            return ( $(this).val() === result[index] );
                                        }).prop('selected', true);
                                        $('#provinces').prop('disabled', false);
                                        break;
                                    case 'province_id' :
                                        ajax_get_provinces( result.region_id ).done(function(){
                                            $( '[name=' + index + '] option' ).filter( function(){
                                                return ( $(this).val() === result[index] );
                                            }).prop('selected', true);
                                        });
                                        $('#cities').prop('disabled', false);
                                        break;
                                    case 'city_id' :
                                        ajax_get_cities( result.province_id ).done( function(){
                                            $( '[name=' + index + '] option' ).filter( function(){
                                                return ( $(this).val() === result[index] );
                                            }).prop('selected', true);
                                        });
                                        $('#barangays').prop('disabled', false);
                                        break;
                                    case 'barangay_id' :
                                        ajax_get_barangays( result.city_id ).done( function(){
                                            $( '[name=' + index + '] option' ).filter( function(){
                                                return ( $(this).val() === result[index] );
                                            }).prop('selected', true);
                                        } );
                                        break;
                                    case 'type_id' :
                                        $( '[name=' + index + '] option' ).filter( function(){
                                            return ( $(this).val() === result[index] );
                                        }).prop('selected', true);
                                        break;
                                    case 'tags':
                                        $field.val( result[index] );
                                        $field.tagsinput( 'refresh' );
                                        break;
                                    case 'avatar':
                                        break;
                                    default:
                                        $field.val(value);
                                }
                                break;
                            case 'view':
                                switch(index){
                                    case 'region_id' :
                                        $field.val( result.region );
                                        break;
                                    case 'province_id' :
                                        $field.val( result.province );
                                        break;
                                    case 'city_id' :
                                        $field.val( result.city );
                                        break;
                                    case 'barangay_id' :
                                        $field.val( result.barangay );
                                        break;
                                    case 'type_id' :
                                        $field.val( result.type );
                                        break;
                                    case 'images' :
                                        var $property_images = $( '.property-images' ),
                                            html = '';

                                        $property_images.empty();
                                        $.each(result.images, function(index, val) {
                                            html += '<div class="col-xs-6 col-md-3">';
                                            html += '<a href="' + obj.base_url + result.images[index].path + '" class="thumbnail image-link">';
                                            html += '<img src="' + obj.base_url + result.images[index].path + '" />';
                                            html += '</a>';
                                            html += '</div>';
                                        });

                                        $property_images.append( html );
                                        break;
                                    default :
                                        $field.val( value );
                                }
                                $field.prop('disabled', true);
                                break;
                        }
                    } );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function confirmation( $form, $confirm, property_id )
    {
        $form.validate({
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            errorPlacement: function( erorr, element ){
                return false;
            },
            submitHandler: function(){
                obj.ajax_update_property_status( $form, $confirm, property_id );
            }
        });
    }

    function validate( $form, $modal )
    {
        if( $.fn.tagsinput ) $( '[data-bind="tags"]' ).tagsinput( 'refresh' );

        $form.validate({
            ignore : [],
            rules: {
                name : {
                    required : true
                },
                price: {
                    required : true,
                    number : true
                },
                contactno : {
                    required : true
                },
                region_id : {
                    required : true
                },
                province_id : {
                    required : true
                },
                city_id : {
                    required : true
                },
                barangay_id : {
                    required : true
                },
                bathroom : {
                    required : true,
                    number : true
                },
                bedroom : {
                    required : true,
                    number : true
                },
                type_id : {
                    required : true
                },
                features : {
                    required : true
                },
                info : {
                    required : true
                }

            },
            messages : {
                name : {
                    required : "Name is required"
                },
                price : {
                    required : "Price is required",
                    number : "Input only numbers"
                },
                contactno : {
                    required : "Contact Number is required"
                },
                bathroom : {
                    required : "Number of bathroom is required",
                    number : "Input only numbers"
                },
                bedroom : {
                    required : "Number of bedroom is required",
                    number : "Input only numbers"
                },
                type_id : {
                    required : "Type is required"
                },
                features : {
                    required : "Features is required"
                },
                info : {
                    required : "Information is required"
                },
                region_id : {
                    required : "Region is required"
                },
                province_id : {
                    required : "Province is required"
                },
                city_id : {
                    required : "City is required"
                },
                barangay_id : {
                    required : "Barangay is required"
                }
            },
            errorPlacement: function(error, element) {
                switch(element.attr('name'))
                {
                    case 'features':
                            error.insertAfter(".bootstrap-tagsinput");
                            break;
                    default:
                            error.insertAfter(element);

                }
            },
            highlight: function( element, errorClass, validClass ){
                switch($(element).attr('name'))
                {
                    case 'features':
                            $(element).siblings(".bootstrap-tagsinput").addClass( errorClass ).removeClass( validClass );
                            break;
                    default:
                            $(element).addClass( errorClass ).removeClass( validClass );

                }
            },
            unhighlight: function( element, errorClass, validClass ){
                switch($(element).attr('name'))
                {
                    case 'features':
                            $(element).siblings(".bootstrap-tagsinput").removeClass( errorClass ).addClass( validClass );
                            break;
                    default:
                            $(element).removeClass( errorClass ).addClass( validClass );
                }
            },
            submitHandler : function(){
                switch(obj.method){
                    case 'edit' :
                        obj.ajax_update_property( $form, $modal );
                        break;
                }
            }
         });
    }

    function placeholder_float(){
        if($('.fg-line')[0]) {
            $('body').on('focus', '.form-control', function(){
                $(this).closest('.fg-line').addClass('fg-toggled');
            })

            $('body').on('blur', '.form-control', function(){
                var p = $(this).closest('.form-group');
                var i = p.find('.form-control').val();

                if (p.hasClass('fg-float')) {
                    if (i.length == 0) {
                        $(this).closest('.fg-line').removeClass('fg-toggled');
                    }
                }
                else {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            });
        }

        //Add blue border for pre-valued fg-flot text feilds
        if($('.fg-float')[0]) {
            $('.fg-float .form-control').each(function(){
                var i = $(this).val();

                if (!i.length == 0) {
                    $(this).closest('.fg-line').addClass('fg-toggled');
                }

            });
        }
    }

    function load_modal( url, property_id )
    {
        var $modal = $( '#ajaxModal' ),
            $confirm = $( '#confirmation' ),
            property_id = property_id;
            ajax_url = obj.base_url + 'admin/modal/' + url;

        if( url === 'confirmation' ){
            $confirm.modal( {
                show : true,
                backdrop: 'static',
            } );

            obj.ajax_get_confirmation_content( ajax_url, $confirm ).done( function(){
                confirmation( $confirm.find( 'form' ), $confirm, property_id );
            } );
        }
        else{
            $modal.modal( {
                show : true,
                backdrop: 'static',
            } );

            obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                ajax_get_regions().done( function(){
                    ajax_get_property_types().done( function(){
                        ajax_get_property_details( property_id, $modal ).done(function(){
                            validate( $modal.find( 'form' ), $modal );
                            $('.property-images').find( 'a' ).magnificPopup({
                                  // delegate : 'a',
                                  type: 'image',
                                  gallery: {
                                    enabled:true
                                  }
                                  // other options
                            });
                        } );
                    } );
                } );
            } );
        }
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<div>").append(this.eq(0).clone()).html();
    };
});