$( function( $ ){
    var obj = {};

    init();
    get_all_tasks();
    get_all_recently_opened_properties();
    get_agent_details();

    $('.task-list').niceScroll({
        cursorcolor: 'rgba(0, 0, 0, 0.298039)'
    });

    $( '.refresh-properties' ).click(function( e ){
        e.preventDefault();

        get_all_recently_opened_properties();
    } );

    $( '.refresh-task' ).click(function( e ){
        e.preventDefault();

        get_all_tasks();
    } );

    $( '.todo-save' ).click(function( e ){
        e.preventDefault();

        var $form = $(this).parents( 'form' );

        obj.add_task_details( $form );
        get_all_tasks();
    });

    $( document ).on( 'change', '.task-checkbox', function( e ){
        e.preventDefault();

        obj.ajax_update_task_status( $(this).val() ).done(function(){
            get_all_tasks();
        });
    });

    obj.add_task_details = function ( $form ){
        return $.ajax( {
            url : obj.base_url + 'admin/add_task',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            success : function( result ) {
                if( result.status ){
                    toastr.info( 'Task Added...' );
                }
                else{
                    toastr.error( "Error in Adding ", "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_update_task_status = function ( id ){
        return $.ajax( {
            url : obj.base_url + 'admin/update_task_status',
            type : 'post',
            dataType : 'json',
            data : {task_id : id},
            success : function( result ) {
                if( result.status ){
                    toastr.success( 'Task completed...', "OREF" );
                }
                else{
                    toastr.error( "Task already completed ", "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function get_all_tasks()
    {
        $.ajax( {
            url : obj.base_url + 'admin/get_all_tasks',
            type : 'get',
            dataType : 'json',
            success : function( result ) {
                var $list = $('.task-list');

                $list.empty();

                if( result instanceof Array)
                {
                    $.each( result, function(key, value){
                        $list.append(task_list_format(result[key].id, result[key].detail, result[key].is_completed));
                    } );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function task_list_format(id, value, is_completed)
    {
        var html = '<div class="checkbox media"> \
                        <div class="media-body"> \
                            <label> \
                                <input type="checkbox" name="task_id" class="task-checkbox" value="' + id + '"' + (parseInt(is_completed) === 1 ? ' checked' : '') + ' /> \
                                <i class="input-helper"></i> \
                                <span>' + value + '</span> \
                            </label> \
                        </div> \
                    </div>';

        return html;
    }

    function get_all_recently_opened_properties()
    {
        $.ajax({
            url : obj.base_url + 'admin/get_all_property_details_opened_recently',
            type : 'get',
            dataType : 'json',
            success : function( result ){
                var $tbody = $( '.property-table' ).find( 'tbody' );

                $tbody.empty();

                if( result.length )
                {

                    $.each( result, function( i, property ){
                        var $tr = $( '<tr />' )

                        $tr
                            .append( '<td class="f-500 c-cyan">' + property.id + '</td>' )
                            .append( '<td>' + property.name + '</td>' )
                            .append( '<td>' + property.date_viewed + '</td>' )
                            .append( '<td class="f-500 c-cyan">' + property.price + '</td>' );

                        $tbody.append( $tr );
                    } );
                }
                else
                {
                    var $tr = $( '<tr />' );

                    $tr.append( '<td colspan="4" class="text-center">No recently viewed properties</td>' );

                    $tbody.append( $tr );
                }
            },
            error: function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function view_agent_format( id, firstname, lastname, avatar, avatar_bg_color, email )
    {
        var html = '<a class="lv-item" href="' + oref.base_url + 'admin/account/agents"> \
                        <div class="media"> \
                            <div class="pull-left">';
                                if(avatar.trim()) {
                                    html += '<img src="' + oref.base_url + avatar + '" alt="avatar" class="img-circle" height="35">';
                                }
                                else {
                                    html += '<div class="lv-avatar bgm-' + avatar_bg_color + '">' + firstname[0].toUpperCase() + '</div>';
                                }
            html +=         '</div> \
                            <div class="media-body"> \
                                <div class="lv-title">' + firstname + ' ' + lastname + '</div> \
                                <small class="lv-small">' + email + '</small> \
                            </div> \
                        </div> \
                    </a>';

        return html;
    }

    function get_agent_details()
    {
        var $agent_list = $( '.agent-list' );

        $.ajax({
            url : obj.base_url + 'admin/get_agents_details',
            type : 'get',
            dataType : 'json',
            success : function ( result ){
                $agent_list.empty();

                if( result instanceof Array)
                {
                    $.each( result, function(key, value){
                        $agent_list.append(view_agent_format(result[key].id, result[key].firstname, result[key].lastname, result[key].avatar, result[key].avatar_bg_color, result[key].email));
                    } );
                }
            },
            error : function( xhr, status )
            {
                console.log( xhr.responseText );
            }
        });
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        // setInterval(get_all_recently_opened_properties, 2000);

        // setInterval(get_all_tasks, 3000);

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            validClass: 'form-valid',
            focusInvalid: true
        });

        $.validator.addMethod("notEqual", function(value, element, param) {
            // return this.optional(element) || value != param;
            return this.optional(element) || value != $( param ).val();
        }, "Value should be different");

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }
} );