$( function( $ ){
    var obj = {};

    init();
    $("#avatar").fileinput({
                    // overwriteInitial: true,
                    showClose: false,
                    showCaption: false,
                    browseLabel: '',
                    removeLabel: '',
                    browseIcon: '<i class="fa fa-image"></i>',
                    removeIcon: '<i class="fa fa-times"></i>',
                    removeTitle: 'Cancel or reset changes',
                    elErrorContainer: '.avatar-errors',
                    msgErrorClass: 'alert alert-block alert-danger',
                    // showUpload: false,
                    // initialPreview: [
                    //    '<img src="' + $('.avatar').attr('src') + '" class="file-preview-avatar" alt="Your Avatar">'
                    // ],
                    // defaultPreviewContent: '<img src="' + $('.avatar').attr('src') + '" class="file-preview-avatar" alt="Your Avatar">',
                    // layoutTemplates: {main2: '{preview} {remove} {browse}'},
                    allowedFileExtensions: ["jpg", "png", "gif", "jpeg"]
                });

    $( document ).on( 'change', '#regions', function(){
        var value = $(this).val(),
            $provinces = $('#provinces');
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $provinces.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $cities.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
        }
        else{
            $provinces.prop('disabled', false);
            $cities.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            ajax_get_provinces(value);
        }
    });

    $( document ).on( 'change', '#provinces', function(){
        var value = $(this).val(),
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $cities.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
        }
        else{
            $cities.prop('disabled', false);
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            ajax_get_cities(value);
        }
    });

    $( document ).on( 'change', '#cities', function(){
        var value = $(this).val(),
            $barangays = $('#barangays');

        if(value === ''){
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
        }
        else{
            $barangays.prop('disabled', false);
            ajax_get_barangays(value);
        }
    });

    $(document).ready(function() {
        $("#price").on("input", function() {
        // allow numbers, a comma or a dot
            var v= $(this).val(), vc = v.replace(/[^0-9,\.]/, '');
            if (v !== vc)
            $(this).val(vc);
        });
    });

    $( '#edit-profile' ).validate({
        ignore : [],
        rules: {
            avatar : {
                required : true
            },
            username : {
                required : true,
                remote : {
                    url : obj.base_url+"admin/is_username_available",
                    type : "post"
                }
            },
            firstname : {
                required : true
            },
            lastname : {
                required : true
            },
            contactno : {
                required : true
            },
            region_id : {
                required : true
            },
            province_id : {
                required : true
            },
            city_id : {
                required : true
            },
            barangay_id : {
                required : true
            }
        },
        messages : {
            avatar : {
                required : "Avatar is required"
            },
            username : {
                required : "Username is required",
                remote : "Username is already exists"
            },
            firstname : {
                required : "Firstname is required"
            },
            lastname : {
                required : "Lastname is required"
            },
            contactno : {
                required : "Contact Number is required"
            },
            region_id : {
                required : "Region is required"
            },
            province_id : {
                required : "Province is required"
            },
            city_id : {
                required : "City is required"
            },
            barangay_id : {
                required : "Barangay is required"
            }
        },
        highlight: function( element, errorClass, validClass ){
            $(element).addClass( errorClass ).removeClass( validClass );
        },
        unhighlight: function( element, errorClass, validClass ){
            $(element).removeClass( errorClass ).addClass( validClass );
        },
        submitHandler: function( form ){
            var $form = $( form );

            obj.ajax_update_admin( $form );
        }
    });

    obj.ajax_update_admin = function ( $form ){
        var $submit = $form.find( '[type="submit"]' ),
            formData = new FormData($form[0]);

        $.ajax( {
            url : obj.base_url + 'admin/update_admin',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    ajax_get_admin_details( $( '#edit-profile' ) );

                    toastr.success( "Profile updated successfully", "OREF" );
                }
                else{
                    toastr.error( "Failed to update", "OREF" );
                }

                $submit.text( 'Update' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function ajax_get_regions(){
        var $regions = $('#regions');

        return $.ajax({
            url : obj.base_url + 'admin/get_region_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $regions.empty();
                    $regions.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].region_id ).text( result[index].short_name );
                        $regions.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_provinces( region_id ){
        var $provinces = $('#provinces');

        return $.ajax({
            url : obj.base_url + 'user/get_province_details',
            type : 'get',
            dataType : 'json',
            data : { region_id : region_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $provinces.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $provinces.empty();
                    $provinces.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].province_id ).text( result[index].long_name );
                        $provinces.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_cities( province_id ){
        var $cities = $('#cities');

        return $.ajax({
            url : obj.base_url + 'user/get_city_details',
            type : 'get',
            dataType : 'json',
            data : { province_id : province_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $cities.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $cities.empty();
                    $cities.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].city_id ).text( result[index].long_name );
                        $cities.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_barangays( city_id ){
        var $barangays = $('#barangays');

        return $.ajax({
            url : obj.base_url + 'user/get_barangay_details',
            type : 'get',
            dataType : 'json',
            data : { city_id : city_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $barangays.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $barangays.empty();
                    $barangays.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].barangay_id ).text( result[index].long_name );
                        $barangays.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_admin_details( $form ){
        $.ajax({
            url : obj.base_url + 'admin/get_admin_details',
            type : 'get',
            dataType : 'json',
            success: function( result ){
                $.each( result, function(index, value){
                    var $field = $form.find( '[name=' + index + ']' );
                        switch( index ){
                            case 'region_id' :
                                $( '[name=' + index + '] option' ).filter( function(){
                                    return ( $(this).val() === result[index] );
                                }).prop('selected', true);
                                $('#provinces').prop('disabled', false);
                                break;
                            case 'province_id' :
                                ajax_get_provinces( result.region_id ).done(function(){
                                    $( '[name=' + index + '] option' ).filter( function(){
                                        return ( $(this).val() === result[index] );
                                    }).prop('selected', true);
                                });
                                $('#cities').prop('disabled', false);
                                break;
                            case 'city_id' :
                                ajax_get_cities( result.province_id ).done( function(){
                                    $( '[name=' + index + '] option' ).filter( function(){
                                        return ( $(this).val() === result[index] );
                                    }).prop('selected', true);
                                });
                                $('#barangays').prop('disabled', false);
                                break;
                            case 'barangay_id' :
                                ajax_get_barangays( result.city_id ).done( function(){
                                    $( '[name=' + index + '] option' ).filter( function(){
                                        return ( $(this).val() === result[index] );
                                    }).prop('selected', true);
                                } );
                                break;
                            case 'tags':
                                $field.val( result[index] );
                                $field.tagsinput( 'refresh' );
                                break;
                            case 'avatar':
                                $( '.avatar' ).attr( 'src', obj.base_url + result[index] );
                                break;
                            default:
                                $field.val(value);
                        }
                } );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.method = $( 'body' ).data( 'method' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        ajax_get_regions().done( function(){
            ajax_get_admin_details( $( '#edit-profile' ) );
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

} );