$( function( $ ){
    var obj = {};

    init();

    $( '[data-bind]').click( function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        obj.method = 'add';
        load_modal( url );
    } );

    obj.property_types_grid = $("#table-property-types").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_all_property_types',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row)
            {
                var $edit = $( '<button />' ),
                    $delete = $( '<button />' );

                $edit
                    .attr({
                        type : 'button',
                        title : 'Edit',
                        class : 'btn bgm-blue command-edit',
                        'data-row-id' : row.id,
                        'data-bind' : 'edit_property_type'
                    })
                    .html( '<span class="zmdi zmdi-edit"></span>' );

                $delete
                    .attr({
                        type : 'button',
                        title : 'Delete',
                        class : 'btn bgm-red command-delete',
                        'data-row-id' : row.id,
                        'data-bind' : 'confirmation'
                    })
                    .html( '<span class="zmdi zmdi-block"></span>' );

                return $edit.outerHTML() + $delete.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        obj.property_types_grid
        .find(".command-edit")
        .on("click", function( e )
        {
            e.preventDefault();

            var type_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'edit';

            load_modal( url, type_id );
        })
        .end()
        .find(".command-delete")
        .on("click", function( e )
        {
            e.preventDefault();

            var type_id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'delete';

            load_modal( url, type_id );
        });
    });

    obj.ajax_add_property_type = function ( $form, $modal ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'admin/add_property_type',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    obj.property_types_grid.bootgrid( 'reload' );
                    $modal.modal( 'hide' );
                    toastr.success( "Added!", "OREF" );
                }
                else{
                    toastr.error( "Failed to add", "OREF" );
                }

                $submit.text( 'Added' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
                placeholder_float();
            },
            error: function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_get_confirmation_content = function( url, $confirm, type_id ){
        $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $confirm.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $confirm.find( '.modal-content' ).html( content ).click( function(){
                    obj.ajax_delete_property_type( $confirm, type_id );
                });
            },
            error: function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_update_property_type = function ( $form, $modal ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'admin/update_property_type',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    obj.property_types_grid.bootgrid( 'reload' );
                    toastr.success('Sucessfully updated type!', "OREF");

                    $modal.modal( 'hide' );
                }
                else{
                    toastr.error( "Failed to update", "OREF" );
                }

                $submit.text( 'Update' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_delete_property_type = function( $confirm, type_id )
    {
        $.ajax({
            url : obj.base_url + 'admin/delete_property_type',
            type : 'post',
            dataType : 'json',
            data : { type_id : type_id },
            success : function( result ){
                if( result.status )
                {
                    obj.property_types_grid.bootgrid( 'reload' );

                    toastr.success( 'Removed!' );
                    $confirm.modal( 'hide' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_property_type_details( $modal, type_id ){
        var $form = $modal.find( 'form' );
        return $.ajax({
            url : obj.base_url + 'admin/get_property_type_details_by_id',
            type : 'get',
            dataType : 'json',
            data : { type_id : type_id },
            success: function( result ){
                $.each( result, function(index, value){
                    $form.find( '[name=' + index + ']' ).val(value);
                } );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function validate( $form, $modal )
    {
        if( $.fn.tagsinput ) $( '[data-bind="tags"]' ).tagsinput( 'refresh' );

        $form.validate({
            rules: {
                type : {
                    required : true
                }
            },
            messages : {
                type : {
                    required : "Type name is required"
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            submitHandler : function(){
                switch(obj.method){
                    case 'add' :
                        obj.ajax_add_property_type( $form, $modal );
                        break;
                    case 'edit' :
                        obj.ajax_update_property_type( $form, $modal )
                        break;
                }
            }
         });
    }

    function load_modal( url, type_id )
    {
        var $modal = $( '#ajaxModal' ),
            $confirm = $( '#confirmation' ),
            type_id = type_id;
            ajax_url = obj.base_url + 'admin/modal/' + url;

        if( url === 'confirmation' ){
            $confirm.modal( {
                show : true,
                backdrop: 'static',
            } );

            obj.ajax_get_confirmation_content( ajax_url, $confirm, type_id );
        }
        else{
            $modal.modal( {
                show : true,
                backdrop: 'static',
            } );

            obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                switch(obj.method){
                    case 'add':
                        validate( $modal.find( 'form' ), $modal );
                        break;
                    case 'edit':
                        ajax_get_property_type_details( $modal, type_id ).done( function(){
                            validate( $modal.find( 'form' ), $modal );
                        } );
                        break;
                }
            });
        }
    }

    function placeholder_float(){
        if($('.fg-line')[0]) {
            $('body').on('focus', '.form-control', function(){
                $(this).closest('.fg-line').addClass('fg-toggled');
            })

            $('body').on('blur', '.form-control', function(){
                var p = $(this).closest('.form-group');
                var i = p.find('.form-control').val();

                if (p.hasClass('fg-float')) {
                    if (i.length == 0) {
                        $(this).closest('.fg-line').removeClass('fg-toggled');
                    }
                }
                else {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            });
        }

        //Add blue border for pre-valued fg-flot text feilds
        if($('.fg-float')[0]) {
            $('.fg-float .form-control').each(function(){
                var i = $(this).val();

                if (!i.length == 0) {
                    $(this).closest('.fg-line').addClass('fg-toggled');
                }

            });
        }
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<div>").append(this.eq(0).clone()).html();
    };
});