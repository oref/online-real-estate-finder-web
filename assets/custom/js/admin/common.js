var oref = {};
    oref.base_url = $( 'body' ).data( 'url' );
    oref.loader = '<img src="'+ oref.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';
    oref.msg_counter = 0;
    oref.processing = false;
    oref.conversations = [];

$( function(){
    /*
     * Detect Mobile Browser
     */
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $('html').addClass('ismobile');
    }

    $( document ).on( 'click', '.message-notification-list > .lv-item', function( e ){
        e.preventDefault();
        var index = $(this).index();

        $.cookie( 'conversation_index', index, { path: '/' } );
        document.location = oref.base_url + 'admin/messages';
    } );

    $('#message-dropdown').on('shown.bs.dropdown', function () {
        $('.tmn-counts.messages').text( null ).addClass( 'hide' );
        get_all_message_notification();
    });

    save_recent_message().done(function(){
        setInterval( get_recent_message, 5000 );
    });
} );

function table_basic(selector){
	$(selector).bootgrid({
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
    });
}

function table_selection(selector){
	$(selector).bootgrid({
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        selection: true,
        multiSelect: true,
        rowSelect: true,
        keepSelection: true
    });
}

function message_notification_format( name, avatar, avatar_bg_color, msg ) {

    var html = '<a href="#" class="lv-item media"> \
                    <div class="lv-avatar pull-left">';
            if(avatar.trim()) {
                html += '<img src="' + oref.base_url + avatar + '" alt="avatar">';
            }
            else {
                html += '<div class="lv-avatar bgm-' + avatar_bg_color + '">' + name[0].toUpperCase() + '</div>';
            }
        html +=    '</div> \
                    <div class="media-body"> \
                        <div class="lv-title">' + name + '</div> \
                        <div class="lv-small">' + msg + '</div> \
                    </div> \
                </a>';

    return html;
}

function save_recent_message() {
    return $.ajax({
        url : oref.base_url + 'admin/get_recent_message',
        type : 'get',
        dataType : 'json',
        success : function( result ){
            if( result.hasOwnProperty )
            {
                oref.conversations.push( parseInt( result.msg_id ) );
            }
        },
        error : function( xhr, status ){
            console.log( xhr.responseText );
        },
    });
}

function get_recent_message() {
    $.ajax({
        url : oref.base_url + 'admin/get_recent_message',
        type : 'get',
        dataType : 'json',
        success : function( result ){
            if( result.hasOwnProperty && $.inArray( parseInt( result.msg_id ), oref.conversations) === -1 && result.current_id !== result.sender_id )
            {
                var sender,
                    $title = $( 'title' );

                oref.conversations.push( parseInt(result.msg_id) );
                oref.msg_counter++;
                $title.text( '(' + oref.msg_counter + ') ' + $title.text() );
                $( '.tmn-counts.messages' ).text(oref.msg_counter).removeClass( 'hide' );
                toastr.options.positionClass = 'toast-top-left';
                toastr.options.timeOut = 10000;
                toastr.options.extendedTimeOut = 10000;

                if(result.from_id === result.current_id){
                    sender = result.to_full_name;
                }
                else{
                    sender = result.from_full_name;
                }

                toastr.options.onclick = function(){
                    document.location = oref.base_url + 'admin/messages';
                }
                toastr.info(result.msg, sender);
            }
        },
        error : function( xhr, status ){
            console.log( xhr.responseText );
        },
    });
}

function get_all_message_notification() {

    var $message_notification_list = $( '.message-notification-list' );

    return $.ajax({
        url : oref.base_url + 'admin/get_all_conversations',
        type : 'get',
        dataType : 'json',
        beforeSend : function(){
            $message_notification_list.html( oref.loader );
        },
        success : function( result ){
            $message_notification_list.empty();

            if( result.conversations instanceof Array )
            {
                $.each( result.conversations, function( index, value ){

                    if( result.conversations[index].from_id === result.id )
                    {
                        $message_notification_list.append( message_notification_format( result.conversations[index].to_full_name, result.conversations[index].to_avatar, result.conversations[index].to_avatar_bg_color, result.conversations[index].msg ) );
                    }
                    else if( result.conversations[index].to_id === result.id )
                    {
                        $message_notification_list.append( message_notification_format( result.conversations[index].from_full_name, result.conversations[index].from_avatar, result.conversations[index].from_avatar_bg_color, result.conversations[index].msg ) );
                    }
                } );
            }
            else
            {
                $message_notification_list.html('<h5 class="text-center">No message to display</h5>');
            }
        },
        error : function( xhr, status ){
            console.log( xhr.responseText );
        }
    });
}