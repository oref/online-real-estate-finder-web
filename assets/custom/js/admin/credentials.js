$( function( $ ){
    var obj = {};

    init();

    $( '[data-form="password"]' ).validate({
        rules: {
                old_password : {
                    required : true,
                    remote : {
                        url : obj.base_url+"admin/is_password_match",
                        type : "post"
                    }
                },
                new_password : {
                    required : true
                },
                confirm_password: {
                    required : true,
                    equalTo : '#new_password'
                }

        },
        messages : {
            old_password : {
                required : "Current Password is required",
                remote : "Use your current password"
            },
            new_password : {
                required : "New Password is required"
            },
            confirm_password : {
                required : "Confirm Password is required",
                equalTo : 'Password does not match'
            }
        },
        highlight: function( element, errorClass, validClass ){
            $(element).addClass( errorClass ).removeClass( validClass );
        },
        unhighlight: function( element, errorClass, validClass ){
            $(element).removeClass( errorClass ).addClass( validClass );
        },
        submitHandler: function( form ){
            var $form = $( form );

            obj.ajax_update_admin_password( $form );
        }
    });

    obj.ajax_update_admin_password = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'admin/update_admin_password',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    toastr.success( "Profile updated successfully", "OREF" );
                }
                else{
                    toastr.error( "Failed to update", "OREF" );
                }

                $submit.text( 'Update' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

} );