$( function( $ ){
    var obj = {};

    init();

    $( '[data-bind]').click( function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        obj.method = 'add';

        load_modal( url );
    } );

    $( document ).on( 'change', '#regions', function(){
        var value = $(this).val(),
            $provinces = $('#provinces');
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $provinces.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $cities.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
        }
        else{
            $provinces.prop('disabled', false);
            $cities.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            ajax_get_provinces(value);
        }
    });

    $( document ).on( 'change', '#provinces', function(){
        var value = $(this).val(),
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $cities.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
        }
        else{
            $cities.prop('disabled', false);
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
            ajax_get_cities(value);
        }
    });

    $( document ).on( 'change', '#cities', function(){
        var value = $(this).val(),
            $barangays = $('#barangays');

        if(value === ''){
            $barangays.prop('disabled', true).empty().append('<option default value="none">-Select-</option>');
        }
        else{
            $barangays.prop('disabled', false);
            ajax_get_barangays(value);
        }
    });

    obj.agent_grid = $("#table-agent").bootgrid({
        ajax: true,
        url : obj.base_url + 'admin/get_user_details_by_type/agent',
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row)
            {
                var $view = $( '<button />' ),
                    $message = $( '<button/>' ),
                    $del = $( '<button />' );

                $view
                    .attr({
                        type : 'button',
                        class : 'btn bgm-blue command-view',
                        'data-row-id' : row.id,
                        'data-bind' : 'view_user_details'
                    })
                    .html( '<span class="zmdi zmdi-eye"></span>' );

                $message
                    .attr({
                        type : 'button',
                        class : 'btn bgm-green command-message',
                        'data-row-id' : row.id,
                        'data-bind' : 'send_message'
                    })
                    .html( '<span class="zmdi zmdi-comments"></span>' );

                $del
                    .attr({
                        type : 'button',
                        class : 'btn bgm-red command-block',
                        'data-row-id' : row.id,
                        'data-bind' : 'confirmation'
                    })
                    .html( '<span class="zmdi zmdi-block"></span>' );

                return $view.outerHTML() + $message.outerHTML() + $del.outerHTML();
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        /* Executes after data is loaded and rendered */
        obj.agent_grid
        .find(".command-view")
        .on("click", function( e )
        {
            e.preventDefault();

            var id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method ='view';

            load_modal( url, id );
        })
        .end()
        .find(".command-message")
        .on("click", function( e )
        {
            e.preventDefault();

            var id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'message';

            load_modal( url, id );
        })
        .end()
        .find(".command-block")
        .on("click", function( e )
        {
            e.preventDefault();

            var id = $(this).data('row-id'),
                url = $( this ).data( 'bind' );

            obj.method = 'delete';

            load_modal( url, id );
        });
    });

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_add_agent = function( $form, $modal ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax({
            url : obj.base_url + 'admin/add_user_by_type/agent',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend: function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success: function( response ){
                if( response.status )
                {
                    obj.agent_grid.bootgrid( 'reload' );
                    $modal.modal( 'hide' );
                    toastr.success( 'Success! User Added...', "OREF" );
                }
                else
                {
                    toastr.error( 'Unable to process your request...', "OREF" );
                }

                $submit.text( 'Processing...' ).prop( 'disabled', false );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_delete_user_by_id = function( $confirm, id )
    {
        $.ajax({
            url : obj.base_url + 'admin/delete_user_by_id',
            type : 'post',
            dataType : 'json',
            data : { id : id },
            success : function( result ){
                if( result.status )
                {
                    obj.agent_grid.bootgrid( 'reload' );

                    toastr.success( 'Removed!' );
                    $confirm.modal( 'hide' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_get_confirmation_content = function( url, $confirm ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $confirm.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $confirm.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    function confirmation( $form, $confirm, id )
    {
        $form.validate({
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            errorPlacement: function( erorr, element ){
                return false;
            },
            submitHandler: function(){
                switch(obj.method){
                    case 'delete' :
                        obj.ajax_delete_user_by_id( $confirm, id );
                        break;
                }
            }
        });
    }

    function ajax_get_regions(){
        var $regions = $('#regions');

        return $.ajax({
            url : obj.base_url + 'admin/get_region_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $regions.empty();
                    $regions.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].region_id ).text( result[index].short_name );
                        $regions.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_provinces( region_id ){
        var $provinces = $('#provinces');

        return $.ajax({
            url : obj.base_url + 'user/get_province_details',
            type : 'get',
            dataType : 'json',
            data : { region_id : region_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $provinces.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $provinces.empty();
                    $provinces.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].province_id ).text( result[index].long_name );
                        $provinces.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_cities( province_id ){
        var $cities = $('#cities');

        return $.ajax({
            url : obj.base_url + 'user/get_city_details',
            type : 'get',
            dataType : 'json',
            data : { province_id : province_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $cities.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $cities.empty();
                    $cities.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].city_id ).text( result[index].long_name );
                        $cities.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_barangays( city_id ){
        var $barangays = $('#barangays');

        return $.ajax({
            url : obj.base_url + 'user/get_barangay_details',
            type : 'get',
            dataType : 'json',
            data : { city_id : city_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $barangays.append($loader);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $barangays.empty();
                    $barangays.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].barangay_id ).text( result[index].long_name );
                        $barangays.append( $option );
                    });

                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function get_admin_details( id, $modal ){
        return $.ajax({
            url : obj.base_url + 'admin/get_admin_details',
            type : 'get',
            dataType : 'json',
            data : { id : id },
            success: function( result ){
                $.each( result, function(index, value){
                    switch(obj.method){
                        case 'view':
                            var $field = $( '.' + index );

                            if( index === 'avatar' ){
                                $field.attr('src', obj.base_url + value);
                            }
                            else{
                                $field.text( value );
                            }
                            break;
                        case 'message':
                            var $form = $modal.find( 'form' );
                            var $field = $form.find( '[name=' + index + ']' );

                            $field.val( value );
                            $field.prop('disabled', true);
                            break;
                    }
                } );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function validate( $form, $modal )
    {
        $form.validate({
            ignore : [],
            rules: {
                firstname : {
                    required : true
                },
                lastname : {
                    required : true
                },
                region_id : {
                    required : true
                },
                province_id : {
                    required : true
                },
                city_id : {
                    required : true
                },
                barangay_id : {
                    required : true
                },
                email : {
                    email : true,
                    required : true
                },
                contactno : {
                    required : true
                }
            },
            highlight: function( element, errorClass, validClass ){
                $(element).addClass( errorClass ).removeClass( validClass );
            },
            unhighlight: function( element, errorClass, validClass ){
                $(element).removeClass( errorClass ).addClass( validClass );
            },
            errorPlacement: function( erorr, element ){
                return false;
            },
            submitHandler: function(){
                obj.ajax_add_agent( $form, $modal );
            }
        });
    }

    function load_modal( url, id )
    {
        var $modal = $( '#ajaxModal' ),
            $confirm = $( '#confirmation' ),
            ajax_url = obj.base_url + 'admin/modal/' + url;

        if( url === 'confirmation' ){
            $confirm.modal( {
                show : true,
                backdrop: 'static',
            } );

            obj.ajax_get_confirmation_content( ajax_url, $confirm ).done( function(){
                confirmation( $confirm.find( 'form' ), $confirm, id );
            } );
        }
        else{
            $modal.modal( {
                show : true,
                backdrop: 'static',
            } );

            switch( obj.method ){
                case 'view':
                    obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                        get_admin_details( id );
                    } );
                    break;
                case 'message':
                    obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                        get_admin_details( id, $modal ).done( function(){
                            validate( $modal.find( 'form' ), true );
                        } );
                    } );
                    break;
                case 'add':
                    obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
                        validate( $modal.find( 'form' ), $modal );
                        ajax_get_regions();
                    } );
                    break;
            }
        }

    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        jQuery.validator.addMethod("notEqual", function(value, element, param) {
            // return this.optional(element) || value != param;
            return this.optional(element) || value != $( param ).val();
        }, "Value should be different");

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<div>").append(this.eq(0).clone()).html();
    };
});