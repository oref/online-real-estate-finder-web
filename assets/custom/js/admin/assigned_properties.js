$( function( $ ){
    var obj = {};

    init();
    get_all_assigned_properties();

    $( document ).on('click', '[data-bind]', function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' )
            property_id = $( this ).data('property-id');

        load_modal( url, property_id );
    } );

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    function get_all_assigned_properties()
    {
        var $assigned_property = $( '.assigned_property' );

        $.ajax({
            url : obj.base_url + 'admin/get_all_property_assigned',
            type : 'get',
            dataType : 'json',
            success : function ( result ){
                $assigned_property.empty();


                if( result instanceof Array)
                {
                    $.each( result, function(key, value){
                        $assigned_property.append(view_assigned_property_format(result[key].id, result[key].name, result[key].price, result[key].address, result[key].date_added));
                    } );
                }
            },
            error : function( xhr, status )
            {
                console.log( xhr.responseText );
            }
        });
    }

    function view_assigned_property_format( id, name, price, address, date_added )
    {
        var html = '<div class="contacts c-profile"> \
                        <div class="col-md-3 col-sm-4 col-xs-6"> \
                            <div class="c-item"> \
                                <div class="c-info"> \
                                    <strong>' + name + '</strong> \
                                    <small>' + price + '</small> \
                                    <small>' + address + '</small> \
                                    <small>' + date_added + '</small> \
                                </div> \
                                <div class="c-footer"> \
                                    <button class="waves-effect" data-bind="view_property_details" data-property-id="' + id + '" > \
                                        <i class="zmdi zmdi-face-add"></i>Details \
                                    </button> \
                                </div> \
                            </div> \
                        </div> \
                    </div>';

        return html;
    }

    function ajax_get_property_details( property_id, $modal ){
        var $form = $modal.find( 'form' );

        return $.ajax({
            url : obj.base_url + 'admin/get_property_details_by_id',
            type : 'get',
            dataType : 'json',
            data : { property_id : property_id },
            success: function( result ){

                if( result instanceof Object ) {
                    $.each( result, function(index, value){
                        var $field = $form.find( '[name=' + index + ']' );

                        switch(index){
                            case 'region_id' :
                                $field.val( result.region );
                                break;
                            case 'province_id' :
                                $field.val( result.province );
                                break;
                            case 'city_id' :
                                $field.val( result.city );
                                break;
                            case 'barangay_id' :
                                $field.val( result.barangay );
                                break;
                            case 'type_id' :
                                $field.val( result.type );
                                break;
                            case 'images' :
                                var $property_images = $( '.property-images' ),
                                    html = '';

                                $property_images.empty();
                                $.each(result.images, function(index, val) {
                                    html += '<div class="col-xs-6 col-md-3">';
                                    html += '<a href="' + obj.base_url + result.images[index].path + '" class="thumbnail image-link">';
                                    html += '<img src="' + obj.base_url + result.images[index].path + '" />';
                                    html += '</a>';
                                    html += '</div>';
                                });

                                $property_images.append( html );
                                break;
                            default :
                                $field.val( value );
                        }
                        $field.prop('disabled', true);
                    } );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function load_modal( url, property_id )
    {
        var $modal = $( '#ajaxModal' ),
            property_id = property_id,
            ajax_url = obj.base_url + 'admin/modal/' + url;

        $modal.modal( {
            show : true,
            backdrop: 'static',
        } );

        obj.ajax_get_modal_content( ajax_url, $modal ).done( function(){
            ajax_get_property_details( property_id ,$modal ).done( function(){
                $('.property-images').find( 'a' ).magnificPopup({
                  // delegate : 'a',
                  type: 'image',
                  gallery: {
                    enabled:true
                  }
                  // other options
                });
            } );
        } );
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';

        jQuery.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        jQuery.validator.addMethod("notEqual", function(value, element, param) {
            // return this.optional(element) || value != param;
            return this.optional(element) || value != $( param ).val();
        }, "Value should be different");

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    $.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<div>").append(this.eq(0).clone()).html();
    }
});