$( function( $ ){
    var obj = {};

    init();

    $('#filmstrip').filmstrip({
        interval: 3000
    });

    $('.property-list').niceScroll({
        cursorcolor: 'rgba(0, 0, 0, 0.298039)'
    });

    $('.selectpicker').selectpicker();

    $('.link-action a').tooltip();
    $('.btn-icon').tooltip();
    $('.social-media a').tooltip();
    $('.filter-gridlist a').tooltip();

    $('input[type="checkbox"]').checkbox({
                    checkedClass: 'fa fa-check-square',
                    uncheckedClass: 'fa fa-square-o'
                });

    $( '.print' ).click(function() {
        window.print();
    });

    // List - Grid Swticher
    $("a.switcher").bind("click", function(e){
        e.preventDefault();
        var theid = $(this).attr("id"),
            thewrapper = $("#product-list"),
            classNames = $(this).attr('class').split(' ');

        if($(this).hasClass("active")) {
            // if currently clicked button has the active class
            // then we do nothing!
            return false;
        } else {
            // otherwise we are clicking on the inactive button
            // and in the process of switching views!
            if(theid == "gridview") {
                $(this).addClass("active");
                $("#listview").removeClass("active");
                // remove the list class and change to grid
                thewrapper.removeClass("list-layout");
                thewrapper.addClass("grid-layout");
            }
            else if(theid == "listview") {
                $(this).addClass("active");
                $("#gridview").removeClass("active");
                // remove the grid view and change to list
                thewrapper.removeClass("grid-layout")
                thewrapper.addClass("list-layout");
            }
        }
    });

    $( '.btn-search' ).click( function( e ){
        e.preventDefault();

        get_current_page();
    } );

    $( 'select[name="sort"]' ).change( function( e ){
        obj.filter.sort = $( this ).val();

        get_current_page();
    } );

    $( 'select[name="offset"]' ).change( function( e ){
        obj.filter.offset = $( this ).val();

        get_current_page();
    } );

    $( document ).on( 'click', '[data-bind="login"], [data-bind="register"], [data-bind="recover"]',  function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    $( document ).on( 'click', '.contact-agent', function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    $( document ).on( 'click', '.modal-close', function( e ){
        e.preventDefault();

        $( this ).parents( '.modal' ).modal( 'hide' );
    } );

    $( document ).on( 'click', '.pagination a', function( e ){
        e.preventDefault();
        var $this = $(this);

        obj.filter.page = $( this ).data('page');

        if( ! $this.parent().is('.active, .disabled') )
        {
            get_current_page().done(function(){
                $('[data-page=' + obj.filter.page + ']').parent().removeClass('active');
                $(this).parent().addClass('active');
            });
        }
    } );

    $( document ).on( 'click', '.save-favorite', function( e ){
        e.preventDefault();

        var $this = $( this ),
            property_id = $( this ).data( 'property-id' );

        if( obj.is_user_logged_in )
        {
            if( $this.hasClass( 'active' ) )
            {
                obj.ajax_delete_favorite_property( $this, property_id );
            }
            else
            {
                obj.ajax_add_favorite_property( $this, property_id );
            }
        }
        else{
            toastr.error( 'You are not logged in.', "OREF" );

            load_modal( 'login' );
        }
    } );

    $( document ).on( 'change', '#regions', function(){
        var value = $(this).val(),
            $provinces = $('#provinces');
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $provinces.prop('disabled', true).empty();
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $provinces.prop('disabled', false);
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            ajax_get_provinces(value);
        }
    });

    $( document ).on( 'change', '#provinces', function(){
        var value = $(this).val(),
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $cities.prop('disabled', false);
            $barangays.prop('disabled', true).empty();
            ajax_get_cities(value);
        }
    });

    $( document ).on( 'change', '#cities', function(){
        var value = $(this).val(),
            $barangays = $('#barangays');

        if(value === ''){
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $barangays.prop('disabled', false);
            ajax_get_barangays(value);
        }
    });

    $('#msg-form').validate({
        ignore: ":hidden:not(#to_id)",
        rules: {
            msg : {
                required : true
            }
        },
        messages: {
            msg : {
                required : "Message is required"
            }
        },
        highlight: function( element, errorClass, validClass ){
            $(element).addClass( errorClass ).removeClass( validClass );
        },
        unhighlight: function( element, errorClass, validClass ){
            $(element).removeClass( errorClass ).addClass( validClass );
        },
        errorPlacement: function( erorr, element ){
            erorr.insertAfter(element);
        },
        submitHandler: function( form ){
            var $form = $( form );

            if( obj.is_user_logged_in )
                {
                    obj.ajax_add_message( $form );
                }
                else{
                    toastr.error( 'You are not logged in.', "OREF" );

                    load_modal( 'login' );
                }
        }
    });

    obj.ajax_add_favorite_property = function( $elem, property_id ){
        $.ajax({
            url : obj.base_url + 'user/add_favorite_property',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    toastr.success( 'Saved!' );
                    get_current_page();
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_delete_favorite_property = function( $elem, property_id ){
        $.ajax({
            url : obj.base_url + 'user/delete_favorite_property',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    get_current_page();
                    $elem.parents('.property-content').remove();
                    toastr.success( 'Removed!' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_add_message = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        return $.ajax( {
            url : obj.base_url + 'user/add_message',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status )
                {
                    toastr.success( "Message sent", "OREF" );
                    window.location.reload();
                }
                else{
                    toastr.error( "Unable to send your message", "OREF" );
                }

                $submit.prop( 'disabled', false );
            },
            error : function( xhr, status ){

                $submit.prop( 'disabled', false );
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_signin = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'user/signin',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status )
                {
                    location.reload();
                }
                else{
                    toastr.error( "Incorrect username/password", "OREF" );
                }

                $submit.text( 'Log In' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_signup = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );
        $.ajax( {
            url : obj.base_url + 'user/signup',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Please wait...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status ){
                    $.cookie('email', $( '#email' ).val(), { path: '/' });

                    toastr.options.timeOut = 0;
                    toastr.options.extendedTimeOut = 0;
                    toastr.info( 'Please check the confirmation link we sent to your email or click <a href="#" data-bind="resend">here</a> to resend the confirmation link.', "OREF" );

                    $form.parents( '.modal' ).modal( 'hide' );
                }
                else{
                    toastr.error( "Unable to process your request. Please Check your internet connection.", "OREF" );
                }

                $submit.text( 'Log In' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                toastr.error("Unable to process your request", "OREF");

                $submit.text( 'Log In' ).prop( 'disabled', false );
            }
        } );
    }

    obj.ajax_resend_confirmation_link = function(){
        $.ajax( {
            url : obj.base_url + 'user/resend',
            type : 'post',
            dataType : 'json',
            data : { email : $.cookie('email') },
            beforeSend : function(){
                toastr.info( "Re-sending the verification link. Please wait.", "OREF" );
            },
            success : function( result ) {
                if(result.status){
                    toastr.remove();
                    toastr.options.timeOut = 5000;
                    toastr.options.extendedTimeOut = 5000;
                    toastr.info( "Please check your email now. We already sent a new verification link.", "OREF" );
                }
                else{
                    toastr.error( "Unable to process your request.", "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_reset_password = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );
        $.ajax( {
            url : obj.base_url + 'user/reset',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Please wait...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    toastr.info( "Please check your email for the new password.", "OREF" );

                    $form.parents( '.modal' ).modal( 'hide' );
                }
                else{
                    toastr.error( "Unable to process your request...", "OREF" );
                }

                $submit.text( 'Log In' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function load_modal( url )
    {
        var $modal = $( '#ajaxModal' );

        $modal.modal( {
            show : true,
            backdrop: 'static',
        } );

        obj.ajax_get_modal_content( obj.base_url + 'user/modal/' + url, $modal ).done(function(){
            validate();
        });
    }

    function refresh(){
        $('.selectpicker').selectpicker('refresh');
    }

    function ajax_get_regions(){
        var $regions = $('#regions');

        return $.ajax({
            url : obj.base_url + 'user/get_region_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', 'none').text('Fetching');
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $regions.empty();
                    $regions.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].region_id ).text( result[index].short_name );
                        $regions.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_provinces( region_id ){
        var $provinces = $('#provinces');

        return $.ajax({
            url : obj.base_url + 'user/get_province_details',
            type : 'get',
            dataType : 'json',
            data : { region_id : region_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $provinces.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $provinces.empty();
                    $provinces.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].province_id ).text( result[index].long_name );
                        $provinces.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_cities( province_id ){
        var $cities = $('#cities');

        return $.ajax({
            url : obj.base_url + 'user/get_city_details',
            type : 'get',
            dataType : 'json',
            data : { province_id : province_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $cities.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $cities.empty();
                    $cities.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].city_id ).text( result[index].long_name );
                        $cities.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_barangays( city_id ){
        var $barangays = $('#barangays');

        return $.ajax({
            url : obj.base_url + 'user/get_barangay_details',
            type : 'get',
            dataType : 'json',
            data : { city_id : city_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $barangays.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $barangays.empty();
                    $barangays.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].barangay_id ).text( result[index].long_name );
                        $barangays.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_property_types(){
        var $types = $('#types');

        return $.ajax({
            url : obj.base_url + 'user/get_property_types_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $types.empty();
                    $types.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].id ).text( result[index].type );
                        $types.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function get_current_page(){
        if(obj.controller === 'front' && obj.method === 'listing') {
            return ajax_get_properties_by_search()
        }
        else if(obj.controller === 'user' && obj.method === 'favorites') {
            return ajax_get_favorites_by_search();
        }
    }

    function get_page_options(){
        return '&sort=' + obj.filter.sort + '&page=' + obj.filter.page + '&offset=' + obj.filter.offset;
    }

    function paginate(item_per_page, current_page, total_records, total_pages)
    {
        var pagination = '';

        pagination += '<ul class="pagination">';

        right_link    = current_page + 3;
        previous       = current_page - 3; //previous link
        next           = current_page + 1; //next link
        first_link     = true; //boolean var to decide our first link

        previous_link = (previous === 0)? 1: previous;
        pagination += '<li' + (current_page > 1 ? '' : ' class="disabled"') + '><a href="#" class="first" data-page="1" title="First"><span class="fa fa-angle-double-left"></span></a></li>'; //first link
        pagination += '<li' + (current_page > 1 ? '' : ' class="disabled"') + '><a href="#" class="previous" data-page="'+previous_link+'" title="Previous"><span class="fa fa-angle-left"></span></a></li>'; //previous link
            for(i = (current_page-2); i < current_page; i++){ //Create left-hand side links
                if(i > 0){
                    pagination += '<li><a href="#" data-page="'+i+'" title="Page'+i+'">'+i+'</a></li>';
                }
            }
        first_link = false; //set first link to false

        if(first_link){ //if current active page is first link
            pagination += '<li class="active"><a href="#" class="first" title="Current Page">'+current_page+'</a></li>';
        }
        else if(current_page == total_pages){ //if it's the last active link
            pagination += '<li class="active"><a href="#" class="last" title="Current Page">'+current_page+'</a></li>';
        }
        else{ //regular current link
            pagination += '<li class="active"><a href="#" title="Current Page">'+current_page+'</a></li>';
        }

        for(i = current_page+1; i < right_link ; i++){ //create right-hand side links
            if(i <= total_pages){
                pagination += '<li><a href="#" data-page="'+i+'" title="Page '+i+'">'+i+'</a></li>';
            }
        }

        next_link = (i > total_pages) ? total_pages : i;
        pagination += '<li' + (current_page < total_pages ? '' : ' class="disabled"') + '><a href="#" class="next" data-page="'+next_link+'" title="Next"><span class="fa fa-angle-right"></span></a></li>'; //next link
        pagination += '<li' + (current_page < total_pages ? '' : ' class="disabled"') + '><a href="#" class="last" data-page="'+total_pages+'" title="Last"><span class="fa fa-angle-double-right"></span></a></li>'; //last link

        pagination += '</ul>';

        return pagination; //return pagination links
    }

    function ajax_get_favorites_by_search(){
        var $property_list = $('.property-list'),
            $pagination_list = $('.pagination-list'),
            $counter = $( '#filtered_property_count' ),
            data = get_page_options();

        return $.ajax( {
            url : obj.base_url + 'user/favorite_list',
            type : 'get',
            dataType : 'json',
            data : data,
            beforeSend : function(){
                $property_list.html(obj.loader);
            },
            success : function( result ) {
                $property_list.empty();
                $pagination_list.empty();

                $( '#total_property_count' ).text( result.total );

                if(result.properties instanceof Array){

                    obj.filter.total = result.total;
                    obj.filter.pages = Math.ceil(obj.filter.total/obj.filter.offset);

                    $counter.text( ((obj.filter.page - 1) * obj.filter.offset) + result.properties.length );
                    $pagination_list.html(paginate( obj.filter.offset, obj.filter.page, obj.filter.total, obj.filter.pages ));

                    $.each( result.properties, function(index, value){
                        $property_list.append(search_result_format(result.properties[index].id, result.properties[index].name, result.properties[index].address, result.properties[index].bedrooms, result.properties[index].bathrooms, result.properties[index].price, result.properties[index].images, result.properties[index].agent_id));
                    } );

                    $('.pagination a, .save-favorite, .contact-agent').tooltip();
                }
                else{
                    $counter.text( 0 );
                    $property_list.html('<div class="col-sm-12 col-md-12 the-title text-center"><h3>Oops! nothing to display</h3></div>');
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function get_total_property(total)
    {
        var count = ((obj.filter.page - 1) * obj.filter.offset) + total;

        return isNaN(count) ? total : count;
    }

    function ajax_get_properties_by_search(){
        var $form = $('.form-search'),
            $submit = $form.find( '[type="submit"]' ),
            $property_list = $('.property-list'),
            $pagination_list = $('.pagination-list'),
            $counter = $( '#filtered_property_count' ),
            data = $form.serialize() + get_page_options();

        return $.ajax( {
            url : obj.base_url + 'user/property_list',
            type : 'get',
            dataType : 'json',
            data : data,
            beforeSend : function(){
                $property_list.html(obj.loader);
                $submit.text( 'Fetching data...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                $property_list.empty();
                $pagination_list.empty();

                $( '#total_property_count' ).text( result.properties.length );

                if(result.properties instanceof Array){
                    var favorites = extract_favorite_properties(result.favorites);

                    obj.filter.total = result.total;
                    obj.filter.pages = Math.ceil(obj.filter.total/obj.filter.offset);

                    $counter.text( get_total_property(result.properties.length) );
                    $pagination_list.html(paginate( obj.filter.offset, obj.filter.page, obj.filter.total, obj.filter.pages ));

                    $.each( result.properties, function(index, value){
                        $property_list.append(search_result_format(result.properties[index].id, result.properties[index].name, result.properties[index].address, result.properties[index].bedrooms, result.properties[index].bathrooms, result.properties[index].price, result.properties[index].images, result.properties[index].agent_id, favorites ));
                    } );

                    $('.pagination a, .save-favorite, .contact-agent').tooltip();
                }
                else{
                    $counter.text( 0 );
                    $property_list.html('<div class="col-sm-12 col-md-12 the-title text-center"><h3>Oops! nothing to display</h3></div>');
                }

                $submit.text( 'Refine Search' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                alert( xhr.responseText );

                $submit.text( 'Refine Search' ).prop( 'disabled', false );
            }
        } );
    }

    function extract_favorite_properties( data ) {
        var property_ids = [];

        if( data instanceof Array ) {
            for( var i = 0 ; i < data.length ; i++ ) {
                property_ids.push(data[i].property_id);
            }
        }

        return property_ids;
    }

    function search_result_format(id, name, address, bedrooms, bathrooms, price, images, agent_id, favorites ) {
        var html = '<div class="prop-item col-sm-6 col-md-4 property-content"> \
                        <div class="thumbnail"> \
                            <div class="thumbnail-img thumbscrubber"> \
                                <span class="ts-inner">';
                                    $.each(images, function(index, val) {
                                        html += '<img class="ts-currslide" src="' + obj.base_url + images[index].path + '" />';
                                    });
            html +=             '</span> \
                            </div> \
                                <div class="thumbnail-body"> \
                                    <div class="caption text-nowrap"> \
                                        <a href="' + obj.base_url + 'property/single/' + id + '" data-id="' + id + '"> \
                                            <h3>' + name + '</h3> \
                                            <span title="' + address + '"> \
                                            <i class="fa fa-map-marker"></i> ' + address + '</span> \
                                            <div class="prop-price">' + price + '</div> \
                                            <p> \
                                                Cupcake ipsum dolor. Sit amet pie wafer sugar plum oat cake lemon drops donut cookie. Soufflé gummi bears muffin pudding. \
                                            </p> \
                                        </a> \
                                    </div> \
                                    <div class="content clearfix"> \
                                        <ul class="list-unstyled feature-list"> \
                                            <li>' + bedrooms + ' Bedrooms</li> \
                                            <li>' + bathrooms + ' Baths</li> \
                                        </ul> \
                                        <div class="link-action clearfix"> \
                                            <a href="#" class="col-md-12 col-sm-12 col-xs-12 save-favorite' + (($.inArray(id, favorites) !== -1 || (obj.controller === 'user' && obj.method === 'favorites')) ? ' active' : '') + '" data-property-id="' + id + '" title="' + (($.inArray(id, favorites) !== -1 || (obj.controller === 'user' && obj.method === 'favorites')) ? 'Remove from' : 'Add to') + ' favorites"> \
                                                <i class="fa fa-star-o"></i> \
                                            </a> \
                                        </div> \
                                    </div> \
                                </div> \
                            </div> \
                        </div>';

        return html;
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.controller = $( 'body' ).data( 'controller' );
        obj.method = $( 'body' ).data( 'method' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';
        obj.is_user_logged_in = $.cookie( 'is_user_logged_in' ) ? $.cookie( 'is_user_logged_in' ) : false;

        obj.filter = {
            sort : 'date_added|desc',
            offset : 9,
            page : 1
        }

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            validClass: 'form-valid',
            focusInvalid: true
        });

        if( obj.method === 'single' )
        {
            obj.map = new GMaps({
                            div: '#gmap-menu',
                            lat: 0,
                            lng: 0
                        });
            GMaps.geocode({
                address: $( '#property-address' ).text().trim(),
                callback: function(results, status){
                    if(status=='OK'){
                        var latlng = results[0].geometry.location;
                        obj.map.setCenter(latlng.lat(), latlng.lng());
                        obj.map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            infoWindow: {
                              content: '<h3>' + $( '#property-name' ).text().trim() + '</h3>'
                            }
                        });
                    }
                }
            });

            add_update_property_view_recently( getSinglePropertyId() );
        }

        if( $.cookie('search') )
        {
            var result = $.parseJSON($.cookie('search')),
                $form = $('.form-search');

            $.removeCookie( 'search', { path: '/' });

            $.each( result, function(index, value){
                var $field = $form.find( '[name=' + index + ']' );

                switch( index ){
                    case 'region_id' :
                        ajax_get_regions().done( function(){
                            $( '[name=' + index + '] option' ).filter( function(){
                                return ( $(this).val() === result[index] );
                            }).prop('selected', true);
                            $('#provinces').prop('disabled', false);
                        } );
                        break;
                    case 'province_id' :
                        ajax_get_provinces( result.region_id ).done(function(){
                            $( '[name=' + index + '] option' ).filter( function(){
                                return ( $(this).val() === result[index] );
                            }).prop('selected', true);

                            refresh();
                        });
                        $('#cities').prop('disabled', false);
                        break;
                    case 'city_id' :
                        ajax_get_cities( result.province_id ).done( function(){
                            $( '[name=' + index + '] option' ).filter( function(){
                                return ( $(this).val() === result[index] );
                            }).prop('selected', true);

                            refresh();
                        });
                        $('#barangays').prop('disabled', false);
                        break;
                    case 'barangay_id' :
                        ajax_get_barangays( result.city_id ).done( function(){
                            $( '[name=' + index + '] option' ).filter( function(){
                                return ( $(this).val() === result[index] );
                            }).prop('selected', true);

                            refresh();
                        } );
                        break;
                    case 'type_id' :
                        ajax_get_property_types().done( function(){
                            $( '[name=' + index + '] option' ).filter( function(){
                                return ( $(this).val() === result[index] );
                            }).prop('selected', true);
                        } );
                        break;
                    default:
                        $field.val(result[index]);
                }
            } );

            get_current_page();
        }
        else{
            ajax_get_regions();
            ajax_get_property_types();

            get_current_page();
        }

        $.validator.addMethod("notEqual", function(value, element, param) {
            // return this.optional(element) || value != param;
            return this.optional(element) || value != $( param ).val();
        }, "Value should be different");

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    function add_update_property_view_recently( property_id )
    {
        $.ajax({
            url : obj.base_url + 'user/add_update_property_view_recently',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            error : function( xhr, status ){
                alert( xhr.responseText );
            }
        });
    }

    function getSinglePropertyId()
    {
        var current_url = parseURL( document.location ),
            url_parts = current_url.pathname.split( '/' ),
            property_id = url_parts[ url_parts.length - 1 ];

        return property_id;
    }

    function validate(){
        var $form = $( '#ajaxModal' ).find( 'form' ),
            name = $form.data( 'form' );

        switch( name ){
            case 'login':
                    $form.validate({
                        rules: {
                                login : {
                                    required : true
                                },
                                password : {
                                    required : true
                                }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        errorPlacement: function( erorr, element ){
                            return false;
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_signin( $form );
                        }
                    });

                    break;
            case 'register':
                    $form.validate({
                        rules: {
                                firstname : {
                                    required : true
                                },
                                lastname : {
                                    required : true
                                },
                                email : {
                                    required : true,
                                    email : true,
                                    remote : {
                                        url : obj.base_url+"user/is_email_available",
                                        type : "post"
                                    }
                                },
                                confirmemail : {
                                    required : true,
                                    email : true,
                                    equalTo : '#email'
                                },
                                password : {
                                    minlength : 6,
                                    required : true
                                },
                                confirmpassword : {
                                    required : true,
                                    equalTo : '#password'
                                }
                        },
                        messages : {
                            firstname : {
                                required : "First name is required"
                            },
                            lastname : {
                                required : "Last name is required"
                            },
                            email : {
                                required : "Email is required",
                                email : "Enter a valid email",
                                remote : "Email already exists"
                            },
                            confirmemail : {
                                required : "Confirm Email is required",
                                equalTo : "Email does not match"
                            },
                            password : {
                                minLength : "Too short",
                                required : "Password is required"
                            },
                            confirmpassword : {
                                required : "Password is required",
                                equalTo : "Password does not match"
                            }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_signup( $form );
                        }
                    });
                    break;
            case 'recover':
                    $form.validate({
                        rules: {
                                email : {
                                    required : true,
                                    email : true,
                                }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        errorPlacement: function( erorr, element ){
                            return false;
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_reset_password( $form );
                        }
                    });
                    break;
        }
    }

    function parseURL(url) {
        var parser = document.createElement('a'),
            searchObject = {},
            queries, split, i;
        // Let the browser do the work
        parser.href = url;
        // Convert query string to object
        queries = parser.search.replace(/^\?/, '').split('&');
        for( i = 0; i < queries.length; i++ ) {
            split = queries[i].split('=');
            searchObject[split[0]] = split[1];
        }
        return {
            protocol: parser.protocol,
            host: parser.host,
            hostname: parser.hostname,
            port: parser.port,
            pathname: parser.pathname,
            search: parser.search,
            searchObject: searchObject,
            hash: parser.hash
        };
    }
} );