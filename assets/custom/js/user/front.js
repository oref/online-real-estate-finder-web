$( function( $ ){
    var obj = {};

    init();
    ajax_verify();
    ajax_get_regions();
    ajax_get_property_types();
    refresh();

    $( 'abbr.timeago' ).timeago();
    $( '.selectpicker' ).selectpicker();

    if( $.cookie( 'error' ) )
    {
        toastr.error( $.cookie( 'error' ), "OREF" );
        $.removeCookie( 'error', { path: '/' });
    }

    $( document ).on( 'change', '#regions', function(){
        var value = $(this).val(),
            $provinces = $('#provinces');
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $provinces.prop('disabled', true).empty();
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $provinces.prop('disabled', false);
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            ajax_get_provinces(value);
        }
    });

    $( document ).on( 'change', '#provinces', function(){
        var value = $(this).val(),
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $cities.prop('disabled', false);
            $barangays.prop('disabled', true).empty();
            ajax_get_cities(value);
        }
    });

    $( document ).on( 'change', '#cities', function(){
        var value = $(this).val(),
            $barangays = $('#barangays');

        if(value === ''){
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $barangays.prop('disabled', false);
            ajax_get_barangays(value);
        }
    });

    $( '.btn-search' ).click( function( e ){
        e.preventDefault();

        var data = {};

        $( '.form-search' ).find( '[name]' ).each(function( index, elem ){
            var key = $( elem ).attr('name');

            data[key] = $( elem ).val();
        });

        $.cookie( 'search', JSON.stringify(data), { path: '/' } );
        document.location = obj.base_url + 'property/listing';
    } );

    $('.adv-link').click(function() {

        $('.btn-search').toggleClass("switch");

        var $lefty = $('.advanced-search');
        $lefty.animate({
            left: parseInt($lefty.css('left'),10) == 0 ? - $lefty.outerWidth() : 0
        });

        return false;
    });

    $( document ).on( 'click', '[data-bind="resend"]', function( e ){
        e.preventDefault();

        obj.ajax_resend_confirmation_link( email );
    } );

    $( document ).on( 'click', '[data-bind="login"], [data-bind="register"], [data-bind="recover"]',  function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    $( document ).on( 'click', '[data-bind="message"]', function( e ){
        e.preventDefault();

        var url = $( this ).data( 'bind' );

        load_modal( url );
    } );

    $( document ).on( 'click', '.modal-close', function( e ){
        e.preventDefault();

        $( this ).parents( '.modal' ).modal( 'hide' );
    } );

    $( document ).on( 'click', '.save-favorite', function( e ){
        e.preventDefault();

        var $this = $( this ),
            property_id = $( this ).data( 'property-id' );

        if( obj.is_user_logged_in )
        {
            if( $this.hasClass( 'active' ) )
            {
                obj.ajax_delete_favorite_property( $this, property_id );
            }
            else
            {
                obj.ajax_add_favorite_property( $this, property_id );
            }
        }
        else{
            toastr.error( 'You are not logged in.', "OREF" );

            load_modal( 'login' );
        }
    } );

    $('#msg-form').validate({
        ignore: ":hidden:not(#to_id)",
        rules: {
            msg : {
                required : true
            }
        },
        messages: {
            msg : {
                required : "Message is required"
            }
        },
        highlight: function( element, errorClass, validClass ){
            $(element).addClass( errorClass ).removeClass( validClass );
        },
        unhighlight: function( element, errorClass, validClass ){
            $(element).removeClass( errorClass ).addClass( validClass );
        },
        errorPlacement: function( erorr, element ){
            erorr.insertAfter(element);
        },
        submitHandler: function( form ){
            var $form = $( form );

            if( obj.is_user_logged_in )
                {
                    obj.ajax_add_message( $form );
                }
                else{
                    toastr.error( 'You are not logged in.', "OREF" );

                    load_modal( 'login' );
                }
        }
    });

    obj.ajax_add_message = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        return $.ajax( {
            url : obj.base_url + 'user/add_message',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status )
                {
                    toastr.success( "Message sent", "OREF" );
                    window.location.reload();
                }
                else{
                    toastr.error( "Unable to send your message", "OREF" );
                }

                $submit.prop( 'disabled', false );
            },
            error : function( xhr, status ){

                $submit.prop( 'disabled', false );
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_add_favorite_property = function( $elem, property_id ){
        $.ajax({
            url : obj.base_url + 'user/add_favorite_property',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    $elem.addClass('active');
                    toastr.success( 'Saved!' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_delete_favorite_property = function( $elem, property_id ){
        $.ajax({
            url : obj.base_url + 'user/delete_favorite_property',
            type : 'post',
            dataType : 'json',
            data : { property_id : property_id },
            success : function( result ){
                if( result.status )
                {
                    $elem.removeClass('active');
                    toastr.success( 'Removed!' );
                }
                else
                {
                    toastr.error( 'Unable to process your request', "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_property_types(){
        var $types = $('#types');

        return $.ajax({
            url : obj.base_url + 'user/get_property_types_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $types.empty();
                    $types.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].id ).text( result[index].type );
                        $types.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_regions(){
        var $regions = $('#regions');

        return $.ajax({
            url : obj.base_url + 'user/get_region_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $regions.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $regions.empty();
                    $regions.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].region_id ).text( result[index].short_name );
                        $regions.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_provinces( region_id ){
        var $provinces = $('#provinces');

        return $.ajax({
            url : obj.base_url + 'user/get_province_details',
            type : 'get',
            dataType : 'json',
            data : { region_id : region_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $provinces.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $provinces.empty();
                    $provinces.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].province_id ).text( result[index].long_name );
                        $provinces.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_cities( province_id ){
        var $cities = $('#cities');

        return $.ajax({
            url : obj.base_url + 'user/get_city_details',
            type : 'get',
            dataType : 'json',
            data : { province_id : province_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $cities.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $cities.empty();
                    $cities.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].city_id ).text( result[index].long_name );
                        $cities.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_barangays( city_id ){
        var $barangays = $('#barangays');

        return $.ajax({
            url : obj.base_url + 'user/get_barangay_details',
            type : 'get',
            dataType : 'json',
            data : { city_id : city_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $barangays.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $barangays.empty();
                    $barangays.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].barangay_id ).text( result[index].long_name );
                        $barangays.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function refresh(){
        $('.selectpicker').selectpicker('refresh');
    }

    function load_modal( url )
    {
        var $modal = $( '#ajaxModal' );

        $modal.modal( {
            show : true,
            backdrop: 'static',
        } );

        obj.ajax_get_modal_content( obj.base_url + 'user/modal/' + url, $modal ).done(function(){
            validate();
        });
    }

    function validate(){
        var $form = $( '#ajaxModal' ).find( 'form' ),
            name = $form.data( 'form' );

        switch( name ){
            case 'login':
                    $form.validate({
                        rules: {
                                login : {
                                    required : true
                                },
                                password : {
                                    required : true
                                }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        errorPlacement: function( erorr, element ){
                            return false;
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_signin( $form );
                        }
                    });

                    break;
            case 'register':
                    $form.validate({
                        rules: {
                                firstname : {
                                    required : true
                                },
                                lastname : {
                                    required : true
                                },
                                email : {
                                    required : true,
                                    email : true,
                                    remote : {
                                        url : obj.base_url+"user/is_email_available",
                                        type : "post"
                                    }
                                },
                                confirmemail : {
                                    required : true,
                                    email : true,
                                    equalTo : '#email'
                                },
                                password : {
                                    minlength : 6,
                                    required : true
                                },
                                confirmpassword : {
                                    required : true,
                                    equalTo : '#password'
                                }
                        },
                        messages : {
                            firstname : {
                                required : "First name is required"
                            },
                            lastname : {
                                required : "Last name is required"
                            },
                            email : {
                                required : "Email is required",
                                email : "Enter a valid email",
                                remote : "Email already exists"
                            },
                            confirmemail : {
                                required : "Confirm Email is required",
                                equalTo : "Email does not match"
                            },
                            password : {
                                minLength : "Too short",
                                required : "Password is required"
                            },
                            confirmpassword : {
                                required : "Password is required",
                                equalTo : "Password does not match"
                            }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_signup( $form );
                        }
                    });
                    break;
            case 'recover':
                    $form.validate({
                        rules: {
                                email : {
                                    required : true,
                                    email : true,
                                }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        errorPlacement: function( erorr, element ){
                            return false;
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_reset_password( $form );
                        }
                    });
                    break;
        }
    }

    obj.ajax_get_modal_content = function( url, $modal ){
        return $.ajax({
            url : url,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $modal.find( '.modal-content' ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.modal-content' ).html();

                $modal.find( '.modal-content' ).html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    obj.ajax_signin = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'user/signin',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status )
                {
                    location.reload();
                }
                else{
                    toastr.error( "Incorrect username/password", "OREF" );
                }

                $submit.text( 'Log In' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_signup = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );
        $.ajax( {
            url : obj.base_url + 'user/signup',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Please wait...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if( result.status ){
                    $.cookie('email', $( '#email' ).val(), { path: '/' });

                    toastr.options.timeOut = 0;
                    toastr.info( 'Please check the confirmation link we sent to your email or click <a href="#" data-bind="resend">here</a> to resend the confirmation link.' );

                    $form.parents( '.modal' ).modal( 'hide' );
                }
                else{
                    toastr.error( "Unable to process your request. Please Check your internet connection.", "OREF" );
                }

                $submit.text( 'Log In' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                toastr.error("Unable to process your request", "OREF");

                $submit.text( 'Log In' ).prop( 'disabled', false );
            }
        } );
    }

    obj.ajax_resend_confirmation_link = function(){
        $.ajax( {
            url : obj.base_url + 'user/resend',
            type : 'post',
            dataType : 'json',
            data : { email : $.cookie('email') },
            beforeSend : function(){
                toastr.info( "Re-sending the verification link. Please wait.", "OREF" );
            },
            success : function( result ) {
                if(result.status){
                    toastr.remove();
                    toastr.options.timeOut = 0;
                    toastr.info( "Please check your email now. We already sent a new verification link." );
                }
                else{
                    toastr.error( "Unable to process your request.", "OREF" );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_reset_password = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );
        $.ajax( {
            url : obj.base_url + 'user/reset',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Please wait...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    toastr.info( "Please check your email for the new password.", "OREF" );

                    $form.parents( '.modal' ).modal( 'hide' );
                }
                else{
                    toastr.error( "Unable to process your request...", "OREF" );
                }

                $submit.text( 'Log In' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function ajax_verify()
    {
        var confirmation_code = getParameterByName( 'c' );

        if( confirmation_code )
        {
            $.ajax( {
                url : obj.base_url + 'user/user_verify',
                type : 'post',
                dataType : 'json',
                data : 'confirmation_code=' + confirmation_code,
                beforeSend : function(){
                    toastr.info('Please wait...', "OREF");
                },
                success : function( result ) {
                    toastr.remove();
                    if(result.status){
                        toastr.options.timeOut = 0;
                        toastr.success( 'Your account has been verified. Click <a href="#" data-bind="login">here<a/> to login', "OREF" );
                    }
                    else{
                        toastr.options.timeOut = 0;
                        toastr.warning( 'Unable to process your request. Click <a href="' + window.location.href.split('?')[0]  + '">here</a> to refresh', "OREF" );
                    }
                },
                error : function( xhr, status ){
                    console.log( xhr.responseText );
                }
            } );
        }
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';
        obj.is_user_logged_in = $.cookie( 'is_user_logged_in' ) ? $.cookie( 'is_user_logged_in' ) : false;

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            validClass: 'form-valid',
            focusInvalid: true
        });

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    function getParameterByName( name ) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

} );