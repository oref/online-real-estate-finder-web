$(function(){
    var obj = {};

    init();

    $('[data-toggle=offcanvas-sm]').click(function () {
        $('.row-offcanvas-sm').toggleClass('active');
        $('[data-toggle=offcanvas-sm]').toggleClass('active');
        $('.sidebar-offcanvas-sm').toggleClass('active');
    });

    $('[data-toggle=offcanvas]').click(function () {
        $('.site-row.row-offcanvas').toggleClass('active');
        $('[data-toggle=offcanvas]').toggleClass('active');
        $('.sidebar-offcanvas').toggleClass('active');
    });

    $( '.property-type' ).click( function( e ){
        e.preventDefault();
        var data = {};

        $( '.form-search' ).find( '[name]' ).each(function( index, elem ){
            var key = $( elem ).attr('name');

            data[key] = $( elem ).val();
        });

        data.type_id = $( this ).data( 'type-id' ).toString();

        $.cookie( 'search', JSON.stringify(data), { path: '/' } );
        document.location = obj.base_url + 'property/listing';
    } );

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
    }
});