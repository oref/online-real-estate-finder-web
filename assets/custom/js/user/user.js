$( function( $ ){
    var obj = {};

    init();
    ajax_verify();

    $("abbr.timeago").timeago();

    $( '#profile-tabs' ).find( 'a' ).click( function(e){
        e.preventDefault();

        var target = $( this ).data( 'target' ).substr( 1 );

        ajax_get_tab_content( target ).done(function(){
            switch( target ){
                case 'edit_profile':
                    ajax_get_regions().done(function(){
                        ajax_get_user_details( target );
                        tab_change( target );
                    });
                break;
                case 'email_preferences':
                    tab_change(target);
                break;
                case 'change_credentials':
                    ajax_get_user_details( target );
                    tab_change( target );
                break;
            }
        });

        $( this ).tab( 'show' );
    });

    $( '#property-tabs' ).find( 'a' ).click( function(e){
        e.preventDefault();
        {
            var target = $( this ).data( 'target' ).substr( 1 );

            ajax_get_tab_content( target ).done( function(){
                tab_change( target );
                if( target === 'approved_properties' ){
                    approved_properties();
                }
            } );

            $( this ).tab( 'show' );
        }
    });

    $( document ).on( 'change', '#regions', function(){
        var value = $(this).val(),
            $provinces = $('#provinces');
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $provinces.prop('disabled', true).empty();
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $provinces.prop('disabled', false);
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            ajax_get_provinces(value);
        }
    });

    $( document ).on( 'change', '#provinces', function(){
        var value = $(this).val(),
            $cities = $('#cities');
            $barangays = $('#barangays');

        if(value === ''){
            $cities.prop('disabled', true).empty();
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $cities.prop('disabled', false);
            $barangays.prop('disabled', true).empty();
            ajax_get_cities(value);
        }
    });

    $( document ).on( 'change', '#cities', function(){
        var value = $(this).val(),
            $barangays = $('#barangays');

        if(value === ''){
            $barangays.prop('disabled', true).empty();
            refresh();
        }
        else{
            $barangays.prop('disabled', false);
            ajax_get_barangays(value);
        }
    });

    $(document).ready(function() {
        $("#price").on("input", function() {
        // allow numbers, a comma or a dot
            var v= $(this).val(), vc = v.replace(/[^0-9,\.]/, '');
            if (v !== vc)
            $(this).val(vc);
        });
    });

    function ajax_get_user_details( tab ){
        var $form = $( '#' + tab ).find( 'form' );

        return $.ajax({
            url : obj.base_url + 'user/get_user_details',
            type : 'get',
            dataType : 'json',
            success: function( result ){
                if( result instanceof Object ) {
                    $.each( result, function(index, value){
                        var $field = $form.find( '[name=' + index + ']' );

                        if( tab === 'edit_profile' ){
                            switch( index ){
                                case 'region_id' :
                                    $( '[name=' + index + '] option' ).filter( function(){
                                        return ( $(this).val() === result[index] );
                                    }).prop('selected', true);
                                    $('#provinces').prop('disabled', false);
                                    break;
                                case 'province_id' :
                                    ajax_get_provinces( result.region_id ).done(function(){
                                        $( '[name=' + index + '] option' ).filter( function(){
                                            return ( $(this).val() === result[index] );
                                        }).prop('selected', true);

                                        refresh();
                                    });
                                    $('#cities').prop('disabled', false);
                                    break;
                                case 'city_id' :
                                    ajax_get_cities( result.province_id ).done( function(){
                                        $( '[name=' + index + '] option' ).filter( function(){
                                            return ( $(this).val() === result[index] );
                                        }).prop('selected', true);

                                        refresh();
                                    });
                                    $('#barangays').prop('disabled', false);
                                    break;
                                case 'barangay_id' :
                                    ajax_get_barangays( result.city_id ).done( function(){
                                        $( '[name=' + index + '] option' ).filter( function(){
                                            return ( $(this).val() === result[index] );
                                        }).prop('selected', true);

                                        refresh();
                                    } );
                                    break;
                                case 'type_id' :
                                    $( '[name=' + index + '] option' ).filter( function(){
                                        return ( $(this).val() === result[index] );
                                    }).prop('selected', true);
                                    break;
                                case 'tags':
                                    $field.val( result[index] );
                                    $field.tagsinput( 'refresh' );
                                    break;
                                case 'avatar':
                                    break;
                                default:
                                    $field.val(result[index]);
                            }
                        }
                        else if( tab === 'change_credentials' ){
                            switch( index ){
                                case 'email':
                                    $('#old_email').val( result[index] );
                                    break;
                            }
                        }
                    } );
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function ajax_get_property_types(){
        var $types = $('#types');

        return $.ajax({
            url : obj.base_url + 'user/get_property_types_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $types.empty();
                    $types.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].id ).text( result[index].type );
                        $types.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function ajax_get_regions(){
        var $regions = $('#regions');

        return $.ajax({
            url : obj.base_url + 'user/get_region_details',
            type : 'get',
            dataType : 'json',
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $regions.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $regions.empty();
                    $regions.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].region_id ).text( result[index].short_name );
                        $regions.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    };

    function ajax_get_provinces( region_id ){
        var $provinces = $('#provinces');

        return $.ajax({
            url : obj.base_url + 'user/get_province_details',
            type : 'get',
            dataType : 'json',
            data : { region_id : region_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $provinces.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $provinces.empty();
                    $provinces.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].province_id ).text( result[index].long_name );
                        $provinces.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_cities( province_id ){
        var $cities = $('#cities');

        return $.ajax({
            url : obj.base_url + 'user/get_city_details',
            type : 'get',
            dataType : 'json',
            data : { province_id : province_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $cities.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $cities.empty();
                    $cities.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].city_id ).text( result[index].long_name );
                        $cities.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    function ajax_get_barangays( city_id ){
        var $barangays = $('#barangays');

        return $.ajax({
            url : obj.base_url + 'user/get_barangay_details',
            type : 'get',
            dataType : 'json',
            data : { city_id : city_id },
            beforeSend: function(){
                var $loader = $('<option />');

                $loader.attr('value', '').text('Fetching...').prop('selected', true);

                $barangays.append($loader);
                refresh();
            },
            success: function( result ){
                if( result instanceof Array ) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('-Select-');

                    $barangays.empty();
                    $barangays.append($select);

                    $.each(result, function(index, value){
                        var $option = $( '<option />' );

                        $option.attr( 'value', result[ index ].barangay_id ).text( result[index].long_name );
                        $barangays.append( $option );
                    });

                    refresh();
                }
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        });
    }

    obj.ajax_update_user = function ( $form ){
        var $submit = $form.find( '[type="submit"]' ),
            formData = new FormData($form[0]);

        $.ajax( {
            url : obj.base_url + 'user/update_user',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    var tab = obj.tabs.defaults.profile;

                    $( '.aside-profile' ).load( obj.base_url + '/user .panel-profile' );

                    ajax_get_tab_content( tab ).done( function(){
                        ajax_get_regions().done( function(){
                            ajax_get_user_details( obj.tabs.defaults.profile );
                        } );

                        tab_change( tab );

                        toastr.success( "Profile updated successfully", "OREF" );
                    } );
                }
                else{
                    toastr.error( "Failed to update", "OREF" );
                }

                $submit.text( 'Update' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_update_user_email = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'user/change_user_email',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );

                toastr.options.timeOut = 0;
                toastr.info('Please wait...', 'OREF');
            },
            success : function( result ) {
                if(result.status){
                    var tab = obj.tabs.defaults.change_credentials;

                    ajax_get_tab_content( tab ).done( function(){
                        tab_change( tab );
                        toastr.remove();
                        toastr.info( "Please check your email. We sent you a verification link for your change email request.", "OREF" );
                    } );
                }
                else{
                    toastr.error( "Failed to update", "OREF" );
                }

                $submit.text( 'Update' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_update_user_password = function ( $form ){
        var $submit = $form.find( '[type="submit"]' );

        $.ajax( {
            url : obj.base_url + 'user/update_user_password',
            type : 'post',
            dataType : 'json',
            data : $form.serialize(),
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    var tab = obj.tabs.defaults.change_credentials;
                    ajax_get_tab_content( tab ).done( function(){
                        tab_change( tab );

                        toastr.success( "Profile updated successfully", "OREF" );
                    } );
                }
                else{
                    toastr.error( "Failed to update", "OREF" );
                }

                $submit.text( 'Update' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function ajax_nearby_properties( $form ){
        return $.ajax( {
            url : obj.base_url + 'user/get_all_property_details_by_status/approved',
            type : 'get',
            dataType : 'json',
            success : function ( result ){
                $.each( result, function(index, value){
                    var region = result[index].region,
                        province = result[index].province,
                        city = result[index].city,
                        barangay = result[index].barangay,
                        province_index = province.indexOf( ' (' ),
                        city_index = city.indexOf( ' (' ),
                        barangay_index = barangay.indexOf( ' (' );

                        if( province_index > 0 ){
                            province = province.substr( 0, province_index ) ;
                        }
                        if( city_index > 0 ){
                            city = city.substr( 0, city_index ) ;
                        }
                        if( barangay_index > 0 ){
                            barangay = barangay.substr( 0, barangay_index ) ;
                        }

                    geocode( region + ' ' + province + ' ' + city + ' ' + barangay, result[index].name );
                });
            },
            error : function ( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    obj.ajax_sell_property = function ( $form ){
        var $submit = $form.find( '[type="submit"]' ),
            formData = new FormData($form[0]);

        $.ajax( {
            url : obj.base_url + 'user/sell_property',
            type : 'post',
            dataType : 'json',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend : function(){
                $submit.text( 'Processing...' ).prop( 'disabled', true );
            },
            success : function( result ) {
                if(result.status){
                    var tab = obj.tabs.defaults.property;

                    ajax_get_tab_content( tab ).done( function(){
                        tab_change( tab );

                        toastr.success( "Property request done. Wait for verification by the broker.", "OREF" );
                    } );
                }
                else{
                    toastr.error( "Failed to request", "OREF" );
                }

                $submit.text( 'Request' ).prop( 'disabled', false );
            },
            error : function( xhr, status ){
                console.log( xhr.responseText );
            }
        } );
    }

    function refresh(){
        $('.selectpicker').selectpicker('refresh');
    }

    function ajax_get_tab_content( target ){
        return $.ajax({
            url : obj.base_url + 'user/tab/'  + target,
            type : 'get',
            dataType : 'html',
            beforeSend: function(){
                $( '#' + target ).html( obj.loader );
            },
            success: function( response ){
                var html = $( $.parseHTML( response ) ),
                    content = html.filter( '.tab-content' ).html();

                $( '#' + target ).empty().html( content );
            },
            error: function( xhr, status ){
                console.log( xhr.responseText )
            }
        });
    }

    function nearby_properties(){
        obj.map = new GMaps({
                    div: '#gmap-menu',
                    lat: 0,
                    lng: 0
                });
        GMaps.geolocate({
          success: function( position ) {
            obj.map.setCenter(position.coords.latitude, position.coords.longitude);
            obj.map.addMarker({
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                infoWindow: {
                  content: '<h4>Your Here</h4>'
                }
            });
          },
          error: function(error) {
            toastr.error('Geolocation failed: '+error.message, "OREF");
          },
          not_supported: function() {
            toastr.info("Your browser does not support geolocation", "OREF");
          },
          always: function() {
            // toastr.success("Location Found", "OREF");
          }
        });
    }

    function init(){
        obj.base_url = $( 'body' ).data( 'url' );
        obj.method = $( 'body' ).data( 'method' );
        obj.loader = '<img src="'+ obj.base_url + 'assets/custom/img/loader/loader.gif' +'" class="loader" />';
        obj.tabs = {};

        obj.tabs.defaults = {
            profile : 'edit_profile',
            property : 'sell_property',
            change_credentials : 'change_credentials',
            nearby_properties : 'nearby_properties'
        }

        $.validator.setDefaults({
            debug : true,
            errorClass: 'form-error',
            success : "valid",
            validClass: 'form-valid',
            focusInvalid: true
        });

        $.validator.addMethod("notEqual", function(value, element, param) {
            // return this.optional(element) || value != param;
            return this.optional(element) || value != $( param ).val();
        }, "Value should be different");

        toastr.options = {
            "toastClass": 'alert',
            "iconClasses": {
                error: 'alert-error',
                info: 'alert-info',
                success: 'alert-success',
                warning: 'alert-warning'
            },
            "closeButton": true,
            "closeHtml": '<button type="button" class="close">&times;</button>',
            "debug": true,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 5000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        switch( obj.method ){
            case 'account':
                ajax_get_tab_content( obj.tabs.defaults.profile ).done(function(){
                    ajax_get_regions().done(function(){
                        ajax_get_user_details( obj.tabs.defaults.profile );
                        tab_change( obj.tabs.defaults.profile );
                    });
                });
            break;
            case 'property':
                ajax_get_tab_content( obj.tabs.defaults.property ).done(function(){
                    ajax_get_regions();
                    ajax_get_property_types();
                    tab_change( obj.tabs.defaults.property );
                });
            break;
            case 'nearby_properties':
                ajax_nearby_properties().done( function(){
                    nearby_properties();
                } );
            break;
        }
    }

    function ajax_verify()
    {
        var confirmation_code = getParameterByName( 'verify' );

        if( confirmation_code && obj.method === 'account' )
        {
            $( '[data-target="#change_credentials"]' ).tab( 'show' );

            $.ajax( {
                url : obj.base_url + 'user/email_verify',
                type : 'post',
                dataType : 'json',
                data : 'confirmation_code=' + confirmation_code,
                success : function( result ) {
                    if(result.status){
                        toastr.options.timeOut = 0;
                        toastr.success( 'Your email has been verified.', "OREF" );

                    }
                    else{
                        toastr.options.timeOut = 0;
                        toastr.warning( 'Unable to process your request. Click <a href="' + window.location.href.split('?')[0]  + '">here</a> to refresh', "OREF" );
                    }

                    ajax_get_tab_content( obj.tabs.defaults.change_credentials ).done(function(){
                        tab_change( obj.tabs.defaults.change_credentials );
                    });
                },
                error : function( xhr, status ){
                    console.log( xhr.responseText );
                }
            } );
        }
    }

    function tab_change( tab ){
        var $form = $( '#' + tab ).find( 'form' );

        switch( tab )
        {
            case 'edit_profile':
                    if( $.fn.selectpicker ) $('.selectpicker').selectpicker();
                    if( $.fn.fileinput() )
                    {
                        $("#avatar").fileinput({
                            overwriteInitial: true,
                            showClose: false,
                            showCaption: false,
                            browseLabel: '',
                            removeLabel: '',
                            browseIcon: '<i class="fa fa-image"></i>',
                            removeIcon: '<i class="fa fa-times"></i>',
                            removeTitle: 'Cancel or reset changes',
                            elErrorContainer: '.avatar-errors',
                            msgErrorClass: 'alert alert-block alert-danger',
                            showUpload: false,
                            initialPreview: [
                               '<img src="' + $('.media-avatar > img').attr('src') + '" class="file-preview-avatar" alt="Your Avatar">'
                            ],
                            defaultPreviewContent: '<img src="' + $('.media-avatar > img').attr('src') + '" class="file-preview-avatar" alt="Your Avatar">',
                            // layoutTemplates: {main2: '{preview} {remove} {browse}'},
                            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"]
                        });
                    }

                    $form.validate({
                        ignore : [],
                        rules: {
                                username : {
                                    required : true,
                                    remote : {
                                        url : obj.base_url+"user/is_username_available",
                                        type : "post"
                                    }
                                },
                                firstname : {
                                    required : true
                                },
                                lastname : {
                                    required : true
                                },
                                contactno : {
                                    required : true
                                },
                                region_id : {
                                    required : true
                                },
                                province_id : {
                                    required : true
                                },
                                city_id : {
                                    required : true
                                },
                                barangay_id : {
                                    required : true
                                }
                            },
                        messages : {
                            username : {
                                required : "Username is required",
                                remote : "Username is already exists"
                            },
                            firstname : {
                                required : "Firstname is required"
                            },
                            lastname : {
                                required : "Lastname is required"
                            },
                            contactno : {
                                required : "Contact Number is required"
                            },
                            region_id : {
                                required : "Region is required"
                            },
                            province_id : {
                                required : "Province is required"
                            },
                            city_id : {
                                required : "City is required"
                            },
                            barangay_id : {
                                required : "Barangay is required"
                            }
                        },
                        highlight: function( element, errorClass, validClass ){
                            switch($(element).attr('name'))
                            {
                                case 'type':
                                case 'region_id':
                                case 'province_id':
                                case 'city_id':
                                case 'barangay_id':
                                        $(element).siblings(".bootstrap-select").addClass( errorClass ).removeClass( validClass );
                                        break;
                                case 'features':
                                        $(element).siblings(".bootstrap-tagsinput").addClass( errorClass ).removeClass( validClass );
                                        break;
                                default:
                                        $(element).addClass( errorClass ).removeClass( validClass );

                            }
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            switch($(element).attr('name'))
                            {
                                case 'type':
                                case 'region_id':
                                case 'province_id':
                                case 'city_id':
                                case 'barangay_id':
                                        $(element).siblings(".bootstrap-select").removeClass( errorClass ).addClass( validClass );
                                        break;
                                case 'features':
                                        $(element).siblings(".bootstrap-tagsinput").removeClass( errorClass ).addClass( validClass );
                                        break;
                                default:
                                        $(element).removeClass( errorClass ).addClass( validClass );
                            }
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_update_user( $form );
                        }
                    });
                    break;
            case 'email_preferences':
                    $('input[type="checkbox"]').checkbox({
                        checkedClass: 'fa fa-check-square',
                        uncheckedClass: 'fa fa-square-o'
                    });

                    $form.validate({
                        rules: {
                                email : {
                                    required : true
                                },
                                password : {
                                    required : true
                                }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        errorPlacement: function( erorr, element ){
                            return false;
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_signin( $form );
                        }
                    });
                    break;
            case 'change_credentials':
                    $( '[data-form="email"]' ).validate({
                        rules: {
                                old_email : {
                                    required : true,
                                    remote : {
                                        url : obj.base_url+"user/is_email_match",
                                        type : "post"
                                    }
                                },
                                new_email : {
                                    required : true,
                                    notEqual : '#old_email',
                                    remote : {
                                        url : obj.base_url+"user/is_email_available",
                                        type : "post"
                                    }
                                },
                                confirm_email : {
                                    required : true,
                                    equalTo : '#new_email'
                                }

                        },
                        messages : {
                            old_email : {
                                required : "Current Email is required",
                                remote : "Please enter your current email"
                            },
                            new_email : {
                                notEqual : "Email should not be the same as current",
                                required : "New Email is required",
                                remote : "Email is already exists"
                            },
                            confirm_email : {
                                required : "Confirm Email is required",
                                equalTo : 'Email does not match'
                            }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_update_user_email( $form );
                        }
                    });

                    $( '[data-form="password"]' ).validate({
                        rules: {
                                old_password : {
                                    required : true,
                                    remote : {
                                        url : obj.base_url+"user/is_password_match",
                                        type : "post"
                                    }
                                },
                                new_password : {
                                    required : true
                                },
                                confirm_password: {
                                    required : true,
                                    equalTo : '#new_password'
                                }

                        },
                        messages : {
                            old_password : {
                                required : "Current Password is required",
                                remote : "Incorrect Password"
                            },
                            new_password : {
                                required : "New Password is required"
                            },
                            confirm_password : {
                                required : "Confirm Password is required",
                                equalTo : 'Password does not match'
                            }
                        },
                        highlight: function( element, errorClass, validClass ){
                            $(element).addClass( errorClass ).removeClass( validClass );
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            $(element).removeClass( errorClass ).addClass( validClass );
                        },
                        submitHandler: function( form ){
                            var $form = $( form );

                            obj.ajax_update_user_password( $form );
                        }
                    });
                    break;
                case 'sell_property':

                    if( $.fn.selectpicker ) $('.selectpicker').selectpicker();
                    if( $.fn.tagsinput ) $( '[data-bind="tags"]' ).tagsinput( 'refresh' );

                    $form.validate({
                        ignore : [],
                        rules: {
                            'property_images[]' : {
                                required : true
                            },
                            name : {
                                required : true
                            },
                            price: {
                                required : true,
                                number : true
                            },
                            contactno : {
                                required : true
                            },
                            region_id : {
                                required : true
                            },
                            province_id : {
                                required : true
                            },
                            city_id : {
                                required : true
                            },
                            barangay_id : {
                                required : true
                            },
                            bathroom : {
                                required : true,
                                number : true
                            },
                            bedroom : {
                                required : true,
                                number : true
                            },
                            type_id : {
                                required : true
                            },
                            features : {
                                required : true
                            },
                            info : {
                                required : true
                            }

                        },
                        messages : {
                            'property_images[]' : {
                                required : "Property images is required"
                            },
                            name : {
                                required : "Name is required"
                            },
                            price : {
                                required : "Price is required",
                                number : "Input only numbers"
                            },
                            contactno : {
                                required : "Contact Number is required"
                            },
                            bathroom : {
                                required : "Number of bathroom is required",
                                number : "Input only numbers"
                            },
                            bedroom : {
                                required : "Number of bedroom is required",
                                number : "Input only numbers"
                            },
                            type_id : {
                                required : "Type is required"
                            },
                            features : {
                                required : "Features is required"
                            },
                            info : {
                                required : "Information is required"
                            },
                            region_id : {
                                required : "Region is required"
                            },
                            province_id : {
                                required : "Province is required"
                            },
                            city_id : {
                                required : "City is required"
                            },
                            barangay_id : {
                                required : "Barangay is required"
                            }
                        },
                        highlight: function( element, errorClass, validClass ){
                            switch($(element).attr('name'))
                            {
                                case 'type_id':
                                case 'region_id':
                                case 'province_id':
                                case 'city_id':
                                case 'barangay_id':
                                        $(element).siblings(".bootstrap-select").addClass( errorClass ).removeClass( validClass );
                                        break;
                                case 'features':
                                        $(element).siblings(".bootstrap-tagsinput").addClass( errorClass ).removeClass( validClass );
                                        break;
                                default:
                                        $(element).addClass( errorClass ).removeClass( validClass );

                            }
                        },
                        unhighlight: function( element, errorClass, validClass ){
                            switch($(element).attr('name'))
                            {
                                case 'type_id':
                                case 'region_id':
                                case 'province_id':
                                case 'city_id':
                                case 'barangay_id':
                                        $(element).siblings(".bootstrap-select").removeClass( errorClass ).addClass( validClass );
                                        break;
                                case 'features':
                                        $(element).siblings(".bootstrap-tagsinput").removeClass( errorClass ).addClass( validClass );
                                        break;
                                default:
                                        $(element).removeClass( errorClass ).addClass( validClass );
                            }
                        },
                        submitHandler : function(){
                            obj.ajax_sell_property($form);
                        }
                     });
                    break;
        }
    }

    function approved_properties_format( id, name, address, bedrooms, bathrooms, price, images, info ) {
        var html = '<div class="prop-item col-sm-6 col-md-4 property-content"> \
                        <div class="thumbnail"> \
                            <div class="thumbnail-img thumbscrubber"> \
                                <span class="ts-inner">';
                                    $.each(images, function(index, val) {
                                        html += '<img class="ts-currslide" src="' + obj.base_url + images[index].path + '" />';
                                    });
            html +=             '</span> \
                            </div> \
                                <div class="thumbnail-body"> \
                                    <div class="caption text-nowrap"> \
                                        <a href="' + obj.base_url + 'property/single/' + id + '" data-id="' + id + '"> \
                                            <h3>' + name + '</h3> \
                                            <span title="' + address + '"><i class="fa fa-map-marker"></i> ' + address + '</span> \
                                            <div class="prop-price">' + price + '</div> \
                                            <p>' + info + '</p> \
                                        </a> \
                                    </div> \
                                    <div class="content clearfix"> \
                                        <ul class="list-unstyled feature-list"> \
                                            <li>' + bedrooms + ' Bedrooms</li> \
                                            <li>' + bathrooms + ' Baths</li> \
                                        </ul> \
                                    </div> \
                                </div> \
                            </div> \
                        </div> \
                    </div>';

        return html;
    }

    function approved_properties()
    {
        var $approved_properties = $( '.approved_properties' );

        $.ajax({
            url : obj.base_url + 'user/get_all_property_assigned_by_seller',
            type : 'get',
            dataType : 'json',
            success : function ( result ){
                $approved_properties.empty();


                if( result instanceof Array)
                {
                    $.each( result, function(key, value){
                        $approved_properties.append(approved_properties_format(result[key].id, result[key].name, result[key].address, result[key].bedrooms, result[key].bathrooms, result[key].price, result[key].images, result[key].info));
                    } );
                }
            },
            error : function( xhr, status )
            {
                console.log( xhr.responseText );
            }
        });
    }

    function getParameterByName( name ) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function geocode( address, content ) {
        GMaps.geocode({
            address: address,
            callback: function(results, status){
                if(status=='OK'){
                    var latlng = results[0].geometry.location;
                    obj.map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng(),
                        infoWindow: {
                          content: '<h3>' + content + '</h3>'
                        }
                    });
                }
            }
        });
    }

} );