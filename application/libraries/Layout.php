<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{
    private $CI;

    public function __construct()
    {
        $this->CI = & get_instance();

        $this->CI->load->model( 'user_model', 'user' );
        $this->CI->load->model( 'admin_model', 'admin' );
        $this->CI->load->model( 'property_model', 'property' );
    }

    public function view($page_path, $page_contents = "", $layout, $flag = FALSE)
    {
        $user_id = $this->CI->session->userdata( 'user_id' );
        $admin_id = $this->CI->session->userdata( 'admin_id' );

        $this->data[ 'content' ] = $this->CI->load->view($page_path, $page_contents, TRUE);
        $this->data[ 'is_user_logged_in' ] = $this->CI->session->userdata( 'is_user_logged_in' );
        $this->data[ 'is_admin_logged_in' ] = $this->CI->session->userdata( 'is_admin_logged_in' );
        $this->data[ 'user' ] = $this->CI->user->get_user_details_by_id($user_id);
        $this->data[ 'admin' ] = $this->CI->admin->get_admin_details_by_id($admin_id);
        $this->data[ 'property_types' ] = $this->CI->property->get_property_types_details();
        // Add assets
        $this->CI
                 //Styles
                 ->add_global_styles('assets/plugins/bootstrap/css/bootstrap', TRUE, TRUE)
                 ->add_global_styles('assets/plugins/font-awesome/css/font-awesome', TRUE, TRUE)
                 ->add_global_styles('assets/plugins/toastr/css/toastr')
                 //Scripts
                 ->add_global_scripts('assets/plugins/jquery/js/jquery', TRUE, TRUE)
                 ->add_global_scripts('assets/plugins/bootstrap/js/bootstrap', TRUE, TRUE)
                 ->add_global_scripts('assets/plugins/moment/js/moment', TRUE, TRUE)
                 ->add_global_scripts('assets/plugins/jquery-form-validation/js/jquery.validate', TRUE, TRUE)
                 ->add_global_scripts('assets/plugins/jquery-form-validation/js/additional-methods')
                 ->add_global_scripts('assets/plugins/bootstrap-select/js/bootstrap-select', TRUE, TRUE)
                 ->add_global_scripts('assets/plugins/jquery-nicescroll/js/jquery.nicescroll',TRUE,TRUE)
                 ->add_global_scripts('assets/plugins/jquery-cookie/js/jquery-cookie')
                 ->add_global_scripts('assets/plugins/bootstrap-checkbox/js/bootstrap-checkbox')
                 ->add_global_scripts('assets/plugins/toastr/js/toastr', TRUE, TRUE)
                 ->add_global_scripts('assets/plugins/timeago/js/jquery.timeago');


        // Load assets
        $this->data['scripts']['head'] = $this->CI->load_scripts(TRUE);
        $this->data['scripts']['body'] = $this->CI->load_scripts();
        $this->data['styles'] = $this->CI->load_styles();


        $this->data[ 'favorites' ] = $this->CI->property->get_total_favorite_properties_by_user_id( $user_id );
        return $this->CI->load->view($layout, $this->data, $flag);
    }

}