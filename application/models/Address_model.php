<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Address_model extends My_model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_region_details()
    {
        $query = $this->db->select( 'r.region_id, r.short_name, r.long_name' )
                          ->from( $this->_table_prefix . 'regions r' )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_province_details( $data )
    {
        extract( $data );

        $query = $this->db->select( 'p.province_id, p.region_id, p.long_name' )
                          ->from( $this->_table_prefix . 'provinces p' )
                          ->join( $this->_table_prefix . 'regions r', 'p.region_id = r.region_id', 'inner' )
                          ->where( 'p.region_id', $region_id )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_city_details( $data )
    {
        extract( $data );

        $query = $this->db->select( 'c.city_id, c.province_id, c.long_name' )
                          ->from( $this->_table_prefix . 'cities c' )
                          ->join( $this->_table_prefix . 'provinces p', 'c.province_id = p.province_id', 'inner' )
                          ->where( 'c.province_id', $province_id )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_barangay_details( $data )
    {
        extract( $data );

        $query = $this->db->select( 'b.barangay_id, b.city_id, b.long_name' )
                          ->from( $this->_table_prefix . 'barangays b' )
                          ->join( $this->_table_prefix . 'cities c', 'b.city_id = c.city_id', 'inner' )
                          ->where( 'b.city_id', $city_id )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }
}