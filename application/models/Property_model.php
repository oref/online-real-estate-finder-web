<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property_model extends MY_Model
{
    public function __construct()
    {
        $this->_models = array( 'user' );

        parent::__construct();
    }

    public function add_property( $data )
    {
        extract($data);

        if( $address_id = $this->user->add_address( $data ) )
        {
            if( !$address_id ) return FALSE;

            $data[ 'address_id' ] = $address_id;

            $property_id = $this->add_property_details( $data );
            if( !$property_id ) return FALSE;

            return $property_id;
        }
    }

    public function add_property_images( $data )
    {
        extract($data);

        $data = array(
                    'property_id'       =>      $id,
                    'path'              =>      $path,
                    'type'              =>      $file_type,
                    'size'              =>      $file_size,
                    'width'             =>      $image_width,
                    'height'            =>      $image_height,
                );

        $query = $this->db->insert( $this->_table_prefix . 'property_images', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function add_property_details( $data )
    {
        extract($data);

        $data = array(
                    'name'              =>      $name,
                    'price'             =>      $price,
                    'address_id'        =>      $address_id,
                    'type_id'           =>      $type_id,
                    'user_id'           =>      $id,
                    'bathrooms'         =>      $bathroom,
                    'bedrooms'          =>      $bedroom,
                    'features'          =>      $features,
                    'info'              =>      $info
                );

        $query = $this->db->insert( $this->_table_prefix . 'properties', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function add_address( $data )
    {
        extract($data);

        $data = array(
                'region_id'     =>  $region_id,
                'province_id'   =>  $province_id,
                'city_id'       =>  $city_id,
                'barangay_id'   =>  $barangay_id
            );

        $query = $this->db->insert( $this->_table_prefix . 'addresses', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function add_update_property_view_recently( $data )
    {
        extract($data);

        $query = $this->db->where( 'property_id', $property_id )
                          ->get( $this->_table_prefix . 'recently_viewed_properties' );

        if( $query->num_rows() )
        {
            return $this->update_property_view_recently( $property_id );
        }
        else
        {
            return $this->add_property_view_recently( $property_id );
        }
    }

    public function add_property_view_recently( $id )
    {
        $data = array(
                    'property_id'   =>    $id
                );

        $query = $this->db->insert( $this->_table_prefix . 'recently_viewed_properties', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function add_property_type( $data )
    {
        extract($data);

        $data = array(
                'type'     =>  $type
            );

        $query = $this->db->insert( $this->_table_prefix . 'property_types', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function update_property_type( $data )
    {
        extract($data);

        $data = array(
                'type'          =>  $type,
                'date_modified' =>  date('Y-m-d H:i:s')
            );

        $this->db->where( 'id', $id )
                 ->update( $this->_table_prefix . 'property_types', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_property_agent_id( $data )
    {
        extract($data);

        $data = array(
                'agent_id'          =>  0
            );

        $this->db->where( 'id', $property_id )
                 ->update( $this->_table_prefix . 'properties', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_property( $data )
    {
        extract($data);

        $property = $this->get_property_details_by_id( $id );

        if( !$property->address_id ) // if address is not set or equal to 0
        {
            $address_id = $this->add_address( $data );

            if( !$address_id ) return FALSE;

            $data[ 'address_id' ] = $address_id;

            if( !$this->update_property_details( $data ) ) return FALSE;
        }
        else{
            $data[ 'address_id' ] = $property->address_id;

            if( !$this->update_address( $data ) && !$this->update_property_details( $data ) ) return FALSE;
        }

        return TRUE;
    }

    public function update_address( $data )
    {
        extract($data);

        $data = array(
                'region_id'     =>  $region_id,
                'province_id'   =>  $province_id,
                'city_id'       =>  $city_id,
                'barangay_id'   =>  $barangay_id
            );

        $this->db->where( 'id', $address_id )
                 ->update( $this->_table_prefix . 'addresses', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_property_details( $data )
    {
        extract( $data );

        $data = array(
                    'name'              =>      $name,
                    'price'             =>      $price,
                    'address_id'        =>      $address_id,
                    'type_id'           =>      $type_id,
                    'user_id'           =>      $id,
                    'bathrooms'         =>      $bathrooms,
                    'bedrooms'          =>      $bedrooms,
                    'features'          =>      $features,
                    'info'              =>      $info
                );

        $this->db->where( 'id', $id )
                 ->update( $this->_table_prefix . 'properties', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_property_view_recently( $id )
    {
        $data = array(
                    'date_viewed' => date('Y-m-d H:i:s')
                );

        $this->db->where( 'property_id', $id )
                 ->update( $this->_table_prefix . 'recently_viewed_properties', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function get_all_property_type_details_by_id( $type_id )
    {
        $query = $this->db->select( 'id, type' )
                          ->from( $this->_table_prefix . 'property_types' )
                          ->where( 'id', $type_id )
                          ->get();

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_property_types_details()
    {
        $query = $this->db->select( 'pt.id, pt.type' )
                          ->from( $this->_table_prefix . 'property_types pt' )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_property_details_by_id( $id )
    {
        $query = $this->db->select( 'p.address_id, p.id, p.user_id, p.agent_id, p.type_id, p.name, p.price, p.bathrooms, p.bedrooms, p.info, p.features, p.status, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', u.firstname, u.lastname) user_full_name, u.email user_email, u.contactno user_contactno, u.avatar, CONCAT_WS(\' \', au.firstname, au.lastname) agent_full_name, au.email agent_email, au.contactno agent_contactno, r.short_name region, pr.long_name province, b.long_name barangay, ct.long_name city, pt.type type' )
                          ->from( $this->_table_prefix . 'properties p' )
                          ->join( $this->_table_prefix . 'addresses a', 'p.address_id = a.id', 'inner' )
                          ->join( $this->_table_prefix . 'users u', 'p.user_id = u.id', 'inner' )
                          ->join( $this->_table_prefix . 'users au', 'p.agent_id = au.id', 'left' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'inner' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'inner' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'inner' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'inner' )
                          ->join( $this->_table_prefix . 'property_types pt', 'p.type_id = pt.id', 'inner' )
                          ->where( 'p.id', $id )
                          ->get();

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_all_property_details_by_status( $status )
    {
        $query = $this->db->select( 'p.address_id, p.id, p.type_id, p.name, p.price, p.bathrooms, p.bedrooms, p.info, p.features, p.status, p.date_sold, CONCAT_WS(\' \', au.firstname, au.lastname) agent_full_name, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, ct.long_name, b.long_name) address, r.short_name region, pr.long_name province, ct.long_name city, b.long_name barangay, pt.type' )
                          ->from( $this->_table_prefix . 'properties p' )
                          ->join( $this->_table_prefix . 'users au', 'p.agent_id = au.id', 'inner' )
                          ->join( $this->_table_prefix . 'addresses a', 'p.address_id = a.id', 'inner' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'inner' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'inner' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'inner' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'inner' )
                          ->join( $this->_table_prefix . 'property_types pt', 'p.type_id = pt.id', 'inner' )
                          ->where( 'p.status', $status )
                          ->get();

        return ( $query ) ? $query->result() : FALSE;
    }

    public function get_all_property_assigned( $id )
    {
        $array = array( 'agent_id' => $id, 'status' => 'approved' );

        $query = $this->db->select( 'p.id, p.name, p.price, p.bathrooms, p.bedrooms, p.info, p.date_added, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, ct.long_name, b.long_name) address' )
                          ->from( $this->_table_prefix . 'properties p' )
                          ->join( $this->_table_prefix . 'addresses a', 'p.address_id = a.id', 'inner' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'inner' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'inner' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'inner' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'inner' )
                          ->where( $array )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_sold_property_assigned( $id )
    {
        $array = array( 'agent_id' => $id, 'status' => 'sold', 'is_claimed' => 0 );

        $query = $this->db->select( 'p.id, p.name, p.price, p.bathrooms, p.bedrooms, p.info, p.date_added, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, ct.long_name, b.long_name) address' )
                          ->from( $this->_table_prefix . 'properties p' )
                          ->join( $this->_table_prefix . 'addresses a', 'p.address_id = a.id', 'inner' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'inner' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'inner' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'inner' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'inner' )
                          ->where( $array )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_property_assigned_by_seller( $id )
    {
        $query = $this->db->select( 'p.address_id, p.id, p.name, p.price, p.bathrooms, p.bedrooms, p.info, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, ct.long_name, b.long_name) address' )
                          ->from( $this->_table_prefix . 'properties p' )
                          ->join( $this->_table_prefix . 'addresses a', 'p.address_id = a.id', 'inner' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'inner' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'inner' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'inner' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'inner' )
                          ->where( 'p.user_id', $id )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_total_properties_by_status( $status )
    {
        $query = $this->db->where( 'status', $status )
                          ->get( $this->_table_prefix . 'properties' );

        return $query->num_rows();
    }

    public function get_total_favorite_properties_by_user_id( $user_id )
    {
        $query = $this->db->where( 'user_id', $user_id )
                          ->get( $this->_table_prefix . 'favorite_properties' );

        return $query->num_rows();
    }

    public function get_total_properties()
    {
        $query = $this->db->get( $this->_table_prefix . 'properties' );

        return $query->num_rows();
    }

    public function get_total_property_types()
    {
        $query = $this->db->get( $this->_table_prefix . 'property_types' );

        return $query->num_rows();
    }

    public function get_total_declined_properties()
    {
        $query = $this->db->where( 'status', 'declined' )
                          ->get( $this->_table_prefix . 'properties' );

        return $query->num_rows();
    }

    public function get_total_sold_properties()
    {
        $query = $this->db->where( 'status', 'sold' )
                          ->get( $this->_table_prefix . 'properties' );

        return $query->num_rows();
    }

    public function get_total_approved_properties()
    {
        $query = $this->db->where( 'is_approved', $this->_approved )
                          ->get( $this->_table_prefix . 'properties' );

        return $query->num_rows();
    }

    public function get_total_not_assigned_properties()
    {
        $query = $this->db->where( 'agent_id', $this->_null )
                          ->get( $this->_table_prefix . 'properties' );

        return $query->num_rows();
    }

    public function get_total_assigned_properties( $id )
    {
        $query = $this->db->where( 'agent_id', $id )
                          ->get( $this->_table_prefix . 'properties' );

        return $query->num_rows();
    }

    public function get_total_favorite_properties_by_status( $id, $status )
    {
        $query = $this->db->from( $this->_table_prefix . 'favorite_properties fp')
                          ->join( $this->_table_prefix . 'properties p', 'p.id = fp.property_id', 'inner')
                          ->where( 'p.status', $status )
                          ->where( 'fp.user_id', $id )
                          ->get();

        return $query->num_rows();
    }

    public function get_all_property_images_by_id( $id )
    {
        $query = $this->db->where( 'property_id', $id )
                          ->get( $this->_table_prefix . 'property_images' );

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_property_details_by_search( $data )
    {
        extract($data);

        $sql = 'SELECT p.address_id, p.id, p.name, p.type_id, p.price, p.bathrooms, p.bedrooms, a.region_id, a.province_id, a.city_id, a.barangay_id, p.agent_id, CONCAT_WS(\' \', r.short_name, pr.long_name, ct.long_name, b.long_name) address, pt.type ';
        $sql .= 'FROM ' . $this->_table_prefix . 'properties p ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'addresses a ON p.address_id = a.id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'regions r ON a.region_id = r.region_id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'provinces pr ON a.province_id = pr.province_id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'cities ct ON a.city_id = ct.city_id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'barangays b ON a.barangay_id = b.barangay_id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'property_types pt ON p.type_id = pt.id ';
        $sql .= 'WHERE p.status = ?';

        if(!empty($name))
        {
            $sql .= ' AND p.name LIKE ?';
        }
        if(!empty($region_id))
        {
            $sql .= ' AND r.region_id = ?';
        }
        if(!empty($province_id))
        {
            $sql .= ' AND pr.province_id = ?';
        }
        if(!empty($city_id))
        {
            $sql .= ' AND ct.city_id = ?';
        }
        if(!empty($barangay_id))
        {
            $sql .= ' AND b.barangay_id = ?';
        }
        if(!empty($type_id))
        {
            $sql .= ' AND pt.id = ?';
        }
        if(!empty($bedrooms))
        {
            $sql .= ' AND p.bedrooms = ?';
        }
        if(!empty($bathrooms))
        {
            $sql .= ' AND p.bathrooms = ?';
        }
        if(!empty($price_from))
        {
            $sql .= ' AND p.price >= ?';
        }
        if(!empty($price_to))
        {
            $sql .= ' AND p.price <= ?';
        }

        $sort = explode('|', $sort);
        unset($data['sort']);
        $sql .= ' ORDER BY p.' . $sort[0] . ' ' . strtoupper($sort[1]);
        $sql .= ' LIMIT ?, ?';

        $params = array( 'approved' );

        foreach($data as $key => $value)
        {
            if(!empty($value))
            {
                switch($key)
                {
                    case 'region_id':
                    case 'province_id':
                    case 'city_id':
                    case 'barangay_id':
                    case 'type_id':
                    case 'bedrooms':
                    case 'bathrooms':
                    case 'price_from':
                    case 'price_to':
                        $params[] = intval($value);
                        break;
                    case 'page':
                        $value = ($page - 1) * $offset;
                        $params[] = intval($value);
                        break;
                    case 'offset':
                        $value = ($value === 'all') ? $this->get_total_properties_by_status( 'approved' ) : $value;
                        $params[] = intval($value);
                        break;
                    default:
                        $params[] = '%' . $value . '%';
                }
            }
        }

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_favorite_properties( $data )
    {
        extract($data);

        $sql = 'SELECT fp.property_id, fp.user_id, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, ct.long_name, b.long_name) address, p.id, p.name, p.price, p.bathrooms, p.bedrooms, p.agent_id ';
        $sql .= 'FROM ' . $this->_table_prefix . 'favorite_properties fp ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'properties p ON fp.property_id = p.id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'addresses a ON p.address_id = a.id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'regions r ON a.region_id = r.region_id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'provinces pr ON a.province_id = pr.province_id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'cities ct ON a.city_id = ct.city_id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'barangays b ON a.barangay_id = b.barangay_id ';
        $sql .= 'WHERE p.status = ? AND fp.user_id = ? ';

        $sort = explode('|', $sort);
        unset($data['sort']);
        $sql .= ' ORDER BY ' . $sort[0] . ' ' . strtoupper($sort[1]);
        $sql .= ' LIMIT ?, ?';

        $params = array( 'approved', intval($id) );
        unset($data['id']);

        foreach($data as $key => $value)
        {
            if(!empty($value))
            {
                switch($key)
                {
                    case 'page':
                        $value = ($page - 1) * $offset;
                        $params[] = intval($value);
                        break;
                    case 'offset':
                        $value = ($value === 'all') ? $this->get_total_properties_by_status( 'approved' ) : $value;
                        $params[] = intval($value);
                        break;
                    default:
                        $params[] = $value;
                }
            }
        }

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_favorite_property_ids( $id )
    {
        $query = $this->db->where( 'user_id', $id )
                          ->get( $this->_table_prefix . 'favorite_properties' );

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_approved_properties( $data, $status )
    {
        extract($data);

        $order_by = $sort;

        if( is_array($sort) )
        {
            $order_by = array();

            foreach( $sort as $key => $value )
            {
                $order_by[] = $key . ' ' . $value;
            }

            $order_by = implode(', ', $order_by);
        }

        $sql = 'SELECT p.id, p.name, CONCAT_WS(\' \', u.firstname, u.lastname) seller_name, CONCAT_WS(\' \', ua.firstname, ua.lastname) agent_name ';
        $sql .= 'FROM ' . $this->_table_prefix . 'properties p ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'users u ON p.user_id = u.id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'users ua ON p.agent_id = ua.id ';
        $sql .= 'WHERE p.is_approved = ? AND ';
        $sql .= 'p.status = ? AND ';
        $sql .= '(p.id LIKE ? OR ';
        $sql .= 'p.name LIKE ? OR ';
        $sql .= 'u.firstname LIKE ? OR ';
        $sql .= 'u.lastname LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', u.firstname, u.lastname) LIKE ? OR ';
        $sql .= 'ua.firstname LIKE ? OR ';
        $sql .= 'ua.lastname LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', ua.firstname, ua.lastname) LIKE ?) ';
        $sql .= 'ORDER BY ' . $order_by . ' ';
        $sql .= 'LIMIT ?, ?';

        $params = array(
                        $this->_approved,
                        $status,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        ($current - 1) * $rowCount,
                        intval($rowCount)
                    );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    public function get_all_declined_properties( $data )
    {
        extract($data);

        $order_by = $sort;

        if( is_array($sort) )
        {
            $order_by = array();

            foreach( $sort as $key => $value )
            {
                $order_by[] = $key . ' ' . $value;
            }

            $order_by = implode(', ', $order_by);
        }

        $sql = 'SELECT p.id, p.name, CONCAT_WS(\' \', u.firstname, u.lastname) seller_name, CONCAT_WS(\' \', ua.firstname, ua.lastname) agent_name ';
        $sql .= 'FROM ' . $this->_table_prefix . 'properties p ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'users u ON p.user_id = u.id ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'users ua ON p.agent_id = ua.id ';
        $sql .= 'WHERE p.status = ? AND ';
        $sql .= '(p.id LIKE ? OR ';
        $sql .= 'p.name LIKE ? OR ';
        $sql .= 'u.firstname LIKE ? OR ';
        $sql .= 'u.lastname LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', u.firstname, u.lastname) LIKE ? OR ';
        $sql .= 'ua.firstname LIKE ? OR ';
        $sql .= 'ua.lastname LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', ua.firstname, ua.lastname) LIKE ?) ';
        $sql .= 'ORDER BY ' . $order_by . ' ';
        $sql .= 'LIMIT ?, ?';

        $params = array(
                        'declined',
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        ($current - 1) * $rowCount,
                        intval($rowCount)
                    );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    public function get_all_sold_properties( $data )
    {
        extract($data);

        $order_by = $sort;

        if( is_array($sort) )
        {
            $order_by = array();

            foreach( $sort as $key => $value )
            {
                $order_by[] = $key . ' ' . $value;
            }

            $order_by = implode(', ', $order_by);
        }

        $sql = 'SELECT p.id, p.name, CONCAT_WS(\'\', p.price * 0.05) price, CONCAT_WS(\' \', u.firstname, u.lastname) agent_name ';
        $sql .= 'FROM ' . $this->_table_prefix . 'properties p ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'users u ON p.agent_id = u.id ';
        $sql .= 'WHERE p.status = ? AND ';
        $sql .= 'p.is_claimed = ? AND ';
        $sql .= '(p.id LIKE ? OR ';
        $sql .= 'p.name LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', p.price * 0.05) LIKE ? OR ';
        $sql .= 'u.firstname LIKE ? OR ';
        $sql .= 'u.lastname LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', u.firstname, u.lastname) LIKE ?) ';
        $sql .= 'ORDER BY ' . $order_by . ' ';
        $sql .= 'LIMIT ?, ?';

        $params = array(
                        'sold',
                        0,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        ($current - 1) * $rowCount,
                        intval($rowCount)
                    );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    public function get_all_not_assigned_properties( $data )
    {
        extract($data);

        $order_by = $sort;

        if( is_array($sort) )
        {
            $order_by = array();

            foreach( $sort as $key => $value )
            {
                $order_by[] = $key . ' ' . $value;
            }

            $order_by = implode(', ', $order_by);
        }

        $sql = 'SELECT p.id, p.name, CONCAT_WS(\' \', u.firstname, u.lastname) seller_name ';
        $sql .= 'FROM ' . $this->_table_prefix . 'properties p ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'users u ON p.user_id = u.id ';
        $sql .= 'WHERE p.agent_id = ? AND ';
        $sql .= '(p.id LIKE ? OR ';
        $sql .= 'p.name LIKE ? OR ';
        $sql .= 'u.firstname LIKE ? OR ';
        $sql .= 'u.lastname LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', u.firstname, u.lastname) LIKE ?) ';
        $sql .= 'ORDER BY ' . $order_by . ' ';
        $sql .= 'LIMIT ?, ?';

        $params = array(
                        $this->_null,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        ($current - 1) * $rowCount,
                        intval($rowCount)
                    );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    public function get_all_assigned_properties( $data )
    {
        extract($data);

        $order_by = $sort;

        if( is_array($sort) )
        {
            $order_by = array();

            foreach( $sort as $key => $value )
            {
                $order_by[] = $key . ' ' . $value;
            }

            $order_by = implode(', ', $order_by);
        }

        $sql = 'SELECT p.id, p.name, CONCAT_WS(\' \', u.firstname, u.lastname) seller_name ';
        $sql .= 'FROM ' . $this->_table_prefix . 'properties p ';
        $sql .= 'INNER JOIN ' . $this->_table_prefix . 'users u ON p.user_id = u.id ';
        $sql .= 'WHERE p.agent_id = ? AND ';
        $sql .= 'p.is_approved = ? AND ';
        $sql .= '(p.id LIKE ? OR ';
        $sql .= 'p.name LIKE ? OR ';
        $sql .= 'u.firstname LIKE ? OR ';
        $sql .= 'u.lastname LIKE ? OR ';
        $sql .= 'CONCAT_WS(\' \', u.firstname, u.lastname) LIKE ?) ';
        $sql .= 'ORDER BY ' . $order_by . ' ';
        $sql .= 'LIMIT ?, ?';

        $params = array(
                        $id,
                        $this->_pending,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        ($current - 1) * $rowCount,
                        intval($rowCount)
                    );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    public function get_all_property_types( $data )
    {
        extract($data);

        $order_by = $sort;

        if( is_array($sort) )
        {
            $order_by = array();

            foreach( $sort as $key => $value )
            {
                $order_by[] = $key . ' ' . $value;
            }

            $order_by = implode(', ', $order_by);
        }

        $sql = 'SELECT id, type, date_modified, date_added ';
        $sql .= 'FROM ' . $this->_table_prefix . 'property_types ';
        $sql .= 'WHERE type = ? OR ';
        $sql .= 'date_modified = ? OR ';
        $sql .= 'date_added = ? OR ';
        $sql .= '1 = 1 ';
        $sql .= 'ORDER BY ' . $order_by . ' ';
        $sql .= 'LIMIT ?, ?';

        $params = array(
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        '%' . $searchPhrase,
                        ($current - 1) * $rowCount,
                        intval($rowCount)
                    );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    public function get_all_property_details_opened_recently()
    {
        $query = $this->db->select( 'p.id, p.name, p.price, p.bathrooms, p.bedrooms, p.info, p.features, p.status, rp.date_viewed' )
                          ->from( $this->_table_prefix . 'recently_viewed_properties rp' )
                          ->join( $this->_table_prefix . 'properties p', 'rp.property_id = p.id', 'inner' )
                          ->order_by( 'rp.date_viewed', 'desc' )
                          ->limit(4)
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;

    }

    public function add_favorite_property( $data )
    {
        extract( $data );

        $data = array(
                    'user_id'       =>  $id,
                    'property_id'   =>  $property_id
                );

        $query = $this->db->insert( $this->_table_prefix . 'favorite_properties', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function delete_property_type( $data )
    {
        extract( $data );

        $query = $this->db->where( 'id', $type_id )
                          ->delete( $this->_table_prefix . 'property_types' );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function delete_favorite_property( $data )
    {
        extract( $data );

        $query = $this->db->where( 'property_id', $property_id )
                          ->delete( $this->_table_prefix . 'favorite_properties' );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_property_status( $data, $status )
    {
        extract( $data );

        $data = array(
                    'status'    =>  $status
                );

        $this->db->where( 'id', $property_id )
                 ->update( $this->_table_prefix . 'properties', $data);

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_property_approve( $data )
    {
        extract( $data );

        $data = array(
                    'is_approved'    =>  1
                );

        $this->db->where( 'id', $property_id )
                 ->update( $this->_table_prefix . 'properties', $data);


        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_property_agent( $data )
    {
        extract( $data );

        $data = array(
                    'agent_id'  =>  $agent_id
                );

        $this->db->where( 'id', $property_id )
                 ->update( $this->_table_prefix . 'properties', $data);


        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_sold_status( $data )
    {
        extract( $data );

        $data = array(
                    'status'        =>  'sold',
                    'is_sold'       =>  1,
                    'date_sold'     =>  date('Y-m-d H:i:s')
                );

        $this->db->where( 'id', $property_id )
                 ->update( $this->_table_prefix . 'properties', $data);

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_re_assigned_property( $data )
    {
        extract( $data );

        $data = array(
                    'status'        =>  'pending',
                    'is_approved'   =>  0,
                    'agent_id'      =>  0
                );

        $this->db->where( 'id', $property_id )
                 ->update( $this->_table_prefix . 'properties', $data);

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_claimed_commission( $data )
    {
        extract( $data );

        $data = array(
                    'is_claimed'       =>  1
                );
        $this->db->where( 'id', $property_id )
                 ->update( $this->_table_prefix . 'properties', $data);

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }
}