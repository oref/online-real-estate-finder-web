<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function new_message( $data )
    {
        extract($data);

        $this->db->trans_start();

        if($conversation = $this->get_conversation_id( $from_id, $to_id ))
        {
            $conversation_id = $this->get_conversation_id( $from_id, $to_id )->id;
        }
        else
        {
            $conversation_id = $this->add_new_conversation( $from_id, $to_id );
        }

        // Adding message
        $last_msg_id = $this->add_message( $conversation_id, $from_id, $msg );

        // Update last message on conversation
        $this->update_last_message_id( $conversation_id, $last_msg_id );

        $this->db->trans_complete();

        return ($this->db->trans_status()) ? TRUE : FALSE;
    }

    public function add_new_conversation( $from_id, $to_id )
    {
        $data = array(
                    'from_id'   =>  $from_id,
                    'to_id'     =>  $to_id
                );

        $query = $this->db->insert( $this->_table_prefix . 'conversations', $data );

        return ($query) ? $this->db->insert_id() : FALSE;
    }

    public function get_conversation_id( $from_id, $to_id )
    {
        $sql = 'SELECT id FROM ';
        $sql .= $this->_table_prefix . 'conversations ';
        $sql .=  'WHERE (from_id = ? AND to_id = ?) OR ';
        $sql .= '(from_id = ? AND to_id = ?)';

        $query = $this->db->query( $sql, array( $from_id, $to_id, $to_id, $from_id ) );

        return ($query->num_rows()) ? $query->row() : FALSE;
    }

    public function get_all_conversations_by_id( $id, $limit = null )
    {
        if( !is_null($limit) ){
            $this->db->limit( $limit );
        }
        $query = $this->db->select( 'c.id, ut.id to_id, CONCAT_WS(\' \', ut.firstname, ut.lastname) to_full_name, ut.avatar to_avatar, ut.avatar_bg_color to_avatar_bg_color, uf.id from_id, CONCAT_WS(\' \', uf.firstname, uf.lastname) from_full_name, uf.avatar from_avatar, uf.avatar_bg_color from_avatar_bg_color, m.id msg_id, m.msg' )
                          ->from( $this->_table_prefix . 'conversations c' )
                          ->join( $this->_table_prefix . 'users uf', 'c.from_id = uf.id', 'inner' )
                          ->join( $this->_table_prefix . 'users ut', 'c.to_id = ut.id', 'inner' )
                          ->join( $this->_table_prefix . 'messages m', 'c.last_msg_id = m.id', 'inner' )
                          ->where( 'c.from_id', $id )
                          ->or_where( 'c.to_id', $id )
                          ->order_by( 'c.date_added desc' )
                          ->get();

        return ($query->num_rows()) ? $query->result() : FALSE;
    }

    public function get_all_messages_by_id( $data )
    {
        extract($data);

        $total = $this->get_total_messages_by_id( $id );
        $start = $total - $start;

        $query = $this->db->select( 'u.id sender_id, c.to_id, c.from_id, CONCAT_WS(\' \', u.firstname, u.lastname) sender_name, u.avatar sender_avatar, u.avatar sender_avatar_bg_color, m.id, m.msg, m.date_added' )
                          ->from( $this->_table_prefix . 'conversations c' )
                          ->join( $this->_table_prefix . 'messages m', 'm.conversation_id = c.id', 'inner' )
                          ->join( $this->_table_prefix . 'users u', 'm.sender_id = u.id', 'inner' )
                          ->where( 'c.id', $id )
                          ->order_by( 'm.date_added asc' )
                          ->limit( $total, ($start > 0) ? $start : 0 )
                          ->get();

        return ($query->num_rows()) ? $query->result() : FALSE;
    }

    public function get_total_messages_by_id( $id )
    {
        $query = $this->db->where( 'conversation_id', $id )
                          ->get( $this->_table_prefix . 'messages' );

        return $query->num_rows();
    }

    public function get_recent_message( $id )
    {
        $query = $this->db->select( 'c.id, ut.id to_id, CONCAT_WS(\' \', ut.firstname, ut.lastname) to_full_name, ut.avatar to_avatar, ut.avatar_bg_color to_avatar_bg_color, uf.id from_id, CONCAT_WS(\' \', uf.firstname, uf.lastname) from_full_name, uf.avatar from_avatar, uf.avatar_bg_color from_avatar_bg_color, m.id msg_id, m.msg, m.sender_id' )
                          ->from( $this->_table_prefix . 'conversations c' )
                          ->join( $this->_table_prefix . 'users uf', 'c.from_id = uf.id', 'inner' )
                          ->join( $this->_table_prefix . 'users ut', 'c.to_id = ut.id', 'inner' )
                          ->join( $this->_table_prefix . 'messages m', 'c.last_msg_id = m.id', 'inner' )
                          ->where( 'c.from_id', $id )
                          ->or_where( 'c.to_id', $id )
                          ->order_by( 'c.date_added desc' )
                          ->get();

        return ($query->num_rows()) ? $query->row() : FALSE;
    }

    public function add_message( $conversation_id, $sender_id, $msg )
    {
        $data = array(
                    'conversation_id'   =>  $conversation_id,
                    'sender_id'         =>  $sender_id,
                    'msg'               =>  $msg,
                );

        $query = $this->db->insert( $this->_table_prefix . 'messages', $data );

        return ($query) ? $this->db->insert_id() : FALSE;
    }

    public function update_last_message_id( $conversation_id, $last_msg_id )
    {
        $data = array(
                    'last_msg_id'       =>  $last_msg_id
                );

        $this->db->where( 'id', $conversation_id )
                 ->update( $this->_table_prefix . 'conversations', $data );

        return ($this->db->affected_rows()) ? TRUE : FALSE;
    }

}