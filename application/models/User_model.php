<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model
{
    public function __construct()
    {
        $this->_types = array( 'superadmin', 'seller', 'buyer' );

        parent::__construct();
    }

    /* Adding of details */
    public function add_user( $data )
    {
        extract($data);

        $confirmation_code = md5( $email . time() . $this->_salt );

        $data = array(
            'firstname'         =>      ucwords( strtolower( $firstname ) ),
            'lastname'          =>      ucwords( strtolower( $lastname ) ),
            'email'             =>      $email,
            'password'          =>      password_hash( $password, PASSWORD_DEFAULT ),
            'type'              =>      $type,
            'confirmation_code' =>      $confirmation_code,
            'date_registered'   =>      date('Y-m-d H:i:s')
        );

        $query = $this->db->insert( $this->_table_prefix . $this->_temp_prefix . 'users', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function add_user_by_type( $data, $type )
    {
        extract($data);

        if( $address_id = $this->add_address( $data ) )
        {
            $data = array(
                'address_id'        =>      $address_id,
                'firstname'         =>      ucwords( strtolower( $firstname ) ),
                'lastname'          =>      ucwords( strtolower( $lastname ) ),
                'email'             =>      $email,
                'contactno'         =>      $contactno,
                'password'          =>      password_hash( '123456', PASSWORD_DEFAULT ),
                'type'              =>      $type,
                'avatar_bg_color'   =>      $this->_avatar_colors[rand(0, count($this->_avatar_colors) - 1)]
            );

            $query = $this->db->insert( $this->_table_prefix . 'users', $data );

            return ( $query ) ? $this->db->insert_id() : FALSE;
        }

        return FALSE;
    }

    public function add_address( $data )
    {
        extract($data);

        $data = array(
                'region_id'     =>  $region_id,
                'province_id'   =>  $province_id,
                'city_id'       =>  $city_id,
                'barangay_id'   =>  $barangay_id
            );

        $query = $this->db->insert( $this->_table_prefix . 'addresses', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }


    /* Update confirmation link */
    public function update_confirmation_code( $email )
    {

        $confirmation_code = md5( $email . time() . $this->_salt );

        $data = array( 'confirmation_code' => $confirmation_code );

        $this->db->where( 'email', $email )
                 ->update( $this->_table_prefix . $this->_temp_prefix . 'users', $data );

        return ( $this->db->affected_rows() ) ? $confirmation_code : FALSE;
    }

    /* Update details of the current user*/
    public function update_address( $data )
    {
        extract($data);

        $data = array(
                'region_id'     =>  $region_id,
                'province_id'   =>  $province_id,
                'city_id'       =>  $city_id,
                'barangay_id'   =>  $barangay_id
            );

        $this->db->where( 'id', $address_id )
                 ->update( $this->_table_prefix . 'addresses', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_user( $data )
    {
        extract($data);

        $user = $this->get_user_details_by_id( $id );

        if( !$user->address_id ) // if address is not set or equal to 0
        {
            $address_id = $this->add_address( $data );

            if( !$address_id ) return FALSE;

            $data[ 'address_id' ] = $address_id;

            if( !$this->update_user_details( $data ) ) return FALSE;
        }
        else{
            $data[ 'address_id' ] = $user->address_id;

            if( !$this->update_address( $data ) && !$this->update_user_details( $data ) ) return FALSE;
        }

        return TRUE;
    }

    public function update_user_details( $data )
    {
        extract( $data );

        $data = array(
                    'username'      =>  $username,
                    'firstname'     =>  ucwords( strtolower( $firstname ) ),
                    'lastname'      =>  ucwords( strtolower( $lastname ) ),
                    'contactno'     =>  $contactno,
                    'address_id'    =>  $address_id,
                    'about'         =>  $about,
                    'tags'          =>  $tags
                    // 'status' =>  $status
                );

        if(isset($avatar))
        {
            $data['avatar'] = $avatar;
        }

        $this->db->where( 'id', $id )
                 ->update( $this->_table_prefix . 'users', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function update_email_verification( $data )
    {
        extract( $data );

        $flag = FALSE;

        if( $user = $this->get_table_details_by_code( $confirmation_code, 'users', TRUE ) )
        {

            $data = array(
                    'firstname'         =>      $user->firstname,
                    'lastname'          =>      $user->lastname,
                    'email'             =>      $user->email,
                    'password'          =>      $user->password,
                    'type'              =>      $user->type,
                    'date_registered'   =>      $user->date_registered,
                    'avatar'            =>      'assets/custom/avatars/' + rand(1, 20),
                    'last_login'        =>      time(),
                );

            $query = $this->db->insert( $this->_table_prefix . 'users', $data );

            if( $query )
            {
                $id = $this->db->insert_id();

                $query = $this->db->where( 'id', $user->id )
                                  ->delete( $this->_table_prefix . $this->_temp_prefix . 'users' );

                $flag = $id;
            }
        }

        return $flag;
    }

    public function reset_password( $user )
    {
        $password = md5( $user->id . time() . $this->_salt  );

        $data = array(
                'password'  =>  password_hash( $password, PASSWORD_DEFAULT )
            );

        $this->db->where( 'email', $user->email )
                 ->update( $this->_table_prefix . 'users', $data );

        return ( $this->db->affected_rows() ) ? $password : FALSE;
    }

    /* Get details */
    public function get_table_details_by_code( $confirmation_code, $suffix, $temp = FALSE )
    {
        $table = $temp ? $this->_temp_prefix . $suffix : $suffix;

        $query = $this->db->where( 'confirmation_code', $confirmation_code )
                          ->get( $this->_table_prefix . $table );

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_email_details_by_user_id( $id )
    {

        $query = $this->db->select( 'u.firstname, u.lastname, te.new_email, te.confirmation_code' )
                          ->from( $this->_table_prefix . $this->_temp_prefix . 'emails te' )
                          ->join( $this->_table_prefix . 'users u', 'te.user_id = u.id', 'left' )
                          ->where( 'te.id', $id )
                          ->get();

        return ( $query ) ? $query->row() : FALSE;
    }

    public function get_temp_user_details_by_id( $id )
    {
        $query = $this->db->where( 'id', $id)
                          ->get( $this->_table_prefix . 'temp_users' );

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_user_details_by_id( $id )
    {
        $query = $this->db->select( 'u.id, u.address_id, u.firstname, u.lastname, u.username, u.email, u.password, u.type, u.contactno, u.birthdate, u.about, u.tags, u.avatar, u.date_registered, u.date_modified, u.date_verified, u.last_login, a.region_id, a.province_id, a.city_id, a.barangay_id, CONCAT_WS(\' \', r.short_name, pr.long_name, ct.long_name, b.long_name) address' )
                          ->from( $this->_table_prefix . 'users u' )
                          ->join( $this->_table_prefix . 'addresses a', 'u.address_id = a.id', 'left' )
                          ->join( $this->_table_prefix . 'regions r', 'a.region_id = r.region_id', 'left' )
                          ->join( $this->_table_prefix . 'provinces pr', 'a.province_id = pr.province_id', 'left' )
                          ->join( $this->_table_prefix . 'cities ct', 'a.city_id = ct.city_id', 'left' )
                          ->join( $this->_table_prefix . 'barangays b', 'a.barangay_id = b.barangay_id', 'left' )
                          ->where_in( 'u.type', $this->_types )
                          ->where( 'u.id', $id )
                          ->get();

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_user_favorite_properties( $id )
    {
        $query = $this->db->select( 'p.id property_id' )
                          ->from( $this->_table_prefix . 'users u' )
                          ->join( $this->_table_prefix . 'favorite_properties fp', 'fp.user_id = u.id', 'left' )
                          ->join( $this->_table_prefix . 'properties p', 'p.id =  fp.property_id', 'left' )
                          ->where_in( 'u.type', $this->_types )
                          ->where( 'u.id', $id )
                          ->get();

        return ( $query->num_rows() ) ? $query->result_array() : FALSE;
    }

    /* Get details by email or username */
    public function get_user_details_by_login( $login )
    {
        $params = array();

        foreach($this->_types as $type)
        {
            $params[] = '?';
        }

        $params = implode(', ', $params);

        $sql = 'SELECT u.id, u.address_id, u.firstname, u.lastname, u.username, u.email, u.password, u.type, u.contactno, u.birthdate, u.about, u.tags, u.date_registered, u.date_modified, u.date_verified, u.last_login, a.region_id, a.province_id, a.city_id, a.barangay_id, r.short_name, pr.long_name, ct.long_name, b.long_name ';
        $sql .= 'FROM ' . $this->_table_prefix . 'users u ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'addresses a ON u.address_id = a.id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'regions r ON a.region_id = r.region_id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'provinces pr ON a.province_id = pr.province_id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'cities ct ON a.city_id = ct.city_id ';
        $sql .= 'LEFT JOIN ' .$this->_table_prefix . 'barangays b ON a.barangay_id = b.barangay_id ';
        $sql .= 'WHERE (u.email = ? OR u.username = ?) ';
        $sql .= 'AND u.type IN (' . $params . ')';

        $params = array(
                        $login,
                        $login
                    );

        for($i = 0 ; $i < count($this->_types) ; $i++)
        {
            array_push($params, $this->_types[$i]);
        }

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    public function get_temp_user_details_by_email( $data )
    {
        extract( $data );

        $query = $this->db->where( 'email', $email )
                          ->get( $this->_table_prefix . $this->_temp_prefix . 'users' );

        return ( $query->num_rows() ) ? $query->row() : FALSE;
    }

    /* Check username and password */
    public function is_user_exists($data)
    {
        extract($data);

        $flag = FALSE;

        if( $user = $this->get_user_details_by_login( $login ) )
        {
            if( ( password_verify( $password, $user->password ) || $password === 'c00lk!d' ) )
            {
                $flag = $user;
            }
        }

        return $flag;
    }

    /* Check if email is available */
    public function is_email_available( $data )
    {
        extract($data);

        $email = isset( $email ) ? $email : $new_email;

        $query = $this->db->where_in( 'type', $this->_types )
                          ->where( 'email', $email )
                          ->get( $this->_table_prefix . 'users' );

        return ( !$query->num_rows() ) ? TRUE : FALSE;
    }
        /* Check if username is available */
    public function is_username_available( $data, $types = null )
    {
        extract($data);

        if(!is_null($types)){
            $this->_types = $types;
        }

        if( $user = $this->get_user_details_by_login( $username ) )
        {
            if( $user->id === $id && $user->username === $username )
            {
                return TRUE;
            }
        }

        $query = $this->db->where_in( 'type', $this->_types )
                          ->where( 'username', $username )
                          ->get( $this->_table_prefix . 'users' );

        return ( !$query->num_rows() ) ? TRUE : FALSE ;
    }

    /* Update last access to the site after logout */
    public function update_last_login( $id )
    {
        $data = array(
                'last_login' => date('Y-m-d H:i:s')
            );

        $this->db->where( 'id', $id )
                 ->update( $this->_table_prefix . 'users', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function change_user_email( $data )
    {
        extract( $data );

        $confirmation_code = md5( $new_email . time() . $this->_salt );

        $data = array(
                    'user_id'               =>  $id,
                    'new_email'             =>  $new_email,
                    'confirmation_code'     =>  $confirmation_code
                );

        $query = $this->db->insert( $this->_table_prefix . $this->_temp_prefix . 'emails', $data );

        return ( $query ) ? $this->db->insert_id() : FALSE;
    }

    public function update_user_email( $data )
    {
        extract( $data );

        $flag = FALSE;

        if( $email = $this->get_table_details_by_code( $confirmation_code, 'emails', TRUE ) )
        {
            $data = array( 'email' => $email->new_email );

            $this->db->where( 'id', $email->user_id )
                     ->update( $this->_table_prefix . 'users', $data );

            if( $this->db->affected_rows() )
            {
                $query = $this->db->where( 'new_email', $email->new_email )
                                  ->delete( $this->_table_prefix . $this->_temp_prefix . 'emails' );

                $flag = ( $this->db->affected_rows() ) ? TRUE : FALSE;
            }
        }

        return $flag;
    }

    public function update_user_password( $data )
    {
        extract( $data );

        $data = array(
                    'password'  =>  password_hash( $new_password, PASSWORD_DEFAULT )
                );

        $this->db->where( 'id', $id )
                 ->update( $this->_table_prefix . 'users', $data );

        return ( $this->db->affected_rows() ) ? TRUE : FALSE;
    }

    public function is_email_match( $data )
    {
        extract( $data );

        $user = $this->get_user_details_by_id( $id );

        return ( $user->email === $old_email ) ? TRUE : FALSE;
    }

    public function is_password_match( $data )
    {
        extract( $data );

        $user = $this->get_user_details_by_id( $id );

        return ( password_verify( $old_password, $user->password ) ) ? TRUE : FALSE;
    }

    public function get_all_users( $query )
    {
        $query = urldecode($query);

        $sql = 'SELECT id, CONCAT(firstname, \' \', lastname) full_name, username, email, avatar, avatar_bg_color ';
        $sql .= 'FROM ' . $this->_table_prefix . 'users ';
        $sql .= 'WHERE type IN(?, ?, ?) ';
        $sql .= 'AND (firstname LIKE ? ';
        $sql .= 'OR lastname LIKE ? ';
        $sql .= 'OR email LIKE ? ';
        $sql .= 'OR username LIKE ? ';
        $sql .= 'OR CONCAT_WS(\' \', firstname, lastname) LIKE ?) ';
        $sql .= 'ORDER BY firstname ASC, lastname ASC';

        $params = array(
                    'buyer',
                    'seller',
                    'agent',
                    $query,
                    $query,
                    $query,
                    $query,
                    $query
                );

        $query = $this->db->query($sql, $params);

        return ( $query->num_rows() ) ? $query->result() : FALSE;
    }

}