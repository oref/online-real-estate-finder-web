<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front/user';
$route['404_override'] = 'error';
$route['translate_uri_dashes'] = FALSE;

/**
 *
 * Start of User Routes
 *
 */

/**
 * Error Routes
 */
$route['ajax_user/(:any)'] = 'error';
$route['ajax_admin/(:any)'] = 'error';

/**
 * Custom Routes
 */
$route['user'] = 'user/overview';
$route['about'] = 'front/about';
$route['contact-us'] = 'front/contact_us';
$route['property/listing'] = 'front/listing';
$route['property/single/(:num)'] = 'front/single/$1';
$route['terms'] = 'front/terms';

/**
 * Ajax View Routes e.g. modal, tabs..
 */
$route['user/modal/(:any)'] = 'ajax_user/modal/$1';
$route['user/tab/(:any)'] = 'ajax_user/tab/$1';
/**
 * Ajax Process Routes e.g. form data, email..
 */
$route['user/signin'] = 'ajax_user/signin';
$route['user/signup'] = 'ajax_user/signup';
$route['user/is_email_available'] = 'ajax_user/is_email_available';
$route['user/is_username_available'] = 'ajax_user/is_username_available';
$route['user/is_email_match'] = 'ajax_user/is_email_match';
$route['user/is_password_match'] = 'ajax_user/is_password_match';
$route['user/get_user_details'] = 'ajax_user/get_user_details';
$route['user/update_user'] = 'ajax_user/update_user';
$route['user/change_user_email'] = 'ajax_user/change_user_email';
$route['user/update_user_password'] = 'ajax_user/update_user_password';
$route['user/add_update_property_view_recently'] = 'ajax_user/add_update_property_view_recently';
$route['user/user_verify'] = 'ajax_user/user_verify';
$route['user/email_verify'] = 'ajax_user/email_verify';
$route['user/resend'] = 'ajax_user/resend_confirmation_link';
$route['user/reset'] = 'ajax_user/reset_password';
$route['user/sell_property'] = 'ajax_user/sell_property';
$route['user/add_favorite_property'] = 'ajax_user/add_favorite_property';
$route['user/delete_favorite_property'] = 'ajax_user/delete_favorite_property';
$route['user/property_search'] = 'ajax_user/property_search';
$route['user/get_region_details'] = 'ajax_user/get_region_details';
$route['user/get_province_details'] = 'ajax_user/get_province_details';
$route['user/get_city_details'] = 'ajax_user/get_city_details';
$route['user/get_barangay_details'] = 'ajax_user/get_barangay_details';
$route['user/get_property_types_details'] = 'ajax_user/get_property_types_details';
$route['user/get_all_property_details_by_status/(:any)'] = 'ajax_user/get_all_property_details_by_status/$1';
$route['user/get_all_property_assigned_by_seller'] = 'ajax_user/get_all_property_assigned_by_seller';
$route['user/get_all_conversations'] = 'ajax_user/get_all_conversations';
$route['user/get_all_messages'] = 'ajax_user/get_all_messages';
$route['user/get_recent_message'] = 'ajax_user/get_recent_message';
$route['user/add_message'] = 'ajax_user/add_message';
$route['user/get_all_users/(:any)'] = 'ajax_user/get_all_users/$1';

/**
 *
 * End of User Routes
 *
 */

/**
 * Start of Property Routes
 */
$route['user/property_list'] = 'ajax_user/property_list';
$route['user/favorite_list'] = 'ajax_user/favorite_list';
$route['listing'] = 'front/listing';

/**
 *
 * Start of Admin Routes
 *
 */

/**
 * Custom Routes
 */
$route['admin'] = 'front/admin';
$route['admin/lock'] = 'front/lock';
$route['admin/property/approved'] = 'admin/approved';
$route['admin/property/declined'] = 'admin/declined';
$route['admin/property/sold'] = 'admin/sold';
$route['admin/property/types'] = 'admin/types';
$route['admin/account/buyers'] = 'admin/buyers';
$route['admin/account/sellers'] = 'admin/sellers';
$route['admin/account/agents'] = 'admin/agents';
$route['admin/account/brokers'] = 'admin/brokers';
$route['admin/account/administrators'] = 'admin/administrators';
$route['admin/property_details'] = 'admin/modals/view_sell_property';

/**
 * Ajax Process Routes e.g. form data, email..
 */
$route['admin/modal/(:any)'] = 'ajax_admin/modal/$1';
$route['admin/signin'] = 'ajax_admin/signin';
$route['admin/signup'] = 'ajax_admin/signup';
$route['admin/unlock'] = 'ajax_admin/unlock';
$route['admin/update_admin_password'] = 'ajax_admin/update_admin_password';
$route['admin/is_password_match'] = 'ajax_admin/is_password_match';
$route['admin/get_all_tasks'] = 'ajax_admin/get_all_tasks';
$route['admin/add_task'] = 'ajax_admin/add_task';
$route['admin/update_task_status'] = 'ajax_admin/update_task_status';
$route['admin/delete_property_type'] = 'ajax_admin/delete_property_type';
$route['admin/delete_user_by_id'] = 'ajax_admin/delete_user_by_id';
// $route['admin/get_all_property_by_status/(:any)'] = 'ajax_admin/get_all_property_by_status/$1';
$route['admin/get_all_not_assigned_properties'] = 'ajax_admin/get_all_not_assigned_properties';
$route['admin/get_all_assigned_properties'] = 'ajax_admin/get_all_assigned_properties';
$route['admin/get_all_approved_properties/(:any)'] = 'ajax_admin/get_all_approved_properties/$1';
$route['admin/get_all_declined_properties'] = 'ajax_admin/get_all_declined_properties';
$route['admin/get_all_sold_properties'] = 'ajax_admin/get_all_sold_properties';
$route['admin/get_all_property_types'] = 'ajax_admin/get_all_property_types';
$route['admin/get_admin_details'] = 'ajax_admin/get_admin_details';
$route['admin/get_property_details_by_id'] = 'ajax_admin/get_property_details_by_id';
$route['admin/get_property_type_details_by_id'] = 'ajax_admin/get_property_type_details_by_id';
$route['admin/is_email_available'] = 'ajax_admin/is_email_available';
$route['admin/is_username_available'] = 'ajax_admin/is_username_available';
$route['admin/add_user_by_type/(:any)'] = 'ajax_admin/add_user_by_type/$1';
$route['admin/get_all_users/(:any)'] = 'ajax_admin/get_all_users/$1';
$route['admin/get_user_details_by_type/(:any)'] = 'ajax_admin/get_user_details_by_type/$1';
$route['admin/get_all_property_details_opened_recently'] = 'ajax_admin/get_all_property_details_opened_recently';
$route['admin/update_property_status/(:any)'] = 'ajax_admin/update_property_status/$1';
$route['admin/update_property_agent'] = 'ajax_admin/update_property_agent';
$route['admin/update_property_approve'] = 'ajax_admin/update_property_approve';
$route['admin/update_sold_status'] = 'ajax_admin/update_sold_status';
$route['admin/update_claimed_commission'] = 'ajax_admin/update_claimed_commission';
$route['admin/update_re_assigned_property'] = 'ajax_admin/update_re_assigned_property';
$route['admin/get_all_agents'] = 'ajax_admin/get_all_agents';
$route['admin/get_agents_details'] = 'ajax_admin/get_agents_details';
$route['admin/get_all_conversations'] = 'ajax_admin/get_all_conversations';
$route['admin/get_all_messages'] = 'ajax_admin/get_all_messages';
$route['admin/get_recent_message'] = 'ajax_admin/get_recent_message';
$route['admin/add_message'] = 'ajax_admin/add_message';
$route['admin/sell_property'] = 'ajax_admin/sell_property';
$route['admin/update_property'] = 'ajax_admin/update_property';
$route['admin/update_property_type'] = 'ajax_admin/update_property_type';
$route['admin/update_admin'] = 'ajax_admin/update_admin';
$route['admin/add_property_type'] = 'ajax_admin/add_property_type';
$route['admin/get_region_details'] = 'ajax_admin/get_region_details';
$route['admin/get_province_details'] = 'ajax_admin/get_province_details';
$route['admin/get_city_details'] = 'ajax_admin/get_city_details';
$route['admin/get_barangay_details'] = 'ajax_admin/get_barangay_details';
$route['admin/get_property_types_details'] = 'ajax_admin/get_property_types_details';
$route['admin/get_all_property_assigned'] = 'ajax_admin/get_all_property_assigned';
$route['admin/get_all_sold_property_assigned'] = 'ajax_admin/get_all_sold_property_assigned';
$route['admin/update_property_agent_id'] = 'ajax_admin/update_property_agent_id';
$route['admin/get_all_property_details_by_status/(:any)'] = 'ajax_admin/get_all_property_details_by_status/$1';
/**
 *
 * End of Admin Routes
 *
 */