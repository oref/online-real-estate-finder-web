<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close modal-close">&times;</button>
        <h4 class="modal-title" id="modalRecover">Forgot Password?</h4>
    </div>
    <form method="POST" class="normal-form" role="form" data-form="recover">
        <div class="modal-body clearfix">
            <div class="form-group">
                <label for="email" class="sr-only">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
            </div>

            <div class="pull-right">
                <a href="#" data-bind="login">LogIn</a>
                <span>or</span>
                <a href="#" data-bind="register">Register</a>
            </div>
        </div>
        <div class="modal-footer">
            <span>Enter email above to reset password.</span>
            <button type="submit" class="btn-credential btn btn-flat-alizarin btn-block">Submit</button>
        </div>
    </form>
</div>