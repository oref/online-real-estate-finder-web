<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close modal-close">&times;</button>
        <h4 class="modal-title">Login</h4>
    </div>
    <form method="POST" class="normal-form" role="form" data-form="login">
        <div class="modal-body clearfix">
            <div class="form-group">
                <label for="login" class="sr-only">Username or Email</label>
                <input type="text" class="form-control" name="login" id="login" placeholder="Username or Email">
            </div>
            <div class="form-group">
                <label for="password" class="sr-only">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            </div>

            <div class="pull-right">
                <a href="#" data-bind="recover">Forgot your Password?</a>
            </div>
        </div>
        <div class="modal-footer">
            I do not have an account. Register <a href="#" data-bind="register">here</a>.
            <button type="submit" class="btn-credential btn btn-flat-alizarin btn-block">Log In</button>
        </div>
    </form>
</div>