<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close modal-close">&times;</button>
        <h4 class="modal-title">Send Message</h4>
    </div>
    <form method="POST" class="normal-form" role="form" data-form="message">
        <div class="modal-body clearfix">
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" class="form-control" id="" placeholder="OREF TEAM" readonly />
            </div>

            <div class="form-group">
                <label for="">Email address</label>
                <input type="email" class="form-control" id="" placeholder="onlinerealestatefinder@gmail.com" readonly />
            </div>

            <div class="form-group">
                <label for="">Telephone</label>
                <input type="tel" class="form-control" id="" placeholder="09325421343" readonly />
            </div>

            <div class="form-group">
                <label for="">Message</label>
                <textarea class="form-control" rows="3"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn-credential btn btn-flat-alizarin btn-block">Send</button>
        </div>
    </form>
</div>