<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close modal-close">&times;</button>
        <h4 class="modal-title">Register</h4>
    </div>
    <form method="POST" class="normal-form" role="form" data-form="register">
        <div class="modal-body">
            <div class="form-group">
                <label for="firstname" class="sr-only">First Name</label>
                <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name">
            </div>
            <div class="form-group">
                <label for="" class="sr-only">Last Name</label>
                <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last Name">
            </div>
            <div class="form-group">
                <label for="email" class="sr-only">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
            </div>

            <div class="form-group">
                <label for="confirmemail" class="sr-only">Confirm Email</label>
                <input type="email" class="form-control" name="confirmemail" id="confirmemail" placeholder="Re-type Email">
            </div>

            <div class="form-group">
                <label for="password" class="sr-only">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            </div>

            <div class="form-group">
                <label for="confirmpassword" class="sr-only">Password</label>
                <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm Password">
            </div>
            <div class="form-group">
                <label class="label-block text-center">Type of User</label>
                <div class="btn-group btn-group-justified   " data-toggle="buttons">
                    <label class="btn btn-flat-asbestos active">
                        <input type="radio" class="radio inline" name="type" value="buyer" checked>Buyer
                    </label>
                    <label class="btn btn-flat-asbestos">
                        <input type="radio" class="radio inline" name="type" value="seller">Seller
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            Already have an account? Log in <a href="#" data-bind="login">here</a>.
            <button type="submit" class="btn-credential btn btn-flat-alizarin btn-block">Register</button>
        </div>
    </form>
</div>