<div class="tab-content">
    <div class="row">
        <form method="POST" class="normal-form form-horizontal clearfix" enctype="multipart/form-data">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="username" name="username">
                        <!-- <span class="help-block">This is your username.</span> -->
                    </div>
                </div>

                <div class="form-group">
                    <label for="firstname" class="col-sm-3 control-label">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="firstname" name="firstname">
                        <!-- <span class="help-block">Sample of Help Block.</span> -->
                    </div>
                </div>

                <div class="form-group">
                    <label for="lastname" class="col-sm-3 control-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="lastname" name="lastname">
                        <!-- <span class="help-block">Sample of Help Block.</span> -->
                    </div>
                </div>

                <div class="form-group">
                    <label for="contactno" class="col-sm-3 control-label">Contact No.</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="contactno" name="contactno">
                        <!-- <span class="help-block">Number Only.</span> -->
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Address</label>
                    <div class="col-sm-9">
                        <select name="region_id" id="regions" class="form-control selectpicker"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <select name="province_id" id="provinces" class="form-control selectpicker" disabled></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <select name="city_id" id="cities" class="form-control selectpicker" disabled></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <select name="barangay_id" id="barangays" class="form-control selectpicker" disabled></select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="about" class="col-sm-3 control-label">About</label>
                    <div class="col-sm-9">
                        <textarea rows="5" id="about" name="about"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="tags" class="col-sm-3 control-label">Interest Tags</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="tags" name="tags" data-bind="tags" />
                        <span class="help-block">Type and press enter for tags.</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="avatar-wrapper text-center">
                    <input id="avatar" name="avatar" type="file" class="file-loading" />
                </div>
                <div class="avatar-errors center-block"></div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                        <button type="submit" class="btn btn-flat-warning">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>