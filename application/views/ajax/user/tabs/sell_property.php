<div class="tab-content">
    <div class="row">
        <div class="col-md-12">
            <form class="normal-form form-horizontal" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <input id="property_images" name="property_images[]" type="file" multiple />
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="firstname" class="col-sm-3 control-label">Price</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="price" name="price">
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Address</label>
                    <div class="col-sm-9">
                        <select name="region_id" id="regions" class="form-control selectpicker"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <select name="province_id" id="provinces" class="form-control selectpicker" disabled></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <select name="city_id" id="cities" class="form-control selectpicker" disabled></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <select name="barangay_id" id="barangays" class="form-control selectpicker" disabled></select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="lastname" class="col-sm-3 control-label">Bathroom/s</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="bathroom" name="bathroom">
                        <!-- <span class="help-block">Sample of Help Block.</span> -->
                    </div>
                </div>

                <div class="form-group">
                    <label for="lastname" class="col-sm-3 control-label">Bedroom/s</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="bedroom" name="bedroom">
                        <!-- <span class="help-block">Sample of Help Block.</span> -->
                    </div>
                </div>

                <div class="form-group">
                    <label for="type" class="col-sm-3 control-label">Type</label>
                    <div class="col-sm-9">
                        <select name="type_id" id="types" class="form-control selectpicker"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="features" class="col-sm-3 control-label">Feature/s</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="features" name="features" data-bind="tags" />
                        <span class="help-block">Type and press enter for features.</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="about" class="col-sm-3 control-label">Info</label>
                    <div class="col-sm-9">
                        <textarea rows="5" id="info" name="info"></textarea>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                            <button type="submit" class="btn btn-flat-warning">Request</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>