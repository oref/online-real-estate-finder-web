<div class="tab-content">
	<div class="row">
		<form class="normal-form form-horizontal clearfix" data-form="email" method="POST">
			<div class="col-md-12">

				<h4 class="form-title">Change Email</h4>
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Old Email</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="old_email" name="old_email" />
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">New Email</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="new_email" name="new_email" />
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-3 control-label">Confirm New Email</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="confirm_email" name="confirm_email" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button type="submit" class="btn btn-flat-warning">Update</button>
					</div>
				</div>
			</div>
		</form>
		<form class="normal-form form-horizontal clearfix" data-form="password" method="POST">
			<div class="col-md-12">
				<h4 class="form-title">Change Password</h4>
				<div class="form-group">
					<label for="old_password" class="col-sm-3 control-label">Old Password</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="old_password" name="old_password" />
					</div>
				</div>

				<div class="form-group">
					<label for="new_password" class="col-sm-3 control-label">New Password</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="new_password" name="new_password" />
					</div>
				</div>

				<div class="form-group">
					<label for="confirm_password" class="col-sm-3 control-label">Confirm New Password</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="confirm_password" name="confirm_password" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button type="submit" class="btn btn-flat-warning">Update</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>