<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Send Email</h4>
    </div>
    <div class="modal-body">

        <form class="form-horizontal" role="form">
            <div class="card-body card-padding">
                <div class="form-group ">
                    <div class="col-sm-12 fg-float">
                        <div class="fg-line">
                            <!-- input value should be the email of the user where the button is triggered -->
                            <input type="text" class="form-control fg-input input-sm" id="receiver" value="Hello" disabled="TRUE">
                        </div>
                        <label class="fg-label" style="padding-left: inherit;">To</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 fg-float">
                        <div class="fg-line">
                            <input type="text" class="form-control fg-input input-sm" id="subject">
                        </div>
                        <label class="fg-label" style="padding-left: inherit;">Subject</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="fg-line">
                            <textarea class="form-control" rows="10" placeholder="Enter Message Here..."></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-sm" data-dismiss="modal">Send</button>
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
    </div>
</div>