<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">View Property Details</h4>
    </div>
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
        <div class="modal-body">

            <div class="card-body card-padding">

                <div class="form-group">
                    <h5>Property Information</h5>
                </div>
                <input type="hidden" name="id" />
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="name" name="name">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="price" name="price">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="regions" class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" name="region_id" id="regions" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="provinces" class="col-sm-2 control-label">Province</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" name="province_id" id="provinces" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cities" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" name="city_id" id="cities" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="barangays" class="col-sm-2 control-label">Barangay</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" name="barangay_id" id="barangays" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <h5>Other Information</h5>
                </div>

                <div class="form-group">
                    <label for="bathrooms" class="col-sm-2 control-label">Bathroom/s</label>
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="bathrooms" name="bathrooms">
                        </div>
                    </div>
                    <label for="bedrooms" class="col-sm-2 control-label">Bedroom/s</label>
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="bedrooms" name="bedrooms">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="features" class="col-sm-2 control-label">Feature/s</label>
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="features" name="features" data-bind="tags">
                            <span class="help-block">Type and press enter for features.</span>
                        </div>
                    </div>
                    <label for="type" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="type" name="type_id">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control noresize" rows="10" id="info" name="info" placeholder="Info/Description"></textarea>
                    </div>
                </div>

                <div class="row property-images"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>