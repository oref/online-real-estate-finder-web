<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Add Property type</h4>
    </div>
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
        <div class="modal-body">

            <div class="card-body card-padding">

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" id="type" name="type" placeholder="Type Name">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-sm">Add</button>
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>