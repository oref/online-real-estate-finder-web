<div class="modal-content">
    <div class="modal-header" style="background: #f8f8f8;">
        <h4 class="modal-title"></h4>
    </div>
    <div class="modal-body" style="background: #f8f8f8;">

        <div class="card-body card-padding">
            <div class="row">
                <div class="col-sm-5">
                    <div class="card" id="profile-main">
                        <div class="pm-overview">
                            <div class="pmo-pic">

                                <div class="p-relative">
                                    <img class="img-responsive img-circle avatar" src="">
                                </div>

                                <div class="pmo-stat">
                                    <h4 class="m-0 c-white"><span class="firstname"></span> <span class="lastname"></span></h4>
                                </div>
                            </div>

                            <div class="pmo-block pmo-contact hidden-xs">
                                <ul>
                                    <li><i class="zmdi zmdi-account-circle"></i> Username: <span class="username"></span></li>
                                    <li><i class="zmdi zmdi-time"></i>Date Joined: <span class="date_registered"></span></li>
                                    <li><i class="zmdi zmdi-phone"></i>Contact Number: <span class="contactno"></span></li>
                                    <li><i class="zmdi zmdi-email"></i>Email Address: <span class="email"></span></li>
                                    <li>
                                        <i class="zmdi zmdi-pin"></i>
                                        <address class="m-b-0 address"></address>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-7 m-b-25">
                    <div class="pm-body clearfix">
                        <div class="pmb-block">

                            <div class="pmbb-header">
                                <h4><i class="zmdi zmdi-chart m-r-5"></i>About</h4>
                            </div>
                            <div class="pmbb-body p-1-30">
                                <div class="card">
                                    <span class="about"></span>
                                </div>
                            </div>

                            <div class="pmbb-header">
                                <h4><i class="zmdi zmdi-eye m-r-5"></i>Timestamp</h4>
                            </div>
                            <div class="pmbb-body p-1-30">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th data-column-id="Timestamp" data-order="desc">Last Login</th>
                                                    <th data-column-id="Timestamp" data-order="desc">Date Modified</th>
                                                    <th data-column-id="Timestamp" data-order="desc">Date Registered</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- maximum of 5s  -->
                                                <tr>
                                                    <td class="last_login"></td>
                                                    <td class="date_modified"></td>
                                                    <td class="date_registered"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="pmbb-header">
                                <h4><i class="zmdi zmdi-label m-r-5"></i>Tags</h4>
                            </div>
                            <div class="pmbb-body p-1-30">
                                <div class="card">
                                    <span class="tags"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="modal-footer" style="background: #f8f8f8;">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
    </div>
</div>