<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Property</h4>
    </div>
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
        <div class="modal-body">

            <div class="card-body card-padding">

                <div class="form-group">
                    <h5>Property Information</h5>
                </div>
                <input type="hidden" name="id" />
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" id="name" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" id="price" name="price">
                    </div>
                </div>

                <div class="form-group">
                    <label for="regions" class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-10">
                        <select name="region_id" id="regions" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="provinces" class="col-sm-2 control-label">Province</label>
                    <div class="col-sm-10">
                        <select name="province_id" id="provinces" class="form-control" disabled>
                            <option value="none">-Select region first-</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cities" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <select name="city_id" id="cities" class="form-control" disabled>
                            <option value="none">-Select province first-</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="barangays" class="col-sm-2 control-label">Barangay</label>
                    <div class="col-sm-10">
                        <select name="barangay_id" id="barangays" class="form-control" disabled>
                            <option value="none">-Select city first-</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <h5>Other Information</h5>
                </div>

                <div class="form-group">
                    <label for="bathrooms" class="col-sm-2 control-label">Bathroom/s</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" id="bathrooms" name="bathrooms">
                    </div>
                    <label for="bedrooms" class="col-sm-2 control-label">Bedroom/s</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" id="bedrooms" name="bedrooms">
                    </div>
                </div>

                <div class="form-group">
                    <label for="features" class="col-sm-2 control-label">Feature/s</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" id="features" name="features" data-bind="tags">
                        <span class="help-block">Type and press enter for features.</span>
                    </div>
                    <label for="type" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-4">
                        <select class="form-control selectpicker" id="types" name="type_id"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="info" class="col-sm-2 control-label">Info / Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control noresize" rows="10" id="info" name="info"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-sm">Update</button>
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>