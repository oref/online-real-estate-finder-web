<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title text-center confirm">Confirmation <i class="fa fa-exclamation"></i></h4>
    </div>
    <form class="form-horizontal" method="post">
        <div class="modal-body text-center">
            <b>Are you sure you about this property?</b>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-danger btn-sm">Continue</button>
            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>