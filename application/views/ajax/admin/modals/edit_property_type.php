<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Property type</h4>
    </div>
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
        <div class="modal-body">

            <div class="card-body card-padding">

                <div class="form-group">
                    <input type="hidden" name="id" />
                    <label for="name" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="type" name="type" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-sm">Edit</button>
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>