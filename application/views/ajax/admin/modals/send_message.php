<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">New Message</h4>
    </div>
    <form method="post" id="msg-form">
        <div class="modal-body">
            <div class="form-group">
                <div class="message-search">
                    <div class="fg-line">
                        <input type="hidden" name="to_id" id="to_id" />
                        <input type="text" name="to" id="to" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <textarea class="form-control" name="msg" rows="10" placeholder="Write a message..."></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary waves-effect" id="submit_msg">Send</button>
        </div>
    </form>
</div>