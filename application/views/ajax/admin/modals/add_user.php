<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Add User</h4>
    </div>
    <form class="form-horizontal" method="post">
        <div class="modal-body">

            <div class="card-body card-padding">

                <div class="form-group">
                    <h5>Personal Information</h5>
                </div>

                <div class="form-group">
                    <label for="firstname" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-5">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="firstname" name="firstname" placeholder="First Name">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="lastname" name="lastname" placeholder="Last Name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="regions" class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <select name="region_id" id="regions" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="provinces" class="col-sm-2 control-label">Province</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <select name="province_id" id="provinces" class="form-control" disabled>
                                <option value="none">-Select region first-</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cities" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <select name="city_id" id="cities" class="form-control" disabled>
                                <option value="none">-Select province first-</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="barangays" class="col-sm-2 control-label">Barangay</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <select name="barangay_id" id="barangays" class="form-control" disabled>
                                <option value="none">-Select city first-</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <h5>Contact Information</h5>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">E-Address</label>
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <input type="email" class="form-control input-sm" id="email" name="email" placeholder="example@example.com">
                        </div>
                    </div>
                    <label for="contactno" class="col-sm-2 control-label">Contact No</label>
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="contactno" name="contactno" placeholder="XXX-XXXX">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-sm">Add</button>
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
        </div>
    </form>
</div>