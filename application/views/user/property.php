<!-- main tab-content -->
<div class="tab-content no-bg no-border">
	<?php if( $user->type === 'seller' || $user->type === 'superadmin' ): ?>
	<ul id="property-tabs" class="nav nav-tabs nav-tabs-line">
			<li class="active"><a href="#" data-target="#sell_property">Sell Property</a></li>
			<li><a href="#" data-target="#approved_properties">My property</a></li>
	</ul>
	<!-- inner tab-content -->
	<div class="tab-content tab-content-account">
		<!-- Ajax tab content here... -->
		<div class="tab-pane fade in active" id="sell_property"></div>
		<div class="tab-pane fade" id="approved_properties"></div>
	</div><!-- end inner tab-content -->
	<?php else: ?>
		<div class="tab-content tab-content-account">
			<div class="text-center"><font color="#ff0000"><h3>Page Not Found!</h3></font></div>
			<img width="100%" src="<?php echo base_url(); ?>assets/custom/img/error_img/404.gif">
		</div>
	<?php endif; ?>
</div><!-- end main tab-content -->