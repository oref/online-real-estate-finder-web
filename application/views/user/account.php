<!-- main tab-content -->
<div class="tab-content no-bg no-border">
	<ul id="profile-tabs" class="nav nav-tabs nav-tabs-line">
		<li class="active"><a href="#" data-target="#edit_profile">Edit Profile</a></li>
		<!-- <li><a href="#" data-target="#email_preferences">Email Preferences</a></li> -->
		<li><a href="#" data-target="#change_credentials">Change Email &amp; Password</a></li>
	</ul>
	<!-- inner tab-content -->
	<div class="tab-content tab-content-account">
		<!-- Ajax tab content here... -->
		<div class="tab-pane fade in active" id="edit_profile"></div>
		<!-- <div class="tab-pane fade" id="email_preferences"></div> -->
		<div class="tab-pane fade" id="change_credentials"></div>
	</div><!-- end inner tab-content -->
</div><!-- end main tab-content -->