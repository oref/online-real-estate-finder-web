<div class="row">
<!-- product Listing -->
    <div id="product-list" class="product-list full-listing grid-layout clearfix">
        <div class="col-sm-12 col-md-12 the-title text-left">
            <h2>My Favorites</h2>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="filter-bar clearfix">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <h5 class="filter-title">Showing <span id="filtered_property_count">number</span> of <span id="total_property_count"></span> results</h5>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7">
                        <form class="form-inline pull-right">
                            <ul class="list-unstyled filter-gridlist">
                                <li class="filter-select">
                                    Sort by:
                                    <select class="form-control selectpicker" name="sort">
                                        <option value="date_added|desc">Most Recent</option>
                                        <option value="price|desc">Highest Price</option>
                                        <option value="price|asc">Lower Price</option>
                                    </select>
                                </li>
                                <li class="filter-select">
                                    Show:
                                    <select class="form-control selectpicker" name="offset">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="all">All</option>
                                    </select>
                                    Entries
                                </li>
                                <li class="hidden-xs">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Grid View" id="gridview" class="switcher active"><i class="fa fa-th"></i></a>
                                </li>
                                <li class="hidden-xs">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="List View" id="listview" class="switcher"><i class="fa fa-th-list"></i></a>
                                </li>
                                <li class="hidden-xs" style="display: none;"> <!-- Remove "display: none;" for the map marker icon -->
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Map View"><i class="fa fa-map-marker"></i></a>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="property-list panel"></div>
    <!-- End product Listing -->
    </div>
    <div class="col-md-12">
        <nav class="pagination-list text-right"></nav>
    </div>
</div>