<div class="tab-content no-bg no-border">
	<!-- Listing Sale -->
	<div class="product-list grid-layout clearfix">
		<div class="row">
			<div class="col-sm-12 col-md-12 the-title text-left">
				<div class="row">
					<div class="col-md-7 col-sm-7 col-xs-8">
						<h3>Welcome</h3>
						<span>Thank you for registering to the <font color="red">Online Real Estate Finder</font> website</span>
					</div>
				</div>
			</div>
			<div class="container-fluid text-justify">
				Thank you for registering with us. You will receive an email from us shortly, confirming your registration request and including summary of your registration details.
				<br><br>
				We strongly suggest to update your profile so the broker and agents would know a little bit about you.
				<br><br>
				When making a transaction using the OREF website we would like to ask you to be as honest as possible. We discourage bogus information since this will result to bad customer and client experience.
				<br><br>
				If you have comments or suggestions regarding the website please do not hesitate to contact us simply by clicking on the Contact us link below the page.
			</div>
		</div>
	</div>
</div>