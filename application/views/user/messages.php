<div class="card m-b-0" id="messages-main">

    <div class="ms-menu">
        <div class="ms-block">
            <div class="ms-user">
                <?php if( !empty( $user->avatar ) ): ?>
                    <img src="<?php echo base_url() . $user->avatar; ?>" alt="avatar">
                <?php else: ?>
                    <div class="lv-avatar bgm-<?php echo $user->avatar_bg_color; ?>"><?php echo strtoupper($user->firstname[0]); ?></div>
                <?php endif; ?>
                <span>Signed in as <strong><?php echo $user->firstname . ' ' . $user->lastname; ?></strong></span>
            </div>
        </div>

        <div class="ms-block">
            <button class="btn btn-primary btn-block" data-bind="send_message">New Message</button>
        </div>

        <div class="listview lv-user m-t-20 conversation-list">

        </div>


    </div>

    <div class="ms-body">
        <div class="listview lv-message">
            <div class="lv-header-alt clearfix">
                <div id="ms-menu-trigger">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </div>

                <div class="lvh-label hidden-xs">
                    <div class="lv-avatar pull-left" id="msg-avatar"></div>
                    <span class="c-black" id="msg-title"></span>
                </div>

                <ul class="lv-actions actions">
                    <li>
                        <a href="">
                            <i class="zmdi zmdi-delete"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="zmdi zmdi-time"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-sort"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="">Latest</a>
                            </li>
                            <li>
                                <a href="">Oldest</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="">Refresh</a>
                            </li>
                            <li>
                                <a href="">Message Settings</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="lv-body message-list">

            </div>

            <div class="lv-footer ms-reply">
                <form method="post" role="form" id="msg-form">
                    <input type="hidden" name="to_id" id="send_to_id" />
                    <textarea name="msg" id="send_to_msg" placeholder="Write a reply..."></textarea>
                    <button type="submit"><i class="zmdi zmdi-mail-send"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>