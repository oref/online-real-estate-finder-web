<div class="container overflow-hidden">
    <div class="row row-offcanvas row-offcanvas-left row-offcanvas-sm row-offcanvas-sm-left row-offcanvas-xs box-padding">
        <div class="col-md-3 sidebar-offcanvas sidebar-offcanvas-sm">
            <section id="sidebar">
                <section id="aside-product-search">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Search Listing -->
                            <div class="product-list-search aside" id="searchPanel">
                                <div class="inner">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default panel-search">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                        Advanced Search
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in panel-all-collapse">
                                                <div class="panel-body">
                                                    <form class="form-search clearfix">
                                                        <div class="form-group col-lg-12">
                                                            <label for="name">Search by Name</label>
                                                            <input type="text" class="form-control" placeholder="Enter keyword.." name="name" id="name">
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="type">Type</label>
                                                            <select name="type_id" id="types" class="form-control selectpicker"></select>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="regions">Region</label>
                                                            <select name="region_id" id="regions" class="form-control selectpicker"></select>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="provinces">Province</label>
                                                            <select name="province_id" id="provinces" class="form-control selectpicker" disabled></select>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="cities">City</label>
                                                            <select name="city_id" id="cities" class="form-control selectpicker" disabled></select>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="barangays">Barangay</label>
                                                            <select name="barangay_id" id="barangays" class="form-control selectpicker" disabled></select>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="bedrooms">Bedrooms</label>
                                                            <input type = "text" class="form-control selectpicker" placeholder="Enter quantity.." name="bedrooms" id="bedrooms">
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="bathrooms">Bathrooms</label>
                                                            <input type = "text" class="form-control selectpicker" placeholder="Enter quantity.." name="bathrooms" id="bathrooms">
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="price_from">Price: Minimum</label>
                                                            <input type = "text" class="form-control selectpicker" placeholder="from.." name="price_from" id="price_from">
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <label for="price_to">Price: Maximum</label>
                                                            <input type = "text" class="form-control selectpicker" placeholder="to.." name="price_to" id="price_to">
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <button type="submit" class="btn-search btn btn-flat-alizarin btn-search"> Refine Search</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end Search Listing -->
                        </div>
                    </div>
                </section>
            </section>
        </div>
        <div class="col-md-9">
            <section id="content">
                <div class="row">
                <!-- product Listing -->
                    <div id="product-list" class="product-list full-listing grid-layout clearfix">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-flat-asbestos btn-icon hidden-md hidden-lg text-left" data-toggle="offcanvas-sm">
                                <i class="fa fa-share"></i> Advanced Filter
                            </button>
                        </div>
                        <div class="col-sm-12 col-md-12 the-title text-left">
                            <h2>Property for Sale in City</h2>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="filter-bar clearfix">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-5 col-md-5">
                                        <h5 class="filter-title">Showing <span id="filtered_property_count">number</span> of <span id="total_property_count"></span> results</h5>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7">
                                        <form class="form-inline pull-right">
                                            <ul class="list-unstyled filter-gridlist">
                                                <li class="filter-select">
                                                    Sort by:
                                                    <select class="form-control selectpicker" name="sort">
                                                        <option value="date_added|desc">Most Recent</option>
                                                        <option value="price|desc">Highest Price</option>
                                                        <option value="price|asc">Lower Price</option>
                                                    </select>
                                                </li>
                                                <li class="filter-select">
                                                    Show:
                                                    <select class="form-control selectpicker" name="offset">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="all">All</option>
                                                    </select>
                                                    Entries
                                                </li>
                                                <li class="hidden-xs">
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Grid View" id="gridview" class="switcher active"><i class="fa fa-th"></i></a>
                                                </li>
                                                <li class="hidden-xs">
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="List View" id="listview" class="switcher"><i class="fa fa-th-list"></i></a>
                                                </li>
                                                <li class="hidden-xs" style="display: none;"> <!-- Remove "display: none;" for the map marker icon -->
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Map View"><i class="fa fa-map-marker"></i></a>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="property-list panel"></div>
                    <!-- End product Listing -->
                    </div>
                    <div class="col-md-12">
                        <nav class="pagination-list text-right"></nav>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>