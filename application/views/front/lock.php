<div class="lc-block lcb-alt toggled" id="l-lockscreen">

    <img class="lcb-user" src="<?php echo base_url() . $admin->avatar; ?>" title="<?php echo $admin->firstname . " " . $admin->lastname; ?>">

    <form method="post" id="admin-lock">
        <div class="fg-line">
            <input type="password" class="form-control text-center" id="password" name="password" placeholder="Password" />
        </div>

        <button type="submit" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
    </form>
    <ul class="login-navigation">
        <li data-block="#l-login" class="bgm-green">Login</li>
    </ul>
</div>

<!-- Login -->
<div class="lc-block" id="l-login">
    <form method="post" id="admin-login">
        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
            <div class="fg-line">
                <input type="text" name="login" class="form-control" placeholder="Username" />
            </div>
        </div>

        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
            <div class="fg-line">
                <input type="password" name="password" class="form-control" placeholder="Password" />
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="keep" />
                <i class="input-helper"></i>
                Keep me signed in
            </label>
        </div>

        <button type="submit" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>

        <ul class="login-navigation">
            <li data-block="#l-lockscreen" class="bgm-orange">Unlock</li>
        </ul>
    </form>
</div>