<!-- Login -->
<div class="lc-block toggled" id="l-login">
    <form method="post" id="admin-login">
        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
            <div class="fg-line">
                <input type="text" name="login" class="form-control" placeholder="Username" />
            </div>
        </div>

        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
            <div class="fg-line">
                <input type="password" name="password" class="form-control" placeholder="Password" />
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="keep" />
                <i class="input-helper"></i>
                Keep me signed in
            </label>
        </div>

        <button type="submit" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>

        <ul class="login-navigation">
            <li data-block="#l-forget-password" class="bgm-orange">Forgot Password?</li>
        </ul>
    </form>
</div>

<!-- Forgot Password -->
<div class="lc-block" id="l-forget-password">
    <form method="post">
        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo lorem fringilla enim feugiat commodo sed ac lacus.</p>

        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
            <div class="fg-line">
                <input type="text" class="form-control" placeholder="Email Address">
            </div>
        </div>

        <button type="submit" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></submit>

        <ul class="login-navigation">
            <li data-block="#l-login" class="bgm-green">Login</li>
        </ul>
    </form>
</div>