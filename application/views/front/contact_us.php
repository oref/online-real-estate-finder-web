<section id="title-section">
    <div class="title-section-img">
        <img src="<?php echo base_url(); ?>assets/custom/img/coverphoto/img-contact.jpg">
    </div>
    <div class="title-section">
        <div class="col-md-12">
            <div class="title-section-content">
                <h1 class="title-heading text-center">Contact Us</h1>
                <span class="title-desc text-center">We'd love to hear from you. If you have questions and suggestions feel free to contact us.</span>
            </div>
        </div>
    </div>
</section>

<section id="content">
<!-- contact -->
        <div class="contact clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-8">
                        <div class="contact-content box">
                            <form method="post" class="normal-form" id="msg-form">
                                <div class="form-group">
                                    <input type="hidden" name="to_id" id="to_id" value="1" />
                                    <label for="">Name</label>
                                    <input type="text" class="form-control" id="to" name="to" placeholder="OREF TEAM" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="">Email address</label>
                                    <input type="email" class="form-control" id="" placeholder="onlinerealestatefinder@gmail.com" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="">Message</label>
                                    <textarea class="form-control" name="msg" rows="6"></textarea>
                                </div>
                                <button type="submit" class="btn btn-flat-warning" id="submit_msg">Submit</button>

                            </form>
                        </div>
                    </div>

                    <div class="col-sm-4 col-md-4">
                        <div class="box no-padding">
                            <div class="embedded-maps">
                                <iframe src="https://www.google.com/maps/embed/v1/place?q=10.295143045609677,+123.88050019741058&key=AIzaSyD-JodMGqvXEI7m301AaN_PvCrpp0VdYrw" width="100%" height="250" frameborder="0" style="border:0"></iframe>
                            </div>
                            <div class="address">
                                <address>
                                    <strong>OREF, Inc.</strong><br>
                                    N. Bacalso Avenue<br>
                                    Cebu City, Philippines, 6000<br>
                                    <abbr title="Phone">P:</abbr> (032)261-7741
                                </address>

                                <address>
                                    <strong>Full Name</strong><br>
                                    <a href="mailto:#">Online Real Estate Finder</a>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>