<section id="about">
    <div class="jumbotron">
        <div class="container">
            <div class="page-header">
                <h1>About Us</h1>
            </div>

            <div class="page-body">
                <big>Online property selling and buying is now a trend however there are still developers who are using the traditional way of tracking sold and available properties. The group had made an abrupt interview with a client and asked the issues that the client was experiencing. We have noted that the client is tracking the commission earned by the real estate agent or if the agent has reached the specified quota for the month manually. On the other hand, to know the dilemma of the real estate agents the group went into the mall and observed real estate agents trying to attract and convince customer to buy a property. We have noticed that real estate agents have much time wasted looking for a buyer since not all shoppers are into buying properties. The group has decided to create a system that would allow developers check available and sold properties. The system will be named as Online Real estate finder. This system will give the developer a glance on the commissions of each and every real estate agent and this will determine if the agent has reached or below quota. The system promised to provide the image of the property available and the real estate agent contact information when a buyer is interested of the said property. This will also provide an efficient way of providing information needed by the buyer and is the best way to save time and effort in all parties.</big>
            </div>
            <br>
            <div class="page-body">
            <h4>The purpose of this product is</h4>
            <ul>
                <li>Reduce operational costs</li>
                <li>Allow the developers to post a new and available properties.</li>
                <li>Entry of total sales will be stored electronically, making it easier to view the monthly quota.</li>
                <li>Ensure accuracy </li>
                <li>Since Online Real Estate Finder software is of interest to multiple business. The intent of this project is to create a flexible system that can be accessed in multiple locations.</li>
                <li>In general the product scope focuses in providing convenience and efficiency in looking for available properties.</li>
            </ul>
            <h4>The Limitations of this product is</h4>
            <ul>
                <li>The system is not capable of payments transactions.</li>
                <li>In the meantime the system focuses on providing services solely for Cebu customers only.</li>
                <li>Exclusive only for web.</li>
            </ul>
            </div>
            <br>
            <div class="page-header">
                <h3>The Developers</h3>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#"><img class="media-object img-circle" src="<?php echo base_url(); ?>assets/custom/img/profile-pics/michael.jpg" height="300" width="300"></a>
                </div>
                <div class="media-body page-body">
                    <h4 class="media-heading">Michael Amaleona</h4>
                    <b>Age:</b>19 <br>
                    <b>Course:</b> Bachelor of Science in Information Technology<br>
                    <b>Birthdate:</b> February 12, 1996<br>
                    <b>Comments:</b><br>
                    Hi, Im Michael Amaleona a 3rd year BSIT (Bachelor of Science in Information Technology) student of Cebu Institute of Technology - University. Being one of the developers in this system requires alot of patience and determination. Creating an online real estate web application is a bit challenging since we do not have any degree in finance, business administration, construction management or urban development. We didnt take necessary classes and assessments to get a real estate license however with a thorough research were able to come up with brllian ideas on how to create the real estate finder web application. I must say that the success of this project is the group itself.
                    <br>
                    <br>
                    This is the begining of our success.
                </div>
            </div>
            <br>
            <div class="media">
                <div class="media-body page-body">
                    <div class="text-right"><h4 class="media-heading">Christian Denver Solis</h4></div>
                    Hi, Im Christian Denver Solis a 3rd year BSIT (Bachelor of Science in Information Technology) student of Cebu Institute of Technology - University. Being the one of the developers in this system, its not easy to came up with this idea, I thought there will be a never ending problems that we might encounter but the help of my teammates, We accomplish this system by brainstroming, trial and error proccess, meetings and so many to mention.
                    <br>
                    <br>
                                    </div>

                <div class="media-right">
                    <a href="#"><img class="media-object img-circle" src="<?php echo base_url(); ?>assets/custom/img/profile-pics/koling.jpg" height="300" width="300"></a>
                </div>
            </div>
            <br>
            <div class="media">
                <div class="media-left">
                    <a href="#"><img class="media-object img-circle" src="<?php echo base_url(); ?>assets/custom/img/profile-pics/glenn.jpg" height="300" width="300"></a>
                </div>
                <div class="media-body page-body">
                    <h4 class="media-heading">Glenn V. Casipong</h4>
                    <b>Age:</b>27<br>
                    <b>Course:</b> Bachelor of Science in Information Technology<br>
                    <b>Birthdate:</b> August 14, 1988<br>
                    <b>Comments:</b><br>
                     Being one of the developers in this system, there were times when I literally felt as though I was drowning – because I had never done such a thing, and I wondered whether I was ever going to get our head around what seemed such a complicated process.
                    But were determined to learn, and by pursuing such a sizeable project, from the outset, it forced us to start talking to real estate agents, council town planners, civil engineers and the like. The learning process was incredible. Little by little, and with dogged persistence, we started to understand. Never before had we attempted something of this magnitude – but the more we asked questions the more we seemed to comprehend.
                    <br>
                    <br>

                </div>
            </div>
            <br>
            <div class="media">
                <div class="media-body page-body">
                    <div class="text-right"><h4 class="media-heading">George William Villahermosa</h4></div>
                   <div class = "text-right"> 20 <b>: Age</b><br>
                    Bachelor of Science in Computer Engineering <b>: Course</b> <br>
                    November 23, 1994 <b>: Birthdate</b><br>
                    <b>Comments:</b><br>
                    Hi, Im George William Villahermosa a 4rd year BSCpE (Bachelor of Science in Computer Engineering) student of Cebu Institute of Technology - University. Being the one of the developers in this system is something that i can be proud of. My motivation is service. I want to make sure that the buyer’s requirements are met and that they get the value of their investment. If buyers get what they want and they are satisfied, I’m happy and motivated to always give my best to every transaction.
                    </div>
                    <br>
                    <br>
                </div>

                <div class="media-right">
                    <a href="#"><img class="media-object img-circle" src="<?php echo base_url(); ?>assets/custom/img/profile-pics/george.jpg" height="300" width="300"></a>
                </div>
            </div>
        </div>
    </div>
</section>