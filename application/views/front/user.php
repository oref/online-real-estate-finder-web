<section id="product-search">
    <div class="container-fluid">
        <div class="row">

            <!-- Search Listing -->
            <div class="product-list-search sidebar-offcanvas">
                <form class="form-search" method="post">
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                        <div class="product-list-search-inner">
                            <div class="col-md-12">
                                <div class="col-md-5 col-sm-5 head-title">
                                    <h2>Find Property</h2>
                                    <br>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" placeholder="Enter keyword.." name="name" id="name">
                            </div>

                            <div class="form-group col-md-6 col-sm-6">
                                <label for="price_from">Price From</label>
                                <input type="text" class="form-control" name="price_from" placeholder="Min" id="price_from">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="price_from">Price To</label>
                                <input type="text" class="form-control" name="price_to" placeholder="Max" id="price_to">
                            </div>

                            <div class="form-group col-md-6 col-sm-6">
                                <label for="types">Property Type</label>
                                <select name="type_id" id="types" class="form-control selectpicker"></select>
                            </div>

                            <div class="form-group col-md-6 col-sm-6">
                                <label for="regions">Region</label>
                                <select name="region_id" id="regions" class="form-control selectpicker"></select>
                            </div>

                            <div class="form-group col-md-4 col-sm-4">
                                <label for="provinces">Province</label>
                                <select name="province_id" id="provinces" class="form-control selectpicker" disabled></select>
                            </div>

                            <div class="form-group col-md-4 col-sm-4">
                                <label for="cities">City</label>
                                <select name="city_id" id="cities" class="form-control selectpicker" disabled></select>
                            </div>

                            <div class="form-group col-md-4 col-sm-4">
                                <label for="barangays">Barangay</label>
                                <select name="barangay_id" id="barangays" class="form-control selectpicker" disabled></select>
                            </div>

                            <div class="form-group form-action col-md-12">
                                <button type="submit" class="btn-search btn btn-flat-alizarin">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end Search Listing -->

            <!-- product Slideshow -->
            <div class="product-main-slider">
                <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
                    <div id="carousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel" data-slide-to="1"></li>
                            <li data-target="#carousel" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="<?php echo base_url() ?>assets/custom/img/commons/f/f5/CITIB-15.jpg" alt="...">
                                <div class="carousel-caption">
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="<?php echo base_url() ?>assets/custom/img/property-thumbnail/sample-logo-1.png" alt="Sample Logo 1">
                                        </a>
                                        <div class="media-body">
                                            <a href="<?php echo base_url() ?>property/single"><h4 class="media-heading">Sample Property</h4></a>
                                            <p>Village Name, City</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <img src="<?php echo base_url() ?>assets/custom/img/commons/d/d0/Art_Deco_type_house.jpg" alt="...">
                                <div class="carousel-caption">
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="<?php echo base_url() ?>assets/custom/img/property-thumbnail/sample-logo-2.png" alt="Sample Logo 1">
                                        </a>
                                        <div class="media-body">
                                            <a href="<?php echo base_url() ?>property/single"><h4 class="media-heading">Sample Property</h4></a>
                                            <p>Village Name, City</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <img src="<?php echo base_url() ?>assets/custom/img/commons/4/40/Dammtrappgatan_34-36_Enskede_G%C3%A5rd_002.jpg" alt="...">
                                <div class="carousel-caption">
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="<?php echo base_url() ?>assets/custom/img/property-thumbnail/sample-logo-3.png" alt="Sample Logo 1">
                                        </a>
                                        <div class="media-body">
                                            <a href="<?php echo base_url() ?>property/single"><h4 class="media-heading">Sample Property</h4></a>
                                            <p>Village Name, City</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- End product Slideshow -->

        </div>

    </div>

</section>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <section id="content">
                <div class="row">
                    <?php if( !empty($properties) ): ?>
                    <!-- product Listing -->
                    <div class="product-list grid-layout clearfix">

                        <div class="col-sm-12 col-md-12 the-title text-left">
                            <div class="row">
                                <div class="col-md-6 col-sm-7 col-xs-8">
                                    <h2>Newly updated property</h2>
                                </div>
                                <div class="col-md-6 col-sm-5 col-xs-4">
                                    <a class="pull-right btn btn-flat-alizarin" href="<?php echo base_url() ?>property/listing"><span>View More Listing</span> <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>

                    <?php $ctr = 0; ?>
                    <?php foreach( $properties as $property ):  ?>
                        <?php if( $ctr === 4 ) break; $ctr++; ?>
                        <div class="prop-item col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <div class="thumbnail-img thumbscrubber">
                                    <span class="ts-inner">
                                        <?php for( $i = 0 ; $i < count($property->images) ; $i++ ): ?>
                                        <img src="<?php echo base_url() . $property->images[ $i ]->path; ?>" class="ts-currslide">
                                        <?php endfor; ?>
                                    </span>
                                </div>

                                <div class="thumbnail-body">
                                    <div class="caption text-nowrap">
                                        <a href="<?php echo base_url() ?>property/single/<?php echo $property->id; ?>">
                                            <h3><?php echo $property->name; ?></h3>
                                            <span>
                                                <i class="fa fa-map-marker prop-address"></i>
                                                <?php echo $property->address; ?>
                                            </span>

                                            <div class="prop-price"><?php echo $property->price; ?></div>
                                        </a>
                                    </div>

                                    <div class="content clearfix">
                                        <ul class="list-unstyled feature-list">
                                            <li><?php echo $property->bedrooms; ?> Bedrooms</li>
                                            <li><?php echo $property->bathrooms; ?> Baths</li>
                                        </ul>

                                        <div class="link-action clearfix">
                                            <a href="#" class="col-md-12 col-sm-12 col-xs-12 save-favorite<?php echo (!empty($favorites) && in_array($property->id, $favorites)) ? ' active' : ''; ?>" data-toggle="tooltip" data-placement="top" title="Save to Favorite" data-property-id="<?php echo $property->id; ?>" >
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-xs-12">
                            <div class="text-center">
                                <h3>No properties to be displayed</h3>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </div>
</div>