<div class="container">
	<div class="row row-offcanvas-sm row-offcanvas-sm-right box-padding">

		<div class="col-md-9">
			<section id="content">

	<div class="row">
		<!-- Single Listing -->

		<div id="product-single" class="clearfix">

			<!--Property Information-->
			<div class="col-md-12">
				<button type="button" class="btn btn-flat-asbestos btn-icon hidden-md hidden-lg hidden-xs pull-right" data-toggle="offcanvas-sm">
					<i class="fa fa-share"></i> Contact &amp; Share
				</button>
				<div class="clearfix"></div>
			</div>

			<div id="product-title" class="col-sm-12 col-md-12 col-xs-12">
				<div class="title-bar clearfix">
					<div class="row">
						<?php
						$province_index = strrpos($property->province, ' (');
						$city_index = strrpos($property->city, ' (');
						$barangay_index = strrpos($property->barangay, ' (');

						if($province_index){
							$property->province = substr($property->province, 0, $province_index - strlen($property->province)) ;
						}
						if($city_index){
							$property->city = substr($property->city, 0, $city_index - strlen($property->city)) ;
						}
						if($barangay_index){
							$property->barangay = substr($property->barangay, 0, $barangay_index - strlen($property->barangay)) ;
						}
						?>
						<div class="col-sm-6 col-md-6 col-xs-12">
							<h2 id="property-name"><?php echo $property->name; ?></h2>
							<span>
								<i class="fa fa-map-marker"></i>
								<span id="property-address">
									<?php echo $property->region . " " . $property->province . " " . $property->city . " ". $property->barangay; ?>
								</span>
							</span>
						</div>

						<div class="col-sm-6 col-md-6 col-xs-12">
							<span class="prop-price">
								<?php echo $property->price; ?>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="row no-margin">
				<div class="col-md-12">
					<ul class="nav nav-tabs nav-tabs-inline">
						<li class="col-md-6 col-sm-6 col-xs-6">
							<a href="#propertydetails" data-toggle="tab">
								<i class="fa fa-home"></i>
								<span>Property Details</span>
							</a>
						</li>
						<li class="col-md-6 col-sm-6 col-xs-6 active">
							<a href="#locationmap" id="map-link" data-toggle="tab">
								<i class="fa fa-map-marker"></i>
								<span>Location Map</span>
							</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="tab-content tab-content-inline clearfix">
  				<div class="tab-pane fade" id="propertydetails">

					<div id="product-gallery" class="col-sm-12 col-md-12">
						<div class="product-details box">
							<div id="filmstrip" class="filmstrip slide">
							    <div class="filmstrip-top">
							        <div class="filmstrip-inner">
							        	<?php for( $i = 0 ; $i < count($property->images) ; $i++ ): ?>
					            		<div class="item<?php echo ($i === 0) ? ' active' : '' ; ?>">
							                <img class="media-object" src="<?php echo base_url() . $property->images[ $i ]->path; ?>">
							            </div>
							            <?php endfor; ?>
							        </div>
							        <a class="left filmstrip-control" href="#filmstrip" data-filmstrip="prev"><i class="fa fa-chevron-left"></i></a>
							        <a class="right filmstrip-control" href="#filmstrip" data-filmstrip="next"><i class="fa fa-chevron-right"></i></a>
							    </div>

							    <div class="filmstrip-thumbs-wrapper">
								    <div class="filmstrip-thumbs">
								    	<?php for( $i = 0 ; $i < count($property->images) ; $i++ ): ?>
								        <a class="thumb<?php echo ($i === 0) ? ' selector' : '' ; ?>" href="#filmstrip" data-filmstrip="selector">
								            <img class="media-object" src="<?php echo base_url() . $property->images[ $i ]->path; ?>">
								            <?php if($i === 0): ?>
								            	<div class="pointer <?php echo $i; ?>"></div>
								        	<?php endif; ?>
								        </a>
								        <?php endfor; ?>
								    </div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-12">
						<div class="product-details box">
							<div class="product-info">
								<h3>Property Features</h3>
								<ul>
									<li><?php echo $property->features; ?></li>
								</ul>
								<h3>Property Description</h3>
								<p>
								<?php echo $property->info; ?>
								</p>
							</div>
						</div>
					</div>
				</div>

				<!--Map Location-->
				<div class="tab-pane fade in active" id="locationmap">
					<div class="col-sm-12 col-md-12">
						<div class="product-details box locationmap">
							<div id="controls"></div>
							<div id="gmap-menu" class="map"></div>
						</div>
					</div>
				</div>
				<!-- End Location Map -->

				<!--Side bar-->
				<div class="tab-pane fade" id="localinfo">

				</div>
			</div>

			<div class="row no-margin">
				<div class="col-sm-12 col-md-12">
					<div class="title-bar clearfix">
						<h3>Contact Agent about this Property</h3>
					</div>
				</div>

				<div class="col-sm-12 col-md-12">
					<div class="product-details box">
						<div class="product-contact">
							<div class="title-bar clearfix">
								<h5>Assigned Agent</h5>
							</div>
							<form method="post" class="normal-form" id="msg-form">
								<div class="form-group">
									<label for="name">Name</label>
									<input type="hidden" name="to_id" id="to_id" value="<?php echo $property->agent_id; ?>" />
									<input type="text" class="form-control" id="to" name="to" placeholder="<?php echo $property->agent_full_name; ?>" disabled>
								</div>

								<div class="form-group">
									<label for="email">Email address</label>
									<input type="email" class="form-control" id="email" placeholder="<?php echo $property->agent_email; ?>" disabled>
								</div>

								<div class="form-group">
									<label for="contact">Telephone</label>
									<input type="tel" class="form-control" id="contact" placeholder="<?php echo $property->agent_contactno; ?>" disabled>
								</div>

								<div class="form-group">
									<label for="">Message</label>
									<textarea class="form-control" name="msg" rows="5" placeholder="Write a message..."></textarea>
								</div>

								<button type="submit" class="btn btn-flat-warning" id="submit_msg">Send</button>

							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End Single Listing -->

	</div>
			</section>
		</div>

		<div class="col-md-3 sidebar-offcanvas-sm">
			<section id="sidebar">
				<section id="aside-product-single">

	<div class="row">

		<!-- Sidebar for Single Listing -->
		<div class="aside-product-profile">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="panel panel-default panel-aside">
					<!-- Default panel contents -->
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Advertised By</h4>
					</div>

					<div class="panel-body">
						<div class="agent-avatar">
							<img src="<?php echo base_url(); ?><?php echo (!empty($user->avatar) ? 'assets/custom/img/avatar/uif-2.jpg' : $property->avatar ); ?>" class="img-circle">
						</div>
						<div class="agent-info">
							<a href="#">
								<h4><?php echo $property->user_full_name; ?></h4>
								<span><?php echo $property->user_email; ?></span>
							</a>
						</div>
						<div class="agent-contact">
							<span><?php echo $property->user_contactno; ?></span>
						</div>
					</div>
				</div>

				<div class="panel panel-default panel-aside">
					<ul class="list-group list-group-aside">
						<li class="list-group-item">
							<a href="#" class="save-favorite<?php echo (!empty($favorites) && in_array($property->id, $favorites)) ? ' active' : ''; ?>" data-property-id="<?php echo $property->id; ?>" data-toggle="tooltip" data-placement="top">
							<i class="fa fa-block fa-star-o"></i> Save to Favorite</a></li>
						<li class="list-group-item"><a href="#" class="print"><i class="fa fa-block fa-print"></i> Print this page</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

	</div>

</section>

			</section>
		</div>

	</div>
</div>

</section>