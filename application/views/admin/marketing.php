<div class="card">
    <div class="card-header">
        <h2>Sales this year </h2>

        <ul class="actions">
            <li class="dropdown action-show">
                <a href="" data-toggle="dropdown">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>

                <div class="dropdown-menu pull-right">
                    <p class="p-20">
                        You can put anything here
                    </p>
                </div>
            </li>
        </ul>
    </div>

    <div class="card-body">
        <div class="chart-edge">
            <div id="line-chart" class="flot-chart"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <!-- Recent Items -->
        <div class="card">
            <div class="card-header">
                <h2>Recently Opened Properties</h2>
                <ul class="actions">
                    <li>
                        <a href="" class="refresh-properties">
                            <i class="zmdi zmdi-refresh"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="card-body m-t-0">
                <table class="table table-inner table-vmiddle">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Property Name</th>
                            <th>Date viewed</th>
                            <th style="width: 60px">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $ctr = 0; ?>
                        <?php if(!empty($properties)): ?>
                        <?php foreach( $properties as $property ): ?>
                            <?php if( $ctr === 4 ) break; $ctr++; ?>
                            <tr>
                                <td class="f-500 c-cyan"><?php echo $property->id; ?></td>
                                <td><?php echo $property->name; ?></td>
                                <td><?php echo $property->date_viewed; ?></td>
                                <td class="f-500 c-cyan"><?php echo $property->price; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td> No recently viewed properties</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>

                </table>
            </div>
            <div id="recent-items-chart" class="flot-chart"></div>
        </div>

    </div>
</div>