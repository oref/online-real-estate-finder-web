<!-- Add Button -->
<a class="btn btn-float btn-primary m-btn" href="#" data-bind="add_property_type">
    <i class="zmdi zmdi-plus"></i>
</a>

<div class="card">
    <div class="card-header">
        <h2>Manage Property types</h2>
    </div>
    <table class="table table-striped table-vmiddle" id="table-property-types">
        <thead>
            <tr>
                <th data-column-id="id" data-order="asc">ID</th>
                <th data-column-id="type" data-order="asc">Property types</th>
                <th data-column-id="date_modified" data-order="asc">Date Modified</th>
                <th data-column-id="date_added">Date Added</th>
                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Action</th>
            </tr>
        </thead>
    </table>
</div>