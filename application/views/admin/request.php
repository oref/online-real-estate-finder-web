<div class="card">
    <div class="card-header">
        <h2>Manage Requests</h2>
    </div>
    <?php if( $admin->type === 'agent' ): ?>
    <div class="card-header">
        <h2>Request Pending</h2>
    </div>
    <table class="table table-striped table-vmiddle" id="table-approve-property-request">
        <thead>
            <tr>
                <th data-column-id="id" data-order="asc">Property ID</th>
                <th data-column-id="name" data-order="asc">Property Name</th>
                <th data-column-id="seller_name" data-order="asc">Seller Name</th>
                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Action</th>
            </tr>
        </thead>
    </table>
    <?php elseif( $admin->type === 'broker' || $admin->type === 'superadmin' || $admin->type === 'administrator' ): ?>
    <div class="card-header">
        <h2>Assigning of Agents</h2>
    </div>
    <table class="table table-striped table-vmiddle" id="table-assign-agent-request">
        <thead>
            <tr>
                <th data-column-id="id" data-order="asc">Property ID</th>
                <th data-column-id="name" data-order="asc">Property Name</th>
                <th data-column-id="seller_name" data-order="asc">Seller Name</th>
                <th data-column-id="agent" data-sortable="false" data-formatter="select">Assign Agent</th>
                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Action</th>
            </tr>
        </thead>
    </table>
    <div class="card-header">
        <h2>Approving of properties</h2>
    </div>
    <table class="table table-striped table-vmiddle" id="table-update-property-status-request">
        <thead>
            <tr>
                <th data-column-id="id" data-order="asc">Property ID</th>
                <th data-column-id="name" data-order="asc">Property Name</th>
                <th data-column-id="seller_name" data-order="asc">Seller Name</th>
                <th data-column-id="agent_name">Assigned Agent Name</th>
                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Action</th>
            </tr>
        </thead>
    </table>
    <?php endif; ?>
</div>