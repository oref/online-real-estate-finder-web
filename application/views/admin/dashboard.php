<div class="dash-widgets">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div id="pie-charts" class="dash-widget-item">
                <div class="bgm-pink">
                    <div class="dash-widget-header">
                        <div class="dash-widget-title">Property Statistics</div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="text-center p-20 m-t-25">
                        <div class="easy-pie main-pie" data-percent="<?php echo $pending_percentage; ?>">
                            <div class="percent"><?php echo $pending_percentage; ?></div>
                            <div class="pie-title">Pending Properties</div>
                        </div>
                    </div>
                </div>

                <div class="p-t-20 p-b-20 text-center">
                    <div class="easy-pie sub-pie-1" data-percent="<?php echo $sold_percentage; ?>">
                        <div class="percent"><?php echo $sold_percentage; ?></div>
                        <div class="pie-title">Sold Property</div>
                    </div>
                    <div class="easy-pie sub-pie-2" data-percent="<?php echo $approved_percentage; ?>">
                        <div class="percent"><?php echo $approved_percentage; ?></div>
                        <div class="pie-title">Posted Property</div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="mini-charts-item bgm-cyan">
                <div class="clearfix">
                    <div class="chart stats-bar"></div>
                    <div class="count">
                        <small>Declined Properties</small>
                        <h2><?php echo $total_property_declined; ?></h2>
                    </div>
                </div>
            </div>

            <div class="mini-charts-item bgm-lightgreen">
                <div class="clearfix">
                    <div class="chart stats-bar-2"></div>
                    <div class="count">
                        <small>Registered Agent</small>
                        <h2><?php echo $total_agent; ?></h2>
                    </div>
                </div>
            </div>

            <div class="mini-charts-item bgm-orange">
                <div class="clearfix">
                    <div class="chart stats-line"></div>
                    <div class="count">
                        <small>Registered Seller</small>
                        <h2><?php echo $total_seller; ?></h2>
                    </div>
                </div>
            </div>

            <div class="mini-charts-item bgm-bluegray">
                <div class="clearfix">
                    <div class="chart stats-line-2"></div>
                    <div class="count">
                        <small>Registered Buyer</small>
                        <h2><?php echo $total_buyer; ?></h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="<?php echo ($admin->type === 'agent') ? 'col-md-6' : 'col-md-3'; ?> col-sm-6">
            <div id="todo-lists">
                <div class="tl-header">
                    <h2>Tasks To Do</h2>
                    <ul class="actions">
                        <li>
                            <a href="#" class="refresh-task">
                                <i class="zmdi zmdi-refresh" style="color:white;"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>
                <div class="tl-body">
                    <div id="add-tl-item">
                        <i class="add-new-item zmdi zmdi-plus"></i>
                        <div class="add-tl-body">
                            <form method="POST" class="normal-form" role="form">
                                <textarea placeholder="What you want to do..." name="detail" maxlength="250"></textarea>
                                <div class="add-tl-actions">
                                    <a href="#" class="todo-close" data-tl-action="dismiss"><i class="zmdi zmdi-close"></i></a>
                                    <a href="#" class="todo-save" data-tl-action="save"><i class="zmdi zmdi-check"></i></a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="task-list"></div>
                </div>
            </div>
        </div>

        <?php  if( $admin->type !== 'agent' ): ?>
        <div class="col-md-3 col-sm-6">
            <div id="best-selling" class="dash-widget-item">
                <div class="dash-widget-header">
                    <div class="dash-widget-title">Agents</div>
                    <!-- Agent Picture -->
                    <img src="<?php echo base_url() ?>assets/custom/img/widgets/alpha.jpeg" alt="">
                    <div class="main-item">
                        <h2>Recently Login</h2>
                    </div>
                </div>

                <div class="listview p-t-5 agent-list"></div>
            </div>
        </div>
        <?php endif; ?>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card" >
                <div class="card-header">
                    <h2>Recently Opened Properties</h2>
                    <ul class="actions">
                    <li>
                        <a href="#" class="refresh-properties">
                            <i class="zmdi zmdi-refresh"></i>
                        </a>
                    </li>
                </ul>
                </div>

                <div class="card-body m-t-0 property-table">
                    <table class="table table-inner table-vmiddle">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Property Name</th>
                                <th>Date Viewed</th>
                                <th style="width: 60px">Price</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div id="recent-items-chart" class="flot-chart"></div>
            </div>
        </div>

        <!-- <div class="col-sm-6">

            <div class="card">
                <div class="listview">
                    <div class="lv-header">
                        Annoucements
                    </div>
                    <div class="lv-body">
                        <a class="lv-item" href="">
                            <div class="media">
                                <div class="pull-left">
                                    <img class="lv-img-sm" src="<?php echo base_url() ?>assets/custom/img/profile-pics/1.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="lv-title">Announcement Title</div>
                                    <small class="lv-small">Announcement Description</small>
                                </div>
                            </div>
                        </a>
                        <a class="lv-item" href="">
                            <div class="media">
                                <div class="pull-left">
                                    <img class="lv-img-sm" src="<?php echo base_url() ?>assets/custom/img/profile-pics/2.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="lv-title">Jonathan Morris</div>
                                    <small class="lv-small">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small>
                                </div>
                            </div>
                        </a>
                        <a class="lv-item" href="">
                            <div class="media">
                                <div class="pull-left">
                                    <img class="lv-img-sm" src="<?php echo base_url() ?>assets/custom/img/profile-pics/3.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="lv-title">Fredric Mitchell Jr.</div>
                                    <small class="lv-small">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small>
                                </div>
                            </div>
                        </a>
                        <a class="lv-item" href="">
                            <div class="media">
                                <div class="pull-left">
                                    <img class="lv-img-sm" src="<?php echo base_url() ?>assets/custom/img/profile-pics/4.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="lv-title">Glenn Jecobs</div>
                                    <small class="lv-small">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small>
                                </div>
                            </div>
                        </a>
                        <a class="lv-item" href="">
                            <div class="media">
                                <div class="pull-left">
                                    <img class="lv-img-sm" src="<?php echo base_url() ?>assets/custom/img/profile-pics/4.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="lv-title">Bill Phillips</div>
                                    <small class="lv-small">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small>
                                </div>
                            </div>
                        </a>
                    </div>
                    <a class="lv-footer" href="">View All</a>
                </div>
            </div>

        </div> -->

    </div>

    <div class="row">



    </div>

</div>