<div class="pmb-block" id="profile-update">
    <div class="pmbb-header">
        <h2><i class="zmdi zmdi-account m-r-5"></i> User Information</h2>
    </div>
    <div class="pmbb-body p-l-30">
        <form method="POST" enctype="multipart/form-data" id="edit-profile">
            <div class="pmbb-view">
                <dl class="dl-horizontal">
                    <div class="avatar-wrapper text-center">
                        <input id="avatar" name="avatar" type="file">
                    </div>
                    <div class="avatar-errors center-block"></div>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">User Name</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="text" class="form-control" name="username">
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">First Name</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="text" class="form-control" name="firstname">
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Last Name</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="text" class="form-control" name="lastname">
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Contact Number</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="text" class="form-control" name="contactno">
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Email Address</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="text" class="form-control" name="email">
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Region</dt>
                    <dd>
                        <div class="fg-line">
                            <select name="region_id" id="regions" class="form-control"></select>
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Province</dt>
                    <dd>
                        <div class="fg-line">
                            <select name="province_id" id="provinces" class="form-control" disabled>
                                <option value="none">-Select region first-</option>
                            </select>
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">City</dt>
                    <dd>
                        <div class="fg-line">
                            <select name="city_id" id="cities" class="form-control" disabled>
                                <option value="none">-Select province first-</option>
                            </select>
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Barangay</dt>
                    <dd>
                        <div class="fg-line">
                            <select name="barangay_id" id="barangays" class="form-control" disabled>
                                <option value="none">-Select city first-</option>
                            </select>
                        </div>
                    </dd>
                </dl>
                <div class="m-t-30">
                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>