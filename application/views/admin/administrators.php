<!-- Add button -->
<a class="btn btn-float btn-primary m-btn" href="#" data-bind="add_user">
    <i class="zmdi zmdi-plus"></i>
</a>

<div class="card">
    <div class="card-header">
        <h2>Manage Registered Administrators</h2>
    </div>

    <table id="table-administrator" class="table table-striped table-hover table-vmiddle">
        <thead>
            <tr>
                <th data-column-id="id" data-order="asc">User ID</th>
                <th data-column-id="name" data-order="asc">Name</th>
                <th data-column-id="last_login" data-order="asc">Last Login</th>
                <th data-column-id="status" data-order="asc">Status</th>
                <th data-column-id="Action" data-formatter="commands" data-sortable="false">Action</th>
            </tr>
        </thead>
    </table>
</div>