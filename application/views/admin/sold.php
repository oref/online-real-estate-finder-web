<div class="card">
    <div class="card-header">
        <h2>Manage Property</h2>
    </div>
    <table class="table table-striped table-vmiddle" id="table-sold-properties">
        <thead>
            <tr>
                <th data-column-id="id" data-order="asc">Property ID</th>
                <th data-column-id="name" data-order="asc">Property Name</th>
                <th data-column-id="price" data-order="asc">Commission</th>
                <th data-column-id="agent_name">Sold by</th>
                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Action</th>
            </tr>
        </thead>
    </table>
</div>