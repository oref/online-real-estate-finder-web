<div class="pmb-block" id="profile-update-credentials">
    <div class="pmbb-header">
        <h2><i class="zmdi zmdi-account m-r-5"></i> Update Credentials</h2>
    </div>
    <div class="pmbb-body p-l-30">
        <form method="POST" enctype="multipart/form-data" data-form="password">
            <div class="pmbb-view">
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Old Password</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="password" class="form-control" id="old_password" name="old_password" />
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">New Password</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="password" class="form-control" id="new_password" name="new_password" />
                        </div>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt class="p-t-10">Confirm New Password</dt>
                    <dd>
                        <div class="fg-line">
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" />
                        </div>
                    </dd>
                </dl>
                <div class="m-t-30">
                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>