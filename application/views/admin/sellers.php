<div class="card">
    <div class="card-header">
        <h2>Manage Registered sellers</h2>
    </div>

    <table id="table-seller" class="table table-striped table-hover table-vmiddle">
        <thead>
            <tr>
                <th data-column-id="id" data-order="asc">User ID</th>
                <th data-column-id="name" data-order="asc">Name</th>
                <th data-column-id="date_registered" data-order="asc">Date Applied</th>
                <th data-column-id="last_login" data-order="asc">Last Login</th>
                <th data-column-id="Action" data-formatter="commands" data-sortable="false">Action</th>
            </tr>
        </thead>
    </table>
</div>