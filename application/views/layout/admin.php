<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OREF - <?php echo ($controller) === 'front' ? 'Welcome to our Page!' : ucwords($controller); ?></title>
    <!-- Shortcut icons -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/custom/img/icons/houseicon.png"/>
    <link rel="icon" href="<?php echo base_url() ?>assets/custom/img/icons/houseicon.png"/>
    <!-- Stylesheets -->
    <?php echo $styles; ?>
    <!-- Header Scripts -->
    <?php echo $scripts['head']; ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-url="<?php echo base_url(); ?>" data-controller="<?php echo $controller ?>" data-method="<?php echo $method ?>"<?php echo ( $method === 'admin' || $method === 'lock' ) ? ' class="login-content"' : '' ?>>
        <?php if( $method === 'admin' || $method === 'lock' ): ?>
            <?php echo $content; ?>
        <?php else: ?>
        <header id="header">
                <ul class="header-inner">
                    <li id="menu-trigger" data-trigger="#sidebar">
                        <div class="line-wrap">
                            <div class="line top"></div>
                            <div class="line center"></div>
                            <div class="line bottom"></div>
                        </div>
                    </li>

                    <li class="logo hidden-xs">
                        <a href="<?php echo base_url(); ?>admin/dashboard">Online Real Estate Finder</a>
                    </li>

                    <li class="pull-right">
                        <ul class="top-menu">
                            <li class="dropdown" id="message-dropdown">
                                <a data-toggle="dropdown" class="tm-message" href="#">
                                    <i class="tmn-counts messages hide"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-lg pull-right">
                                    <div class="listview">
                                        <div class="lv-header">
                                            <span>Messages</span>
                                        </div>
                                        <div class="lv-body message-notification-list"></div>
                                        <a class="lv-footer" href="<?php echo base_url() ?>admin/messages">View All</a>
                                    </div>
                                </div>
                            </li>
                            <!-- <li class="dropdown">
                                <a data-toggle="dropdown" id="msg-icon" class="tm-notification" href="#">
                                    <i id="msg-count" class="tmn-counts notification hide">0</i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-lg pull-right">
                                    <div class="listview" id="notifications">
                                        <div class="lv-header">
                                            Notification
                                            <ul class="actions">
                                                <li class="dropdown">
                                                    <a href="#" data-clear="notification">
                                                        <i class="zmdi zmdi-check-all"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="lv-body">
                                            <a class="lv-item" href="">
                                                <div class="media">
                                                    <div class="pull-left">
                                                        <img class="lv-img-sm" src="<?php echo base_url(); ?>assets/custom/img/profile-pics/1.jpg" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="lv-title">Notification Made By</div>
                                                        <small class="lv-small">Notification Message Here</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <a class="lv-footer" href="">View Previous</a>
                                    </div> -->

                                </div>
                            </li>
                            <li>
                                <a class="tm-lock" href="<?php echo base_url() ?>admin/lock"></a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <!-- Top Search Content -->
                <div id="top-search-wrap">
                    <input type="text">
                    <i id="top-search-close">&times;</i>
                </div>
            </header>
            <section id="main">
                <aside id="sidebar">
                    <div class="sidebar-inner c-overflow">
                        <div class="profile-menu">
                            <a href="#">
                                <div class="profile-pic">
                                    <?php if( !empty( $admin->avatar ) ): ?>
                                        <img src="<?php echo base_url() . $admin->avatar; ?>" alt="avatar">
                                    <?php else: ?>
                                        <div class="lv-avatar bgm-<?php echo $admin->avatar_bg_color; ?>"><?php echo strtoupper($admin->firstname[0]); ?></div>
                                    <?php endif; ?>
                                </div>

                                <div class="profile-info">
                                    <?php echo $admin->firstname . ' ' . $admin->lastname ?>

                                    <i class="zmdi zmdi-arrow-drop-down"></i>
                                </div>
                            </a>

                            <ul class="main-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>admin/details"><i class="zmdi zmdi-account"></i> View Profile</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>admin/logout"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                                </li>
                            </ul>
                        </div>
                    <?php  if( $admin->type === 'agent' ): ?>
                        <ul class="main-menu">
                            <li<?php echo ( $method === 'dashboard' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/dashboard"><i class="zmdi zmdi-home"></i>Dashboard</a></li>
                            <li<?php echo ( $method === 'messages' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/messages"><i class="zmdi zmdi zmdi-comments"></i>Messages</a></li>
                            <li<?php echo ( $method === 'request' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/request"><i class="zmdi zmdi zmdi-gesture"></i>Requests</a></li>
                        </ul>
                    <?php endif; ?>

                    <?php if( $admin->type === 'broker' ): ?>
                        <ul class="main-menu">
                            <li<?php echo ( $method === 'dashboard' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/dashboard"><i class="zmdi zmdi-home"></i>Dashboard</a></li>
                            <li<?php echo ( $method === 'messages' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/messages"><i class="zmdi zmdi zmdi-comments"></i>Messages</a></li>
                            <li<?php echo ( $method === 'request' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/request"><i class="zmdi zmdi zmdi-gesture"></i>Requests</a></li>
                            <li<?php echo ( $method === 'marketing' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/marketing"><i class="zmdi zmdi-money"></i>Marketing</a></li>

                            <?php $properties = array( 'approved properties', 'declined properties', 'sold properties', 'types' ); ?>
                            <li class="sub-menu<?php echo ( in_array($method, $properties) ) ? ' active' : ''  ?>">
                                <a href=""><i class="zmdi zmdi-balance"></i> Properties</a>

                                <ul>
                                    <li><a<?php echo ( $method === 'approved properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/approved" name="approved-property">Approved Properties</a></li>
                                    <li><a<?php echo ( $method === 'declined properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/declined" name="declined-property">Declined Properties</a></li>
                                    <li><a<?php echo ( $method === 'sold properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/sold" name="sold-property">Sold Properties</a></li>
                                    <li><a<?php echo ( $method === 'types' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/types">Types</a></li>
                                </ul>
                            </li>

                            <?php $accounts = array( 'buyers', 'sellers', 'agents', 'brokers', 'administrators' ); ?>
                            <li class="sub-menu<?php echo ( in_array($method, $accounts) ) ? ' active' : ''  ?>">
                                <a href="#"><i class="zmdi zmdi-accounts-list-alt"></i> Accounts</a>

                                <ul>
                                    <li><a<?php echo ( $method === 'agents' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/agents"><i class="zmdi zmdi-accounts"></i> Manage Agents</a></li>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; ?>

                    <?php if( $admin->type === 'administrator' ): ?>
                        <ul class="main-menu">
                            <li<?php echo ( $method === 'dashboard' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/dashboard"><i class="zmdi zmdi-home"></i>Dashboard</a></li>
                            <li<?php echo ( $method === 'messages' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/messages"><i class="zmdi zmdi zmdi-comments"></i>Messages</a></li>
                            <li<?php echo ( $method === 'request' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/request"><i class="zmdi zmdi zmdi-gesture"></i>Requests</a></li>
                            <li<?php echo ( $method === 'marketing' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/marketing"><i class="zmdi zmdi-money"></i>Marketing</a></li>

                            <?php $properties = array( 'approved properties', 'declined properties', 'sold properties', 'types' ); ?>
                            <li class="sub-menu<?php echo ( in_array($method, $properties) ) ? ' active' : ''  ?>">
                                <a href="#"><i class="zmdi zmdi-balance"></i> Properties</a>

                                <ul>
                                    <li><a<?php echo ( $method === 'approved properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/approved" name="approved-property">Approved Properties</a></li>
                                    <li><a<?php echo ( $method === 'declined properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/declined" name="declined-property">Declined Properties</a></li>
                                    <li><a<?php echo ( $method === 'sold properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/sold" name="sold-property">Sold Properties</a></li>
                                    <li><a<?php echo ( $method === 'types' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/types">Types</a></li>
                                </ul>
                            </li>

                            <?php $accounts = array( 'buyers', 'sellers', 'agents', 'brokers', 'administrators' ); ?>
                            <li class="sub-menu<?php echo ( in_array($method, $accounts) ) ? ' active' : ''  ?>">
                                <a href="#"><i class="zmdi zmdi-accounts-list-alt"></i> Accounts</a>

                                <ul>
                                    <li><a<?php echo ( $method === 'buyers' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/buyers"><i class="zmdi zmdi-accounts"></i> Manage Buyers</a></li>
                                    <li><a<?php echo ( $method === 'sellers' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/sellers"><i class="zmdi zmdi-accounts"></i> Manage Sellers</a></li>
                                    <li><a<?php echo ( $method === 'agents' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/agents"><i class="zmdi zmdi-accounts"></i> Manage Agents</a></li>
                                    <li><a<?php echo ( $method === 'brokers' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/brokers"><i class="zmdi zmdi-accounts"></i> Manage Brokers</a></li>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; ?>

                    <?php if( $admin->type === 'superadmin' ): ?>
                        <ul class="main-menu">
                            <li<?php echo ( $method === 'dashboard' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/dashboard"><i class="zmdi zmdi-home"></i>Dashboard</a></li>
                            <li<?php echo ( $method === 'messages' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/messages"><i class="zmdi zmdi zmdi-comments"></i>Messages</a></li>
                            <li<?php echo ( $method === 'request' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/request"><i class="zmdi zmdi zmdi-gesture"></i>Requests</a></li>
                            <li<?php echo ( $method === 'marketing' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>admin/marketing"><i class="zmdi zmdi-money"></i>Marketing</a></li>

                            <?php $properties = array( 'approved properties', 'declined properties', 'sold properties', 'types' ); ?>
                            <li class="sub-menu<?php echo ( in_array($method, $properties) ) ? ' active' : ''  ?>">
                                <a href="#"><i class="zmdi zmdi-balance"></i> Properties</a>

                                <ul>
                                    <li><a<?php echo ( $method === 'approved properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/approved">Approved Properties</a></li>
                                    <li><a<?php echo ( $method === 'declined properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/declined">Declined Properties</a></li>
                                    <li><a<?php echo ( $method === 'sold properties' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/sold">Sold Properties</a></li>
                                    <li><a<?php echo ( $method === 'types' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/property/types">Types</a></li>
                                </ul>
                            </li>

                            <?php $accounts = array( 'buyers', 'sellers', 'agents', 'brokers', 'administrators' ); ?>
                            <li class="sub-menu<?php echo ( in_array($method, $accounts) ) ? ' active' : ''  ?>">
                                <a href="#"><i class="zmdi zmdi-accounts-list-alt"></i> Accounts</a>

                                <ul>
                                    <li><a<?php echo ( $method === 'buyers' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/buyers"><i class="zmdi zmdi-accounts"></i> Manage Buyers</a></li>
                                    <li><a<?php echo ( $method === 'sellers' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/sellers"><i class="zmdi zmdi-accounts"></i> Manage Sellers</a></li>
                                    <li><a<?php echo ( $method === 'agents' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/agents"><i class="zmdi zmdi-accounts"></i> Manage Agents</a></li>
                                    <li><a<?php echo ( $method === 'brokers' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/brokers"><i class="zmdi zmdi-accounts"></i> Manage Brokers</a></li>
                                    <li><a<?php echo ( $method === 'administrators' ) ? ' class="active"' : ''  ?> href="<?php echo base_url() ?>admin/account/administrators"><i class="zmdi zmdi-accounts"></i> Manage Administrators</a></li>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; ?>
                    </div>
                </aside>
                <section id="content">
                    <div class="container">
                        <div class="block-header">
                            <h2><?php echo ucfirst($method); ?></h2>
                        </div>
                        <?php
                            switch($method):
                                case 'details':
                                case 'credentials':
                                case 'commission_timeline':
                                case 'assigned_properties':
                                case 'sold_properties':
                        ?>
                        <div class="card" id="profile-main">
                            <div class="pm-overview c-overflow">
                                <div class="pmo-pic">
                                    <div class="p-relative">
                                        <img class="img-responsive avatar" src="<?php echo base_url() . $admin->avatar; ?>" alt="">

                                        <div class="dropdown pmop-message">
                                            <a data-toggle="dropdown" href="" class="btn bgm-white btn-float z-depth-1">
                                                <i class="zmdi zmdi-comment-text-alt"></i>
                                            </a>

                                            <div class="dropdown-menu">
                                                <textarea placeholder="Write something..."></textarea>

                                                <button class="btn bgm-green btn-icon"><i class="zmdi zmdi-mail-send"></i></button>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="pmo-stat">
                                        <h4 class="m-0 c-white"><?php echo $admin->firstname . ' ' . $admin->lastname; ?></h4>
                                    </div>
                                </div>
                                <div class="pmo-block pmo-contact hidden-xs">
                                    <ul>
                                        <li><i class="zmdi zmdi-account-circle"></i><?php echo $admin->username; ?></li>
                                        <li><i class="zmdi zmdi-phone"></i><?php echo $admin->contactno; ?></li>
                                        <li><i class="zmdi zmdi-email"></i><?php echo $admin->email; ?></li>
                                        <li>
                                            <i class="zmdi zmdi-pin"></i>
                                            <address class="m-b-0">
                                                <?php echo $admin->address; ?>
                                            </address>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php
                                switch($method):
                                    case 'details':
                            ?>
                                        <div class="pm-body clearfix">
                                            <ul class="tab-nav tn-justified">
                                                <li class="active waves-effect"><a href="<?php echo base_url() ?>admin/details">Edit Information</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/credentials">Edit Credentials</a></li>
                                                <?php if( $admin->type !== 'agent' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/commission_timeline">Commission Timeline</a></li>
                                                <?php endif; ?>
                                                <?php if( $admin->type === 'agent' || $admin->type === 'superadmin' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/assigned_properties">Assigned Properties</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/sold_properties">Sold Properties</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        <?php echo $content; ?>
                                        </div>

                            <?php
                                    break;
                                    case 'credentials':
                            ?>
                                        <div class="pm-body clearfix">
                                            <ul class="tab-nav tn-justified">
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/details">Edit Information</a></li>
                                                <li class="active waves-effect"><a href="<?php echo base_url() ?>admin/credentials">Edit Credentials</a></li>
                                                <?php if( $admin->type !== 'agent' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/commission_timeline">Commission Timeline</a></li>
                                                <?php endif; ?>
                                                <?php if( $admin->type === 'agent' || $admin->type === 'superadmin' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/assigned_properties">Assigned Properties</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/sold_properties">Sold Properties</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        <?php echo $content; ?>
                                        </div>
                            <?php
                                    break;
                                    case 'commission_timeline':
                            ?>
                                        <div class="pm-body clearfix">
                                            <ul class="tab-nav tn-justified">
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/details">Edit Information</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/credentials">Edit Credentials</a></li>
                                                <?php if( $admin->type !== 'agent' ): ?>
                                                <li class="active waves-effect"><a href="<?php echo base_url() ?>admin/commission_timeline">Commission Timeline</a></li>
                                                <?php endif; ?>
                                                <?php if( $admin->type === 'agent' || $admin->type === 'superadmin' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/assigned_properties">Assigned Properties</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/sold_properties">Sold Properties</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        <?php echo $content; ?>
                                        </div>

                            <?php
                                    break;
                                    case 'assigned_properties':
                            ?>
                                        <div class="pm-body clearfix">
                                            <ul class="tab-nav tn-justified">
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/details">Edit Information</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/credentials">Edit Credentials</a></li>
                                                <?php if( $admin->type !== 'agent' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/commission_timeline">Commission Timeline</a></li>
                                                <?php endif; ?>
                                                <?php if( $admin->type === 'agent' || $admin->type === 'superadmin' ): ?>
                                                <li class="active waves-effect"><a href="<?php echo base_url() ?>admin/assigned_properties">Assigned Properties</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/sold_properties">Sold Properties</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        <?php echo $content; ?>
                                        </div>
                            <?php
                                    break;
                                    case 'sold_properties':
                            ?>
                                        <div class="pm-body clearfix">
                                            <ul class="tab-nav tn-justified">
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/details">Edit Information</a></li>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/credentials">Edit Credentials</a></li>
                                                <?php if( $admin->type !== 'agent' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/commission_timeline">Commission Timeline</a></li>
                                                <?php endif; ?>
                                                <?php if( $admin->type === 'agent' || $admin->type === 'superadmin' ): ?>
                                                <li class="waves-effect"><a href="<?php echo base_url() ?>admin/assigned_properties">Assigned Properties</a></li>
                                                <li class="active waves-effect"><a href="<?php echo base_url() ?>admin/sold_properties">Sold Properties</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        <?php echo $content; ?>
                                        </div>
                            <?php
                                    break;
                                endswitch;
                            ?>
                        </div>
                        <?php
                                break;
                            default:
                        ?>
                            <?php echo $content; ?>
                        <?php endswitch; ?>
                    </div>
                </section>
            </section>
        <?php endif; ?>

        <footer id="footer">
            Copyright &copy; 2015 OREF
        </footer>

        <div class="modal fade" id="ajaxModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- AJAX Content here... -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="confirmation">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <!-- AJAX Content here... -->
                </div>
            </div>
        </div>

    <?php echo $scripts['body']; ?>
</body>
</html>