<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>OREF - <?php echo ($controller) === 'front' ? 'Welcome to our Page!' : ucwords($controller); ?></title>
	<!-- Shortcut icons -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/custom/img/icons/houseicon.png"/>
	<link rel="icon" href="<?php echo base_url() ?>assets/custom/img/icons/houseicon.png"/>
	<!-- Stylesheets -->
	<?php echo $styles; ?>
	<!-- Header Scripts -->
	<?php echo $scripts['head']; ?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body data-url="<?php echo base_url(); ?>" data-controller="<?php echo $controller ?>" data-method="<?php echo $method ?>">
	<div class="site-wrapper">
		<div class="site-row row-offcanvas row-offcanvas-left">
			<section id="header">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<nav class="navbar navbar-default" role="navigation">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<?php if($method === 'user'): ?>
									<button type="button" class="hidden-md hidden-lg navbar-toggle navbar-search-toggle" data-toggle="offcanvas">
										<span class="sr-only">Toggle Search</span>
										<i class="fa fa-search"></i>
										<i class="fa fa-times"></i>
									</button>
									<?php endif; ?>
									<!--<img src="<?php echo base_url(); ?>assets/custom/img/icons/logo.png" height="50" width="200">-->
									<a class="current navbar-brand" href="<?php echo base_url() ?>"> Online Real Estate Finder</a>

									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
										<span class="sr-only">Toggle Navigation</span>
										<i class="fa fa-bars"></i>
									</button>
								</div>

								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="collapse">

									<ul class="nav navbar-nav navbar-left">
										<li><a href="<?php echo base_url() ?>"><span class="fa fa-home"></span> Home</a></li>
										<li><a href="<?php echo base_url() ?>property/listing"><span class="fa fa-list"></span> Property Listings</a></li>
									</ul>

									<ul class="nav navbar-nav navbar-right">
										<?php if( $is_user_logged_in ): ?>
											<li class="dropdown user-info">
												<a href="#" class="dropdown-toggle user" data-toggle="dropdown">
													<span class="thumb-md">
														<img src="<?php echo base_url(); ?><?php echo (empty($user->avatar) ? 'assets/custom/img/avatar/uif-2.jpg' : $user->avatar ); ?>" class="img-circle" height="30">
														<?php echo $user->firstname; ?>
													</span>
														<b class="caret"></b>
												</a>
												<ul class="dropdown-menu">
													<li>
														<a href="<?php echo base_url(); ?>user"><span class="fa fa-user"></span> Overview</a>
													</li>
													<?php if($user->type === 'seller' || $user->type === 'superadmin'): ?>
													<li>
														<a href="<?php echo base_url(); ?>user/property"><span class="fa fa-dollar"></span> Properties</a>
													</li>
													<?php endif; ?>
													<li>
														<a href="<?php echo base_url(); ?>user/nearby_properties"><span class="fa fa-map-o"></span> Nearby Properties</a>
													</li>
													<li>
														<a href="<?php echo base_url(); ?>user/favorites"><span class="fa fa-star"></span> Favorites</a>
													</li>
													<li>
														<a href="<?php echo base_url(); ?>user/messages"><span class="fa fa-envelope"></span> Messages</a>
													</li>
													<li>
														<a href="<?php echo base_url(); ?>user/account"><span class="fa fa-sliders"></span> Account</a>
													</li>
													<li>
														<a href="<?php echo base_url(); ?>user/logout"><span class="fa fa-power-off"></span> Log Out</a>
													</li>
												</ul>
											</li>
										<?php else: ?>
											<li><a href="#" class="login" data-bind="login">Log In</a></li>
											<li><a href="#" class="register" data-bind="register">Register</a></li>
										<?php endif; ?>
									</ul>

								</div><!-- /.navbar-collapse -->
							</nav>
						</div>
					</div>
				</div>
			</section>

			<?php switch( $controller ):
						case 'user':
			?>
			<section id="profile-info">
				<div class="profile-cover-section">
					<div class="title-section-img">
						<img src="<?php echo base_url() ?>assets/custom/img/coverphoto/img-coverphoto.jpg" style="top: -380px;" />
					</div>
				</div>
				<div class="profile-cover-info"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-profile">
								<span class="sr-only">Toggle Navigation</span>
								<i class="fa fa-ellipsis-h"></i>
							</button>
							<button type="button" class="btn btn-icon hidden-md hidden-lg text-left" data-toggle="offcanvas-sm">
								<i class="fa fa-user"></i>
							</button>
							<div class="clearfix"></div>
							<div class="collapse navbar-collapse" id="collapse-profile">
								<ul id="profiletab" class="nav nav-tabs nav-tabs-hollo">
									<li<?php echo ( $method === 'overview' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>user">Overview</a></li>
									<?php if($user->type === 'seller' || $user->type === 'superadmin'): ?>
									<li<?php echo ( $method === 'property' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>user/property">Properties</a></li>
									<?php endif; ?>
									<li<?php echo ( $method === 'nearby_properties' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>user/nearby_properties">Nearby Properties</a></li>
									<li<?php echo ( $method === 'favorites' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>user/favorites">Favorites</a></li>
									<li<?php echo ( $method === 'messages' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>user/messages">Messages</a></li>
									<li<?php echo ( $method === 'account' ) ? ' class="active"' : ''  ?>><a href="<?php echo base_url() ?>user/account">Account</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="content">
				<div class="container">
					<div class="row row-offcanvas row-offcanvas-left row-offcanvas-sm row-offcanvas-sm-left row-offcanvas-xs box-padding">
						<div class="col-md-3 sidebar-offcanvas sidebar-offcanvas-sm">
							<div class="aside-profile">
								<div class="panel panel-default panel-profile">
									<div class="panel-body no-padding">
										<div class="media profile text-center">
											<div class="media-avatar">
												<img src="<?php echo base_url(); ?><?php echo (empty($user->avatar) ? 'assets/custom/img/avatar/uif-2.jpg' : $user->avatar ); ?>" class="img-circle">
												<span class="label label-info label-speech">Im a <?php echo $user->type ?></span>
											</div>
											<div class="media-body">
												<h4 class="media-heading full-name"><?php echo $user->firstname . ' ' . $user->lastname ?></h4>
												<span class="media-meta">
													<i class="fa fa-map-marker"></i>
													<?php if( $user->address_id ): ?>
														<?php echo $user->address; ?>
													<?php else: ?>
													N/A
													<?php endif; ?>
												</span>
												<div class="text-center">
													<span>Last Login : </span><abbr class="timeago" title="<?php echo $user->last_login ?>"><?php echo date('l, F j, Y \a\t g:ia', strtotime($user->last_login)); ?></abbr>
												</div>
											</div>

											<ul class="list-group list-group-inline media-counter clearfix">
												<li class="list-group-item col-md-6 col-sm-6 col-xs-6">
													<span class="counter-number"><?php echo $favorites; ?></span>
													<span class="counter-text">Favorites</span>
												</li>
												<li class="list-group-item col-md-6 col-sm-6 col-xs-6">
													<span class="counter-number">5</span>
													<span class="counter-text">Email Alerts</span>
												</li>
											</ul>

											<ul class="list-group text-left">
												<li class="list-group-item">
													<h5 class="list-title uppercase">About</h5>
													<p class="about text-justify">
														<?php if( !empty( $user->about ) ): ?>
														<?php echo $user->about ?>
														<?php else: ?>
															<span class="text-muted">Not available</span>
														<?php endif; ?>
													</p>
												</li>
												<li class="list-group-item">
													<h5 class="list-title uppercase">Interested in <span>buying</span></h5>
													<div class="product-tags clean clearfix interest-tags">
														<?php if( !empty( $user->tags ) ): ?>
														<?php $tags = explode( ",", $user->tags ); ?>
														<?php foreach( $tags as $tag ): ?>
															<a href="#"><?php echo $tag; ?></a>
														<?php endforeach; ?>
														<?php else: ?>
															<span class="text-muted">No tags available</span>
														<?php endif; ?>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-9">
							<div class="profile-content">
								<?php echo $content; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php 		break;
					case 'property':
			?>
				<section id="mini-product-search">
					<div class="container">
						<div class="row">
							<div class="product-list-search sidebar-offcanvas">
								<form class="form-search" role="form">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="product-list-search-inner clearfix">
											<div class="form-group col-md-5 col-sm-6">
												<label for="">Search Property</label>
												<div class="input-group">
													<div class="input-group-btn">
														<button type="button" class="btn btn-flat-asbestos btn-cat dropdown-toggle" data-toggle="dropdown">For Sale <span class="caret"></span></button>
														<ul class="dropdown-menu">
															<li><a href="#">For sale</a></li>
														</ul>

													</div><!-- /btn-group -->
													<input type="text" class="form-control" placeholder="Search Property">
												</div><!-- /input-group -->
											</div>
											<div class="form-group col-md-1 col-sm-3">
												<label for="">Min Price</label>
												<select class="form-control selectpicker">
													<option>$100</option>
													<option>$200</option>
													<option>$300</option>
													<option>$400</option>
													<option>$500</option>
												</select>
											</div>
											<div class="form-group col-md-1 col-sm-3">
												<label for="">Max Price</label>
												<select class="form-control selectpicker">
													<option>$100</option>
													<option>$200</option>
													<option>$300</option>
													<option>$400</option>
													<option>$500</option>
												</select>
											</div>
											<div class="form-group col-md-2 col-sm-3">
												<label for="">Property Type</label>
												<select class="form-control selectpicker">
													<option>Apartments</option>
													<option>Flats</option>
													<option>Linked House</option>
													<option>Semi Detached</option>
													<option>Bungalow</option>
												</select>
											</div>
											<div class="form-group col-md-1 col-sm-3">
												<label for="">Bedrooms</label>
												<select class="form-control selectpicker">
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>
											</div>
											<div class="form-group form-action col-md-2 col-sm-6">
												<button type="submit" class="btn-search btn btn-flat-alizarin">Search again</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
				<?php echo $content; ?>
			<?php 		break;
					default:
			?>
			<!-- Page contents here -->
			<?php echo $content; ?>
			<?php endswitch; ?>

			<section id="connect">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="newsletter clearfix">
								<div class="col-md-2 col-sm-12">
									<h5>Keep in Touch!</h5>
								</div>
								<div class="col-md-7 col-sm-9">
									<form class="form-inline normal-form">
										<div class="form-group">
											<input type="text" class="form-control" id="" placeholder="Your Name">
										</div>

										<div class="form-group">
											<input type="email" class="form-control" id="" placeholder="Your Email">
										</div>

										<button type="submit" class="btn btn-flat-alizarin">Subscribe to our Newsletter!</button>
									</form>
								</div>
								<div class="col-md-3 col-sm-3">
									<ul class="social-media list-unstyled clearfix">
										<li><a class="linkedin" href="https://www.linkedin.com/uas/login" target="_blank" data-toggle="tooltip" data-placement="top" title="Follow us on Linkedin!"><i class="fa fa-linkedin"></i></a></li>
										<li><a class="facebook" href="https://www.facebook.com/" target="_blank" data-toggle="tooltip" data-placement="top" title="Like us on Facebook!"><i class="fa fa-facebook"></i></a></li>
										<li><a class="twitter" href="https://www.twitter.com/" target="_blank" data-toggle="tooltip" data-placement="top" title="Follow us on Twitter!"><i class="fa fa-twitter"></i></a></li>
										<li><a class="google" href="https://plus.google.com/" target="_blank" data-toggle="tooltip" data-placement="top" title="Add on Google+!"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="footer">

				<div class="container">
					<div class="row">
						<div class="inner-column clearfix">

							<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
								<h5 class="">Company</h5>
								<ul class="list-unstyled">
									<li><a href="<?php echo base_url() ?>about" name="about">About Us</a></li>
									<li><a href="<?php echo base_url() ?>terms" name="terms">Terms of Use</a></li>
									<li><a href="<?php echo base_url() ?>contact-us" name="contact_us">Contact Us</a></li>
								</ul>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
								<h5 class="">Top Properties Listed</h5>
								<ul class="list-unstyled">
								<?php foreach( $property_types as $property_type ): ?>
									 <li>
										<a href="#" class="property-type" data-type-id="<?php echo $property_type->id; ?>" /><?php echo $property_type->type; ?></a>
									</li>
								<?php endforeach; ?>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
								<h5 class="">Popular Searches</h5>
								<div class="product-tags clearfix">
									<a href="#">Cebu City</a>
								</div>
							</div>
						</div>
					</div>
				</div>


			</section>

			<section id="footer-bottom">

				<div class="container">
					<div class="row">
						<div class="inner-column clearfix">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<p class="text-left">
									Data on sold prices provided by Company.
								</p>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<p class="text-right">
									&copy; <?php echo date("Y"); ?> OREF. All rights reserved.
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div><!-- end .site-row -->
	</div><!-- end .site-wrapper -->

	<div class="modal fade" id="ajaxModal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<!-- AJAX Content here... -->
			</div>
		</div>
	</div>

	<!-- Body Scripts -->
	<?php echo $scripts['body']; ?>
</body>
</html>
