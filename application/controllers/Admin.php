<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Default_Controller
{

    public function __construct()
    {
        $this->models = array( 'admin', 'user', 'property', 'message');
        $this->helpers = array( 'cookie' );

        parent::__construct();

        $this
             //Styles
             ->add_local_styles('assets/plugins/material-design-iconic-font/css/material-design-iconic-font',TRUE,TRUE)
             ->add_local_styles('assets/plugins/animate/css/animate',TRUE,TRUE)
             //Scripts
             ->add_local_scripts('assets/plugins/waves/js/waves',TRUE,TRUE)
             ->add_local_scripts('assets/plugins/moment/js/moment',TRUE,TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-tags-input/js/bootstrap-tags-input' ,TRUE ,TRUE)
             ->add_local_scripts('assets/custom/js/admin/common');


        $this->template = 'layout/admin';

        if(!$this->session_data( 'is_admin_logged_in' ) )
        {
            $error = array(
                        'name'   => 'error',
                        'value'  => 'You are not logged in.',
                        'expire' => '0',
                        'path'   => '/'
                    );

            $this->input->set_cookie( $error );

            redirect( 'admin' );
        }
        else{
            if($this->session_data( 'is_locked' ) && $this->data[ 'method' ] !== 'logout')
            {
                $error = array(
                            'name'   => 'error',
                            'value'  => 'You account is locked.',
                            'expire' => '0',
                            'path'   => '/'
                        );

                $this->input->set_cookie( $error );

                redirect( 'admin/lock' );
            }

            $this->data['admin'] = $this->admin->get_admin_details_by_id( $this->session_data( 'admin_id' ) );
        }
    }

    public function dashboard()
    {

        $this
             //Styles
             //->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/custom/css/admin')
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             //Scripts
             //->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/flot/js/jquery.flot')
             ->add_local_scripts('assets/plugins/sparklines/js/jquery.sparkline',TRUE,TRUE)
             ->add_local_scripts('assets/plugins/jquery-easy-pie-chart/js/jquery.easypiechart' ,TRUE, TRUE)
             ->add_local_scripts('assets/plugins/moment/js/moment', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/charts')
             ->add_local_scripts('assets/plugins/admin/flot-charts/line-chart')
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert',TRUE , TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/dashboard');

        $total_property = $this->property->get_total_properties();
        $total_sold = $this->property->get_total_sold_properties();
        $total_approved = $this->property->get_total_properties_by_status( 'approved' );
        $total_pending = $this->property->get_total_properties_by_status( 'pending' );
        $sold_percentage = 0;
        $approved_percentage = 0;
        $pending_percentage = 0;

        if( $total_property > 0 )
        {
            $sold_percentage = number_format((float)(($total_sold / $total_property) * 100 ), 1);
            $approved_percentage = number_format((float)(($total_approved / $total_property) * 100 ), 1);
            $pending_percentage = number_format((float)(($total_pending / $total_property) * 100 ), 1);

        }

        $this->data[ 'total_property' ] = $total_property;
        $this->data[ 'tasks' ] = $this->admin->get_task_details_by_admin();
        $this->data[ 'total_buyer' ] = $this->admin->get_total_users_by_type( 'buyer' );
        $this->data[ 'total_seller' ] = $this->admin->get_total_users_by_type( 'seller' );
        $this->data[ 'total_agent' ] = $this->admin->get_total_users_by_type( 'agent' );
        $this->data[ 'approved_percentage' ] = $approved_percentage;
        $this->data[ 'pending_percentage' ] = $pending_percentage;
        $this->data[ 'sold_percentage' ] = $sold_percentage;
        $this->data[ 'total_property_declined' ] = $this->property->get_total_declined_properties();
    }

    public function messages( $user_id = null )
    {
        $this
             //Styles
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootstrap-tags-input/js/bootstrap-tags-input' ,TRUE ,TRUE)
             ->add_local_scripts('assets/plugins/typeahead/js/typeahead.bundle', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/messages');

    }

    public function request()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/magnific/css/magnific-popup')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/magnific/js/jquery.magnific-popup', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
			 ->add_local_scripts('assets/custom/js/admin/request');
	}

    public function marketing()
    {
        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/flot/js/jquery.flot')
             ->add_local_scripts('assets/plugins/admin/flot-charts/line-chart')
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/dashboard');

        $this->data[ 'properties' ] = $this->property->get_all_property_details_opened_recently();
    }

    public function approved()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-file-input/css/fileinput', TRUE ,TRUE)
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/plugins/magnific/css/magnific-popup')
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootstrap-file-input/js/fileinput', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/magnific/js/jquery.magnific-popup', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/approved');

    }

    public function declined()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/magnific/css/magnific-popup')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/magnific/js/jquery.magnific-popup', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/declined');

    }

    public function sold()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/plugins/magnific/css/magnific-popup')
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/magnific/js/jquery.magnific-popup', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/sold');

    }

    public function types()
    {
        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/types');
    }

    public function buyers()
    {

        $this->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/buyer');

        $this->data[ 'buyers' ] = $this->admin->get_admin_details_by_type('buyer');
    }

    public function sellers()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/custom/js/admin/seller')
             ->add_local_scripts('assets/plugins/admin/js/functions');

        $this->data[ 'sellers' ] = $this->admin->get_admin_details_by_type('seller');
    }

    public function agents()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/agent');

        $this->data[ 'agents' ] = $this->admin->get_admin_details_by_type('agent');

    }

    public function brokers()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/broker');

        $this->data[ 'brokers' ] = $this->admin->get_admin_details_by_type('broker');
    }

    public function administrators()
    {

        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootgrid/css/jquery.bootgrid', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootgrid/js/jquery.bootgrid', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/administrator');

        $this->data[ 'administrators' ] = $this->admin->get_admin_details_by_type('administrator');
    }

    public function logout()
    {
        if($this->user->update_last_login( $this->session_data( 'admin_id' ) ) )
        {
            $this->session->unset_userdata( 'admin_id' );
            $this->session->unset_userdata( 'is_admin_logged_in' );
            $this->session->unset_userdata( 'is_locked' );

            delete_cookie('is_admin_logged_in', null, '/');
            redirect( 'admin' );
        }

        $this->view = FALSE;
    }

    public function details(){
        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootstrap-file-input/css/fileinput', TRUE ,TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/moment/js/moment', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/file-input/js/file-input', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/details');
    }

    public function credentials(){
        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/bootstrap-file-input/css/fileinput', TRUE ,TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/moment/js/moment', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/file-input/js/file-input', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/credentials');
    }

    public function commission_timeline(){
        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/light-gallery/css/lightGallery')
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/flot/js/jquery.flot')
             ->add_local_scripts('assets/plugins/sparklines/js/jquery.sparkline',TRUE,TRUE)
             ->add_local_scripts('assets/plugins/jquery-easy-pie-chart/js/jquery.easypiechart' ,TRUE, TRUE)
             ->add_local_scripts('assets/plugins/moment/js/moment', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/light-gallery/js/lightGallery', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/commission');
    }

    public function assigned_properties(){
        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/magnific/css/magnific-popup')
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/magnific/js/jquery.magnific-popup', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/assigned_properties');
    }

    public function sold_properties(){
        $this
             //Styles
             ->add_local_styles('assets/plugins/bootstrap-sweet-alert/css/sweet-alert')
             ->add_local_styles('assets/plugins/magnific/css/magnific-popup')
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/bootstrap-sweet-alert/js/sweet-alert', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/magnific/js/jquery.magnific-popup', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/admin/js/functions')
             ->add_local_scripts('assets/custom/js/admin/sold_properties');
    }

}