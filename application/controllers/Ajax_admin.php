<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_admin extends Ajax_Controller
{

    public function __construct()
    {
        $this->models = array( 'admin', 'user', 'property', 'message', 'address' );

        parent::__construct();
    }

    public function modal($modal)
    {
        $this->load->view('ajax/admin/modals/' . $modal );
    }

    public function signin()
    {
        $data = array();
        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            if($admin = $this->admin->is_admin_exists($ajax_data))
            {
                $this->session->unset_userdata( 'is_locked' );

                $session_data = array(
                    'admin_id'              =>  $admin->id,
                    'is_admin_logged_in'    =>  TRUE
                );

                $this->session->set_userdata($session_data);

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function unlock()
    {
        $data = array();
        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $admin = $this->admin->get_admin_details_by_id( $this->session_data( 'admin_id' ) );

            $ajax_data[ 'login' ] = $admin->email;
            if($admin = $this->admin->is_admin_exists($ajax_data))
            {
                $this->session->unset_userdata( 'is_locked' );

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_property()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->update_property($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }

        }

        echo json_encode($data);
    }

    public function update_admin_password()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'admin_id' );

            if($this->admin->update_admin_password($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function is_password_match()
    {
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'admin_id' );

            if($this->admin->is_password_match($ajax_data))
            {
                echo 'true';
            }
            else
            {
                echo 'false';
            }

        }
    }

    public function sell_property()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'admin_id' );

            if($id = $this->property->add_property($ajax_data))
            {
                $ajax_data[ 'id' ] = $id;

                $name = 'property_images';

                if(!empty($_FILES[$name]['name']))
                {
                    $property = $this->property->get_property_details_by_id( $id, 'property' );
                    $uploads = './uploads/';
                    $properties = './uploads/properties/';

                    if(!is_dir($uploads))
                    {
                        mkdir($uploads);
                        chmod($uploads, 0755);
                    }
                    else{
                        if(is_dir($uploads))
                        {
                            if(!is_dir($properties))
                            {
                                mkdir($properties);
                                chmod($properties, 0755);
                            }
                        }
                    }

                    $path = $properties.md5($id);

                    if(is_dir($path))
                    {
                        $files = glob($path . "/*");

                        if(isset($_FILES[$name]) && $_FILES[$name]['size'] > 0)
                        {
                            foreach($files as $file)
                            {
                                unlink($file); // Delete each file through the loop
                            }
                        }
                    }
                    else
                    {
                        mkdir($path);
                    }

                    $config = array(
                                    'upload_path'   =>  $path,
                                    'allowed_types' =>  'gif|jpg|png|jpeg',
                                    'encrypt_name'  =>  true
                                );

                    $this->load->library('upload', $config);

                    foreach( $_FILES as $fieldname => $fileObject )  //fieldname is the form field name
                    {
                        $files = $_FILES;
                        $ctr = count($_FILES[$name]['name']);
                        for( $i = 0 ; $i < $ctr; $i++ )
                        {
                            $_FILES[$name]['name'] = $files[$name]['name'][$i];
                            $_FILES[$name]['type'] = $files[$name]['type'][$i];
                            $_FILES[$name]['tmp_name'] = $files[$name]['tmp_name'][$i];
                            $_FILES[$name]['error'] = $files[$name]['error'][$i];
                            $_FILES[$name]['size'] = $files[$name]['size'][$i];

                            $this->upload->initialize( $config );
                            if($this->upload->do_upload( $name ))
                            {
                                $upload_data = $this->upload->data();

                                $ajax_data[ 'path' ] = ltrim($path . '/' . $upload_data[ 'file_name' ], './');
                                $ajax_data[ 'file_type' ] = $upload_data[ 'file_type' ];
                                $ajax_data[ 'file_size' ] = $upload_data[ 'file_size' ];
                                $ajax_data[ 'image_width' ] = $upload_data[ 'image_width' ];
                                $ajax_data[ 'image_height' ] = $upload_data[ 'image_width' ];
                            }
                            else
                            {
                                return $this->upload->display_errors('', '');
                            }

                            $this->property->add_property_images($ajax_data);
                        }
                    }
                }

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function add_property_type()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($id = $this->property->add_property_type($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function get_all_users( $query )
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $result = $this->admin->get_all_users( $query );
            $data = is_array($result) ? $result : [];
        }

        echo json_encode($data);
    }

    public function get_all_property_details_opened_recently()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->property->get_all_property_details_opened_recently();
        }

        echo json_encode($data);
    }

    public function get_property_details_by_id()
    {
        $data = array();

        if( ($ajax_data = $this->input->get()) && $this->input->is_ajax_request() )
        {
            extract($ajax_data);

            $properties = $this->property->get_property_details_by_id( $property_id );

            $properties->images = $this->property->get_all_property_images_by_id( $property_id );

            $data = $properties;
        }

        echo json_encode($data);
    }

    public function get_property_type_details_by_id()
    {
        $data = array();

        if( ($ajax_data = $this->input->get()) && $this->input->is_ajax_request() )
        {
            extract($ajax_data);
            $data = $this->property->get_all_property_type_details_by_id( $type_id );
        }

        echo json_encode($data);
    }

    public function get_all_approved_properties($status)
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $data = array(
                            'rows'      =>  $this->property->get_all_approved_properties($ajax_data,$status),
                            'total'     =>  $this->property->get_total_approved_properties(),
                            'current'   =>  $ajax_data['current'],
                            'rowCount'  =>  $ajax_data['rowCount']
                );
        }

        echo json_encode($data);
    }

    public function get_all_declined_properties()
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $data = array(
                            'rows'      =>  $this->property->get_all_declined_properties($ajax_data),
                            'total'     =>  $this->property->get_total_declined_properties(),
                            'current'   =>  $ajax_data['current'],
                            'rowCount'  =>  $ajax_data['rowCount']
                );
        }

        echo json_encode($data);
    }

    public function get_all_property_types()
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $data = array(
                            'rows'      =>  $this->property->get_all_property_types($ajax_data),
                            'total'     =>  $this->property->get_total_property_types(),
                            'current'   =>  $ajax_data['current'],
                            'rowCount'  =>  $ajax_data['rowCount']
                );
        }

        echo json_encode($data);
    }

    public function get_all_sold_properties()
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $data = array(
                            'rows'      =>  $this->property->get_all_sold_properties($ajax_data),
                            'total'     =>  $this->property->get_total_sold_properties(),
                            'current'   =>  $ajax_data['current'],
                            'rowCount'  =>  $ajax_data['rowCount']
                );
        }

        echo json_encode($data);
    }

    public function get_all_not_assigned_properties()
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $data = array(
                            'rows'      =>  $this->property->get_all_not_assigned_properties($ajax_data),
                            'total'     =>  $this->property->get_total_not_assigned_properties(),
                            'current'   =>  $ajax_data['current'],
                            'rowCount'  =>  $ajax_data['rowCount']
                );
        }

        echo json_encode($data);
    }

    public function get_all_assigned_properties()
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $id = $this->session_data( 'admin_id' );

            $ajax_data[ 'id' ] = $id;

            $data = array(
                            'rows'      =>  $this->property->get_all_assigned_properties($ajax_data),
                            'total'     =>  $this->property->get_total_assigned_properties( $id ),
                            'current'   =>  $ajax_data['current'],
                            'rowCount'  =>  $ajax_data['rowCount']
                );
        }

        echo json_encode($data);
    }

    public function get_all_tasks()
    {
        $data = array();

        if($this->input->is_ajax_request())
        {
            $data = $this->admin->get_task_details_by_admin();
        }

        echo json_encode($data);
    }

    public function add_task()
    {
        $data = array();

        if($ajax_data = $this->input->post())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'admin_id' );
            if($this->admin->add_task_details($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_task_status()
    {
        $data = array();

        if($ajax_data = $this->input->post())
        {
            if($this->admin->update_task_status($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_property_status( $status )
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->update_property_status( $ajax_data, $status ))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_property_agent_id()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->update_property_agent_id( $ajax_data ))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_property_type()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->update_property_type( $ajax_data ))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_property_approve()
    {
        $data = array();
        if( ( $ajax_data = $this->input->post() ) && $this->input->is_ajax_request() )
        {
            if($this->property->update_property_approve($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_sold_status()
    {
        $data = array();
        if( ( $ajax_data = $this->input->post() ) && $this->input->is_ajax_request() )
        {
            if($this->property->update_sold_status($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_re_assigned_property()
    {
        $data = array();
        if( ( $ajax_data = $this->input->post() ) && $this->input->is_ajax_request() )
        {
            if($this->property->update_re_assigned_property($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_claimed_commission()
    {
        $data = array();
        if( ( $ajax_data = $this->input->post() ) && $this->input->is_ajax_request() )
        {
            if($this->property->update_claimed_commission($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function get_user_details_by_type( $type )
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $data = array(
                            'rows'      =>  $this->admin->get_user_details_by_type( $ajax_data, $type ),
                            'total'     =>  $this->admin->get_total_users_by_type( $type ),
                            'current'   =>  $ajax_data['current'],
                            'rowCount'  =>  $ajax_data['rowCount']
                );
        }

        echo json_encode($data);
    }

    public function get_all_agents()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->admin->get_admin_details_by_type( 'agent' );
        }

        echo json_encode($data);
    }

    public function get_agents_details()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->admin->get_agent_details( 'agent' );
        }

        echo json_encode($data);
    }

    public function get_region_details()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->address->get_region_details();
        }

        echo json_encode($data);
    }

    public function get_property_types_details()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->property->get_property_types_details();
        }

        echo json_encode($data);
    }

    public function get_province_details()
    {
        $data = array();

        if( ( $ajax_data = $this->input->get() ) && $this->input->is_ajax_request() )
        {
            $data = $this->address->get_province_details( $ajax_data );
        }

        echo json_encode($data);
    }

    public function get_city_details()
    {
        $data = array();

        if( ( $ajax_data = $this->input->get() ) && $this->input->is_ajax_request() )
        {
            $data = $this->address->get_city_details( $ajax_data );
        }

        echo json_encode($data);
    }

    public function get_barangay_details()
    {
        $data = array();

        if( ( $ajax_data = $this->input->get() ) && $this->input->is_ajax_request() )
        {
            $data = $this->address->get_barangay_details( $ajax_data );
        }

        echo json_encode($data);
    }

    public function is_username_available()
    {
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'admin_id' );

            if($this->admin->is_username_available($ajax_data))
            {
                echo 'true';
            }
            else
            {
                echo 'false';
            }

        }
    }

    public function add_user_by_type( $type )
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->admin->add_user_by_type($ajax_data, $type))
            {
                $data[ 'status' ] = true;
            }
            else
            {
                $data[ 'status' ] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_property_agent()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->update_property_agent($ajax_data))
            {
                $data[ 'status' ] = true;
            }
            else
            {
                $data[ 'status' ] = false;
            }
        }

        echo json_encode($data);
    }

    public function get_all_conversations()
    {
        $data = array();

        if($this->input->is_ajax_request())
        {
            $data[ 'conversations' ] = $this->message->get_all_conversations_by_id( $this->session_data( 'admin_id' ) );
            $data[ 'id' ] = $this->session_data( 'admin_id' );
        }

        echo json_encode( $data );
    }

    public function get_all_message_notification()
    {
        $data = array();

        if($this->input->is_ajax_request())
        {
            $id = $this->session_data( 'admin_id' );

            $data[ 'conversations' ] = $this->message->get_all_conversations_by_id( $id, 5 );
            $data[ 'id' ] = $id;
        }

        echo json_encode( $data );
    }

    public function get_all_messages()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $data[ 'messages' ] = $this->message->get_all_messages_by_id( $ajax_data );
            $data[ 'id' ] = $this->session_data( 'admin_id' );
        }

        echo json_encode( $data );
    }

    public function get_recent_message()
    {
        $data = array();

        if($this->input->is_ajax_request())
        {
            $data = $this->message->get_recent_message( $this->session_data( 'admin_id' ) );
            if(is_object($data)){
                $data->current_id = $this->session_data( 'admin_id' );
            }
        }

        echo json_encode( $data );
    }

    public function add_message()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'from_id' ] = $this->session_data( 'admin_id' );
            if( $this->message->new_message( $ajax_data ) )
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_admin()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $id = $this->session_data( 'admin_id' );
            $user = $this->admin->get_admin_details_by_id( $id );
            $uploads = './uploads/avatars/';

            if(!is_dir($uploads))
            {
                mkdir($uploads, 0775, TRUE);
            }

            $name = 'avatar';
            $path = $uploads.md5($user->email);

            if(is_dir($path))
            {
                $files = glob($path . "/*");

                if(isset($_FILES[$name]) && $_FILES[$name]['size'] > 0)
                {
                    foreach($files as $file)
                    {
                        unlink($file); // Delete each file through the loop
                    }
                }
            }
            else
            {
                mkdir($path, 0775, TRUE);
            }

            $config = array(
                            'upload_path'   =>  $path,
                            'allowed_types' =>  'gif|jpg|png|jpeg',
                            'encrypt_name'  =>  true
                        );

            $this->load->library('upload', $config);

            if($this->upload->do_upload($name))
            {
                $upload_data = $this->upload->data();

                $ajax_data[ 'avatar' ] = ltrim($path . '/' . $upload_data[ 'file_name' ], './');
            }
            else
            {
                $msg = $this->upload->display_errors('', '');
            }

            $ajax_data[ 'id' ] = $id;

            if($this->admin->update_admin($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }

        }

        echo json_encode($data);
    }

    public function get_admin_details()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $id = $this->session_data( 'admin_id' );
            if( $ajax_data = $this->input->get() )
            {
                extract($ajax_data);
            }

            $data = $this->admin->get_admin_details_by_id( $id, true );
        }

        echo json_encode($data);
    }

    public function delete_property_type()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->delete_property_type($ajax_data))
            {
                $data['status'] = true;
            }
            else{
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function delete_user_by_id()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->admin->delete_user_by_id($ajax_data))
            {
                $data['status'] = true;
            }
            else{
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function get_all_property_assigned()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {

            $properties = $this->property->get_all_property_assigned( $this->session_data( 'admin_id' ) );

            if( is_array( $properties ) )
            {
                for( $i = 0 ; $i < count($properties) ; $i++ )
                {
                    $properties[ $i ]->images = $this->property->get_all_property_images_by_id( $properties[ $i ]->id );
                }
            }

            $data = $properties;
        }

        echo json_encode($data);
    }

    public function get_all_sold_property_assigned()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {

            $properties = $this->property->get_all_sold_property_assigned( $this->session_data( 'admin_id' ) );

            if( is_array( $properties ) )
            {
                for( $i = 0 ; $i < count($properties) ; $i++ )
                {
                    $properties[ $i ]->images = $this->property->get_all_property_images_by_id( $properties[ $i ]->id );
                }
            }

            $data = $properties;
        }

        echo json_encode($data);
    }

    public function get_all_property_details_by_status( $status )
    {
        $data = array();
        if( $this->input->is_ajax_request() )
        {
            $data = $this->property->get_all_property_details_by_status( $status );
        }

        echo json_encode($data);
    }

}