<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends Default_Controller
{

    public function __construct()
    {
        $this->models = array( 'user', 'property', 'admin' );

        parent::__construct();
    }

    public function user()
    {
        $this->template = 'layout/user';

        $this
             //Styles
             ->add_local_styles('assets/custom/css/user')
             //Scripts
             ->add_local_scripts('assets/plugins/jquery-thumbscrubber/js/jquery-thumbscrubber')
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/front');

        if($this->session_data( 'is_user_logged_in' ))
        {
            $data = array();
            $favorites = $this->user->get_user_favorite_properties( $this->session_data( 'user_id' ) );
            foreach( $favorites as $favorite )
            {
                extract( $favorite );
                $data[] = $property_id;
            }

            $this->data[ 'favorites' ] = $data;
        }

        $properties = $this->property->get_all_property_details_by_status( 'approved' );

        if( !empty($properties) )
        {
            for( $i = 0 ; $i < count($properties) ; $i++ )
            {
                $properties[ $i ]->images = $this->property->get_all_property_images_by_id( $properties[ $i ]->id );
            }
        }


        $this->data[ 'properties' ] = $properties;
    }

    public function admin()
    {
        $this->template = 'layout/admin';

        $this
             //Styles
             ->add_local_styles('assets/plugins/material-design-iconic-font/css/material-design-iconic-font', TRUE, TRUE)
             ->add_local_styles('assets/plugins/animate/css/animate', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/jquery-nicescroll/js/jquery.nicescroll', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/waves/js/waves', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-growl/js/bootstrap-growl', TRUE, TRUE)
             ->add_local_scripts('assets/custom/js/admin/front')
             ->add_local_scripts('assets/plugins/admin/js/functions');

        if($this->session_data( 'is_admin_logged_in' ))
        {
            redirect( 'admin/dashboard' );
        }
    }

    public function lock()
    {
        $this->template = 'layout/admin';

        $this
             //Styles
             ->add_local_styles('assets/plugins/material-design-iconic-font/css/material-design-iconic-font', TRUE, TRUE)
             ->add_local_styles('assets/plugins/animate/css/animate', TRUE, TRUE)
             ->add_local_styles('assets/custom/css/admin')
             //Scripts
             ->add_local_scripts('assets/plugins/jquery-nicescroll/js/jquery.nicescroll', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/waves/js/waves', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-growl/js/bootstrap-growl', TRUE, TRUE)
             ->add_local_scripts('assets/custom/js/admin/lock')
             ->add_local_scripts('assets/custom/js/admin/front')
             ->add_local_scripts('assets/plugins/admin/js/functions');

        if($this->user->update_last_login( $this->session_data( 'admin_id' ) ) )
        {
            $session_data = array(
                                'is_locked'    =>  TRUE
                            );

            $this->session->set_userdata($session_data);
        }
        else{
            $error = array(
                        'name'   => 'error',
                        'value'  => 'You are not logged in.',
                        'expire' => '0',
                        'path'   => '/'
                    );

            $this->input->set_cookie( $error );

            redirect( 'admin' );
        }

        $this->data['admin'] = $this->admin->get_admin_details_by_id( $this->session_data( 'admin_id' ) );
    }

    public function listing()
    {
        $this->template = 'layout/user';

        $this
             //Styles
             ->add_local_styles('assets/custom/css/user')
             //Scripts
             ->add_local_scripts('assets/plugins/jquery-thumbscrubber/js/jquery-thumbscrubber')
             ->add_local_scripts('assets/plugins/jquery-filmstrip/js/jquery-filmstrip')
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/property');
    }

    public function single($id)
    {
        $this->template = 'layout/user';

        $this
             //Styles
             ->add_local_styles('assets/custom/css/user')
             //Scripts
             ->add_local_scripts('assets/plugins/jquery-thumbscrubber/js/jquery-thumbscrubber')
             ->add_local_scripts('assets/plugins/jquery-filmstrip/js/jquery-filmstrip')
             ->add_local_scripts('https://maps.google.com/maps/api/js?sensor=true', FALSE)
             ->add_local_scripts('assets/plugins/gmaps/js/gmaps', TRUE, TRUE)
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/property');

        $properties = $this->property->get_property_details_by_id( $id );

        $properties->images = $this->property->get_all_property_images_by_id( $id );

        $this->data[ 'property' ] = $properties;
    }

    public function about()
    {
        $this->template = 'layout/user';

        $this
             //Styles
             ->add_local_styles('assets/custom/css/user')
             //Scripts
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/front');
    }

    public function terms()
    {
        $this->template = 'layout/user';

        $this
             //Styles
             ->add_local_styles('assets/custom/css/user')
             //Scripts
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/front');
    }

    public function contact_us()
    {
        $this->template = 'layout/user';

        $this
             //Styles
             ->add_local_styles('assets/custom/css/user')
             //Scripts
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/front');
    }
}