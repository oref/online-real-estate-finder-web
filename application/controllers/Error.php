<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends Default_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->add_local_styles('assets/custom/css/user');
    }

    public function index()
    {
        $this->template = 'layout/user';
	}

}