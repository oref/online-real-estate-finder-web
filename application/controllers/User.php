<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Default_Controller
{

    public function __construct()
    {
        $this->models = array( 'user', 'property' );
        $this->helpers = array( 'cookie' );

        parent::__construct();

        $this
             //Styles
             ->add_local_styles('assets/custom/css/user');

        $this->template = 'layout/user';

        if($this->session_data( 'is_user_logged_in' ))
        {
            $this->data[ 'user' ] = $this->user->get_user_details_by_id( $this->session_data( 'user_id' ) );
        }
        else
        {
            $error = array(
                    'name'   => 'error',
                    'value'  => 'You are not logged in.',
                    'expire' => '0',
                    'path'   => '/'
                );

            $this->input->set_cookie( $error );
            redirect( base_url() );
        }
    }

    public function account()
    {
        $this
             // Styles
             ->add_local_styles('assets/plugins/bootstrap-file-input/css/fileinput', TRUE ,TRUE)
             // Scripts
             ->add_local_scripts('assets/plugins/bootstrap-file-input/js/fileinput', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-tags-input/js/bootstrap-tags-input', TRUE, TRUE)
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/user');


    }

    public function overview()
    {
        $this
            //Scripts
            ->add_local_scripts('assets/custom/js/user/common');
    }

    public function favorites()
    {
        $this
             // Scripts
             ->add_local_scripts('assets/plugins/jquery-thumbscrubber/js/jquery-thumbscrubber')
             ->add_local_scripts('assets/plugins/jquery-filmstrip/js/jquery-filmstrip')
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/property');
    }

    public function messages()
    {
        $this
             //Styles
             ->add_local_styles('assets/plugins/material-design-iconic-font/css/material-design-iconic-font',TRUE,TRUE)
             ->add_local_styles('assets/plugins/animate/css/animate',TRUE,TRUE)
             ->add_local_styles('assets/custom/css/messages')
             //Scripts
             ->add_local_scripts('assets/plugins/bootstrap-tags-input/js/bootstrap-tags-input' ,TRUE ,TRUE)
             ->add_local_scripts('assets/plugins/typeahead/js/typeahead.bundle', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/waves/js/waves',TRUE,TRUE)
             ->add_local_scripts('assets/plugins/moment/js/moment',TRUE,TRUE)
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/messages');
    }

    public function property()
    {
        $this
             // Styles
             ->add_local_styles('assets/plugins/bootstrap-file-input/css/fileinput', TRUE ,TRUE)
             // Scripts
             ->add_local_scripts('assets/plugins/bootstrap-file-input/js/fileinput', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/bootstrap-tags-input/js/bootstrap-tags-input', TRUE, TRUE)
             ->add_local_scripts('assets/plugins/jquery-thumbscrubber/js/jquery-thumbscrubber')
             ->add_local_scripts('assets/plugins/jquery-filmstrip/js/jquery-filmstrip')
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/user');

        $this->data[ 'user' ] = $this->user->get_user_details_by_id( $this->session_data( 'user_id' ) );
    }

    public function nearby_properties()
    {
        $this
             //Scripts
             ->add_local_scripts('https://maps.google.com/maps/api/js?sensor=true', FALSE)
             ->add_local_scripts('assets/plugins/gmaps/js/gmaps', TRUE, TRUE)
             ->add_local_scripts('assets/custom/js/user/common')
             ->add_local_scripts('assets/custom/js/user/user');
    }

    public function logout()
    {
        if($this->user->update_last_login($this->session_data('user_id')))
        {
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('is_user_logged_in');

            delete_cookie('is_user_logged_in', null, '/');
            redirect( base_url() );
        }

        $this->view = FALSE;
    }

}