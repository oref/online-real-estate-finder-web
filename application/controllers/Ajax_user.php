<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_user extends Ajax_Controller
{

    private $_email_options = array();

    public function __construct()
    {
        $this->models = array( 'user', 'property', 'address', 'message' );

        parent::__construct();

        $this->_email_options = array(
                'smtp_host'     =>  'ssl://smtp.gmail.com',
                'smtp_port'     =>  '465',
                'smtp_user'     =>  'onlinerealestatefinder@gmail.com',
                'smtp_pass'     =>  '5J9dYOp8Sy',
                'mailtype'      =>  'html',
                'mail_title'    =>  'Online Real Estate Finder'
            );
    }

    /**
     * AJAX Views
     */

    public function modal( $modal )
    {
        $this->load->view( 'ajax/user/modals/' . $modal );
    }

    public function tab( $tab )
    {
        $this->load->view( 'ajax/user/tabs/' . $tab );
    }

    /**
     * AJAX Functions
     */
    public function signup()
    {

        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($id = $this->user->add_user($ajax_data))
            {
                $user = $this->user->get_temp_user_details_by_id( $id );

                $msg = '';
                $msg .= 'Hi <strong>' . ucwords($user->firstname) . ' ' . ucwords($user->lastname) . '</strong>,';
                $msg .= '<br>';
                $msg .= '<p>Please click the link below to verify your registration in OREF &copy;';
                $msg .= '<br>';
                $msg .= 'Verification Link: <a href="' . base_url() . '?c=' . $user->confirmation_code . '">' . base_url() . '?c=' . $user->confirmation_code . '</a>';
                $msg .= '<br>';
                $msg .= '<br>';
                $msg .= 'Best regards,';
                $msg .= '<br>';
                $msg .= '<strong>OREF Team</strong>';

                $this->_email_options[ 'subject' ] = 'Online Real Estate Finder - Email Verification';
                $this->_email_options[ 'receiver' ] = $user->email;
                $this->_email_options[ 'msg' ] = $msg;

                $this->mail_helper( $this->_email_options );

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function signin()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($user = $this->user->is_user_exists($ajax_data))
            {
                $session_data = array(
                    'user_id'           =>  $user->id,
                    'is_user_logged_in' =>  TRUE
                );

                $this->input->set_cookie( 'is_user_logged_in', true, 0, null, '/' );
                $this->session->set_userdata($session_data);

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }

        }

        echo json_encode($data);
    }

    public function reset_password()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            extract( $ajax_data );
            if( $user = $this->user->get_user_details_by_login( $email ) )
            {
                $password = $this->user->reset_password( $user );

                $msg = '';
                $msg .= 'Hi <strong>' . ucwords($user->firstname) . ' ' . ucwords($user->lastname) . '</strong>,';
                $msg .= '<br>';
                $msg .= '<p>We have set a new password for your OREF &copy; account. After you login change that to your desired password.';
                $msg .= '<br>';
                $msg .= '<br>';
                $msg .= 'Your new password: <strong>' . $password . '</strong><br>';
                $msg .= '<br>';
                $msg .= 'Login here:' . base_url();
                $msg .= '<br>';
                $msg .= '<br>';
                $msg .= 'Best regards,';
                $msg .= '<br>';
                $msg .= '<strong>OREF Team</strong>';

                $this->_email_options[ 'subject' ] = 'Online Real Estate Finder - Forgot Password Request';
                $this->_email_options[ 'receiver' ] = $user->email;
                $this->_email_options[ 'msg' ] = $msg;

                $this->mail_helper( $this->_email_options );

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }

        }

        echo json_encode($data);
    }

    public function resend_confirmation_link()
    {
        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $user = $this->user->get_temp_user_details_by_email( $ajax_data );

            if( $code = $this->user->update_confirmation_code( $user->email ) )
            {
                $msg = '';
                $msg .= 'Hi <strong>' . ucwords($user->firstname) . ' ' . ucwords($user->lastname) . '</strong>,';
                $msg .= '<br>';
                $msg .= '<p>Please click the link below to verify your registration in OREF &copy;';
                $msg .= '<br>';
                $msg .= 'Verification Link: <a href="' . base_url() . '?c=' . $code . '">' . base_url() . '?c=' . $code . '</a><br>';
                $msg .= '<br>';
                $msg .= '<br>';
                $msg .= 'Best regards,';
                $msg .= '<br>';
                $msg .= '<strong>OREF Team</strong>';

                $this->_email_options[ 'subject' ] = 'Online Real Estate Finder - Email Verification';
                $this->_email_options[ 'receiver' ] = $user->email;
                $this->_email_options[ 'msg' ] = $msg;

                $this->mail_helper( $this->_email_options );

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function user_verify()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($user = $this->user->update_email_verification($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function email_verify()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {

            if($user = $this->user->update_user_email($ajax_data))
            {

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_user()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $id = $this->session_data( 'user_id' );
            $ajax_data[ 'id' ] = $id;

            $name = 'avatar';

            if(!empty($_FILES[$name]['name']))
            {
                $user = $this->user->get_user_details_by_id( $id );
                $uploads = './uploads/';
                $avatars = './uploads/avatars/';

                if(!is_dir($uploads))
                {
                    mkdir($uploads);
                    chmod($uploads, 0755);
                }
                if(is_dir($uploads))
                {
                    if(!is_dir($avatars))
                    {
                        mkdir($avatars);
                        chmod($avatars, 0755);
                    }
                }
                $path = $avatars.md5($user->email);

                if(is_dir($path))
                {
                    $files = glob($path . "/*");

                    if(isset($_FILES[$name]) && $_FILES[$name]['size'] > 0)
                    {
                        foreach($files as $file)
                        {
                            unlink($file); // Delete each file through the loop
                        }
                    }
                }
                else
                {
                    mkdir($path);
                }

                $config = array(
                                'upload_path'   =>  $path,
                                'allowed_types' =>  'gif|jpg|png|jpeg',
                                'encrypt_name'  =>  true
                            );

                $this->load->library('upload', $config);

                if($this->upload->do_upload($name))
                {
                    $upload_data = $this->upload->data();

                    $ajax_data[ 'avatar' ] = ltrim($path . '/' . $upload_data[ 'file_name' ], './');
                }
                else
                {
                    return $this->upload->display_errors('', '');
                }
            }

            if($this->user->update_user($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }

        }

        echo json_encode($data);
    }

    public function is_email_available()
    {
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if( $this->session_data( 'is_logged_in' ) )
            {
                $ajax_data[ 'id' ] = $this->session_data( 'user_id' );
            }

            if($this->user->is_email_available($ajax_data))
            {
                echo 'true';
            }
            else
            {
                echo 'false';
            }

        }
    }

    public function is_username_available()
    {
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'user_id' );

            if($this->user->is_username_available($ajax_data))
            {
                echo 'true';
            }
            else
            {
                echo 'false';
            }

        }
    }

    public function is_email_match()
    {
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'user_id' );

            if($this->user->is_email_match($ajax_data))
            {
                echo 'true';
            }
            else
            {
                echo 'false';
            }
        }
    }

    public function is_password_match()
    {
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'user_id' );

            if($this->user->is_password_match($ajax_data))
            {
                echo 'true';
            }
            else
            {
                echo 'false';
            }

        }
    }

    public function change_user_email()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            extract( $ajax_data );

            $ajax_data[ 'id' ] = $this->session_data( 'user_id' );

            if($id = $this->user->change_user_email($ajax_data))
            {
                $user = $this->user->get_email_details_by_user_id( $id );

                $msg = '';
                $msg .= 'Hi <strong>' . ucwords($user->firstname) . ' ' . ucwords($user->lastname) . '</strong>,';
                $msg .= '<br>';
                $msg .= '<p>Please click the link below to verify your change email request in OREF &copy;';
                $msg .= '<br>';
                $msg .= 'Verification Link: <a href="' . base_url() . 'user/account?verify=' . $user->confirmation_code . '">' . base_url() . 'user/account?verify=' . $user->confirmation_code . '</a>';
                $msg .= '<br>';
                $msg .= '<br>';
                $msg .= 'Best regards,';
                $msg .= '<br>';
                $msg .= '<strong>OREF Team</strong>';

                $this->_email_options[ 'subject' ] = 'Online Real Estate Finder - Change Email Request';
                $this->_email_options[ 'receiver' ] = $user->new_email;
                $this->_email_options[ 'msg' ] = $msg;

                $this->mail_helper( $this->_email_options );

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function update_user_password()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'user_id' );

            if($this->user->update_user_password($ajax_data))
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function sell_property()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'user_id' );

            if($id = $this->property->add_property($ajax_data))
            {
                $ajax_data[ 'id' ] = $id;

                $name = 'property_images';

                if(!empty($_FILES[$name]['name']))
                {
                    $property = $this->property->get_property_details_by_id( $id, 'property' );
                    $uploads = './uploads/';
                    $properties = './uploads/properties/';

                    if(!is_dir($uploads))
                    {
                        mkdir($uploads);
                        chmod($uploads, 0755);
                    }
                    else{
                        if(is_dir($uploads))
                        {
                            if(!is_dir($properties))
                            {
                                mkdir($properties);
                                chmod($properties, 0755);
                            }
                        }
                    }

                    $path = $properties.md5($id);

                    if(is_dir($path))
                    {
                        $files = glob($path . "/*");

                        if(isset($_FILES[$name]) && $_FILES[$name]['size'] > 0)
                        {
                            foreach($files as $file)
                            {
                                unlink($file); // Delete each file through the loop
                            }
                        }
                    }
                    else
                    {
                        mkdir($path);
                    }

                    $config = array(
                                    'upload_path'   =>  $path,
                                    'allowed_types' =>  'gif|jpg|png|jpeg',
                                    'encrypt_name'  =>  true
                                );

                    $this->load->library('upload', $config);

                    foreach( $_FILES as $fieldname => $fileObject )  //fieldname is the form field name
                    {
                        $files = $_FILES;
                        $ctr = count($_FILES[$name]['name']);
                        for( $i = 0 ; $i < $ctr; $i++ )
                        {
                            $_FILES[$name]['name'] = $files[$name]['name'][$i];
                            $_FILES[$name]['type'] = $files[$name]['type'][$i];
                            $_FILES[$name]['tmp_name'] = $files[$name]['tmp_name'][$i];
                            $_FILES[$name]['error'] = $files[$name]['error'][$i];
                            $_FILES[$name]['size'] = $files[$name]['size'][$i];

                            $this->upload->initialize( $config );
                            if($this->upload->do_upload( $name ))
                            {
                                $upload_data = $this->upload->data();

                                $ajax_data[ 'path' ] = ltrim($path . '/' . $upload_data[ 'file_name' ], './');
                                $ajax_data[ 'file_type' ] = $upload_data[ 'file_type' ];
                                $ajax_data[ 'file_size' ] = $upload_data[ 'file_size' ];
                                $ajax_data[ 'image_width' ] = $upload_data[ 'image_width' ];
                                $ajax_data[ 'image_height' ] = $upload_data[ 'image_width' ];
                            }
                            else
                            {
                                return $this->upload->display_errors('', '');
                            }

                            $this->property->add_property_images($ajax_data);
                        }
                    }
                }

                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function add_update_property_view_recently()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->add_update_property_view_recently($ajax_data))
            {
                $data['status'] = true;
            }
            else{
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function property_list()
    {
        $data = array();
        if(($ajax_data = $this->input->get()) && $this->input->is_ajax_request())
        {
            $properties = $this->property->get_all_property_details_by_search( $ajax_data );

            if( is_array( $properties ) )
            {
                for( $i = 0 ; $i < count($properties) ; $i++ )
                {
                    $properties[ $i ]->images = $this->property->get_all_property_images_by_id( $properties[ $i ]->id );
                }
            }

            $data = array(
                        'properties' =>  $properties,
                        'total'      =>  $this->property->get_total_properties_by_status( 'approved' ),
                        'favorites'  =>  $this->property->get_all_favorite_property_ids( $this->session_data( 'user_id' ) )
                    );

            // $this->dump($data);
        }

        echo json_encode($data);
    }

    public function favorite_list()
    {
        $data = array();
        if(($ajax_data = $this->input->get()) && $this->input->is_ajax_request())
        {
            $id = $this->session_data( 'user_id' );
            $ajax_data['id'] = $id;
            $properties = $this->property->get_all_favorite_properties( $ajax_data );

            if( is_array( $properties ) )
            {
                for( $i = 0 ; $i < count($properties) ; $i++ )
                {
                    $properties[ $i ]->images = $this->property->get_all_property_images_by_id( $properties[ $i ]->id );
                }
            }

            $data = array(
                        'properties' =>  $properties,
                        'total'      =>  $this->property->get_total_favorite_properties_by_status( $id, 'approved' )
                    );
        }

        echo json_encode($data);
    }

    public function add_favorite_property()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'id' ] = $this->session_data( 'user_id' );

            if($this->property->add_favorite_property($ajax_data))
            {
                $data['status'] = true;
            }
            else{
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function delete_favorite_property()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            if($this->property->delete_favorite_property($ajax_data))
            {
                $data['status'] = true;
            }
            else{
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function get_all_approved_properties()
    {
        $data = array();

        if( ($ajax_data = $this->input->post()) && $this->input->is_ajax_request() )
        {
            $data = $this->property->get_all_approved_properties($ajax_data);
            $data['current'] = $ajax_data['current'];
            $data['rowCount'] = $ajax_data['rowCount'];
        }

        echo json_encode($data);
    }

    public function get_region_details()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->address->get_region_details();
        }

        echo json_encode($data);
    }

    public function get_province_details()
    {
        $data = array();

        if( ( $ajax_data = $this->input->get() ) && $this->input->is_ajax_request() )
        {
            $data = $this->address->get_province_details( $ajax_data );
        }

        echo json_encode($data);
    }

    public function get_city_details()
    {
        $data = array();

        if( ( $ajax_data = $this->input->get() ) && $this->input->is_ajax_request() )
        {
            $data = $this->address->get_city_details( $ajax_data );
        }

        echo json_encode($data);
    }

    public function get_barangay_details()
    {
        $data = array();

        if( ( $ajax_data = $this->input->get() ) && $this->input->is_ajax_request() )
        {
            $data = $this->address->get_barangay_details( $ajax_data );
        }

        echo json_encode($data);
    }

    public function get_user_details()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->user->get_user_details_by_id( $this->session_data( 'user_id' ) );
        }

        echo json_encode($data);
    }

    public function get_property_types_details()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->property->get_property_types_details();
        }

        echo json_encode($data);
    }

    public function get_all_property_details_by_status( $status )
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $data = $this->property->get_all_property_details_by_status( $status );
        }

        echo json_encode($data);
    }

    public function get_all_property_assigned_by_seller()
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $properties = $this->property->get_all_property_assigned_by_seller( $this->session_data( 'user_id' ) );

            if( is_array( $properties ) )
            {
                for( $i = 0 ; $i < count($properties) ; $i++ )
                {
                    $properties[ $i ]->images = $this->property->get_all_property_images_by_id( $properties[ $i ]->id );
                }
            }

            $data = $properties;
        }

        echo json_encode($data);
    }

    public function get_all_conversations()
    {
        $data = array();

        if($this->input->is_ajax_request())
        {
            $data[ 'conversations' ] = $this->message->get_all_conversations_by_id( $this->session_data( 'user_id' ) );
            $data[ 'id' ] = $this->session_data( 'user_id' );
        }

        echo json_encode( $data );
    }

    public function get_all_message_notification()
    {
        $data = array();

        if($this->input->is_ajax_request())
        {
            $id = $this->session_data( 'user_id' );

            $data[ 'conversations' ] = $this->message->get_all_conversations_by_id( $id, 5 );
            $data[ 'id' ] = $id;
        }

        echo json_encode( $data );
    }

    public function get_all_messages()
    {
        $data = array();

        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $data[ 'messages' ] = $this->message->get_all_messages_by_id( $ajax_data );
            $data[ 'id' ] = $this->session_data( 'user_id' );
        }

        echo json_encode( $data );
    }

    public function get_recent_message()
    {
        $data = array();

        if($this->input->is_ajax_request())
        {
            $data = $this->message->get_recent_message( $this->session_data( 'user_id' ) );
            if(is_object($data)){
                $data->current_id = $this->session_data( 'user_id' );
            }
        }

        echo json_encode( $data );
    }

    public function add_message()
    {
        $data = array();
        if(($ajax_data = $this->input->post()) && $this->input->is_ajax_request())
        {
            $ajax_data[ 'from_id' ] = $this->session_data( 'user_id' );
            if( $this->message->new_message( $ajax_data ) )
            {
                $data['status'] = true;
            }
            else
            {
                $data['status'] = false;
            }
        }

        echo json_encode($data);
    }

    public function get_all_users( $query )
    {
        $data = array();

        if( $this->input->is_ajax_request() )
        {
            $result = $this->user->get_all_users( $query );
            $data = is_array($result) ? $result : [];
        }

        echo json_encode($data);
    }

}